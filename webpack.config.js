webpack = module.require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");

module.exports = {
    entry: {
        'app': './client/app.js',
        'styles': './assets/styles/app.scss'
    },
    output: {
        path: path.resolve(__dirname, './public'),
        filename: 'js/[name].js'
    },
    resolve: {
        extensions: ['*', '.js', '.jsx', '.css', '.scss'],
        modules: ['node_modules', 'bower_components'],
        descriptionFiles: ['package.json', 'bower.json']
    },
    // devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'react', 'stage-1'],
                        plugins: [
                            ["transform-decorators-legacy"],
                        ]
                    }
                }
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        loader: 'css-loader',
                        options: {
                            minimize: true
                        }
                    }]
                })
            },
            {
                test: /\.(sass|scss)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.(woff2?|ttf|eot|svg|otf)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]',
                    outputPath: 'fonts/',
                    publicPath: '../../',
                    //useRelativePath: true
                }
            },
            {
                test: /\.(jpg|png|gif)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]',
                    outputPath: 'images/',
                    publicPath: '../../',
                    //useRelativePath: true
                }
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'css/app.css',
            allChunks: true
        }),
        new webpack.ProvidePlugin({
            moment: 'moment'
        }),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru|en-gb/)
    ]
};


