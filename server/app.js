import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

//Validation
import expressValidator from 'express-validator';
import customValidators from './customValidators';

//Authorization
import passport from 'passport';
import localLoginStrategy from './passport/local-login';
import localSignupStrategy from './passport/local-signup';

import config from '../config/config';

//Routes
import router from './routes/index';
import routerStatic from './routes/static';

//Error Handler
import {errorHandler} from './middleware/errorHandler';


const app = express();
const port = process.env.PORT || config.port;

app.disable('x-powered-by');

app.use('/', express.static('./public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
app.use(expressValidator({
    customValidators: customValidators
}));

//Passport
app.use(passport.initialize());

// load passport strategies
//passport.use('local-signup', localSignupStrategy);
passport.use('local-login', localLoginStrategy);
passport.use('local-signup', localSignupStrategy);


//DB
mongoose.connect(`mongodb://${config.dbUser}:${config.dbPassword}@${config.dbServer}:${config.dbPort}/${config.dbName}`);
mongoose.Promise = global.Promise;

//Routes
app.use('/api', router);
app.use('*', routerStatic);

app.use(errorHandler);

const server = app.listen(port, function() {
    const date = new Date();
    console.log(`Server is up and running at ${date} on port ${port}`);
});

const testApp = server;

export default testApp;