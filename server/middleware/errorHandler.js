import {mongooseErrorFormat} from '../helper';

export function errorHandler(err, req, res, next) {
    if (err.name === 'ValidationError') {
        res.statusCode = 400;
        res.json({
            error: mongooseErrorFormat(err)
        });
    } else if (err.code === 11000) {
        res.statusCode = 400;
        res.json({
            error: 'Record already exists'
        });
    } else if (err.name === 'CastError') {
        res.statusCode = 404;
        res.json({
            error: "Not Found"
        });

    } else if (err.name === 'CleanerNotFound') {
        res.statusCode = 404;
        return res.json({
            error: err.message
        });
    } else if (err.name === 'CleanerInvalid') {
        res.statusCode = 400;
        return res.json({
            error: err.message
        });
    } else if (err.name === 'CustomerInvalid') {
        res.statusCode = 400;
        return res.json({
            error: err.message
        });
    } else if (err.name === 'ErrorSavingBooking') {
        res.statusCode = 400;
        return res.json({
            error: err.message
        });
    } else if (err.name === 'ErrorSavingBookingCity') {
        res.statusCode = 400;
        return res.json({
            error: err.message
        });
    } else {
        res.statusCode = 500;
        return res.json({
            error: 'Server error'
        });
    }
}
