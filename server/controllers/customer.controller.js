import Customer from '../models/customer';
import Booking from '../models/booking';
import {validatorErrorFormat} from '../helper';
import {regexUnicodeHelper} from '../helper';

const nameValidationRegex = new RegExp('^[' + regexUnicodeHelper() + "]+(?:[-'][" + regexUnicodeHelper() + "]+)*([']?)$");

const validationRules = {
    'email': {
        notEmpty: true,
        isEmail: {
            errorMessage: 'Enter a valid e-mail address'
        },
        errorMessage: 'E-mail is required'
    },
    'firstName': {
        notEmpty: true,
        isLength: {
            options: [{min: 2, max: 100}],
            errorMessage: 'First name must be between 2 and 100 chars long'
        },
        matches: {
            // options: [/^[a-zA-Z]+(?:['][a-zA-Z]+)*([']?)$/],
            options: [nameValidationRegex],
            errorMessage: 'First name is invalid'
        },
        errorMessage: 'First name is required'
    },
    'lastName': {
        notEmpty: true,
        isLength: {
            options: [{min: 2, max: 100}],
            errorMessage: 'Last name must be between 2 and 100 chars long'
        },
        matches: {
            options: [nameValidationRegex],
            errorMessage: 'Last name is invalid'
        },
        errorMessage: 'Last name is required'
    },
    'phone': {
        optional: true,
        matches: {
            options: [/^\s*((\+?\s*(\(\s*)?3)?[\s-]*(\(\s*)?8[\s-]*)?(\(\s*)?0[\s-\(]*[1-9][\s-]*\d(\s*\))?([\s-]*\d){7}\s*$/],
            errorMessage: 'This is not a valid phone number'
        }
    }
};

function load(req, res, next, id) {
    Customer.get(id)
        .then(customer => {
            req.customer = customer;
            return next();
        }).catch(err => {
        return next(err);
    });
}

function get(req, res) {
    return res.json(req.customer);
}

function getBookings(req, res, next) {
    const customer = req.customer;

    Booking.find({customer: customer._id})
        .populate('city')
        .populate('cleaner')
        .populate('customer')
        .then((data) => {
            return res.json(data);
        })
        .catch((err) => {
            return next(err);
        });

}

function create(req, res, next) {

    req.sanitize('firstName').trim();
    req.sanitize('lastName').trim();
    req.sanitize('email').trim();
    req.sanitize('phone').trim();

    req.checkBody(validationRules);

    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.status(400).json({
                error: validatorErrorFormat(result.useFirstErrorOnly().array())
            });
            return;
        } else {
            const customer = new Customer({
                email: req.body.email,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                phone: req.body.phone
            });

            customer.save()
                .then((customer) => {
                    res.statusCode = 201;
                    return res.json(customer);
                })
                .catch(err => {
                    return next(err);
                });
        }
    });
}

function update(req, res, next) {

    req.sanitize('firstName').trim();
    req.sanitize('lastName').trim();
    req.sanitize('email').trim();
    req.sanitize('phone').trim();

    req.checkBody(validationRules);

    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.status(400).json({
                error: validatorErrorFormat(result.useFirstErrorOnly().array())
            });
            return;
        } else {
            const customer = req.customer;

            customer.email = req.body.email;
            customer.firstName = req.body.firstName;
            customer.lastName = req.body.lastName;
            customer.phone = req.body.phone;

            customer.save()
                .then((customer) => {
                    return res.json(customer);
                })
                .catch(err => {
                    return next(err);
                });
        }
    });
}

function list(req, res, next) {
    const {limit = 50, skip = 0} = req.query;
    Customer.list({limit, skip})
        .then(customer => {
            res.json(customer);
        })
        .catch(err => {
            return next(err);
        })
}

function remove(req, res, next) {
    const customer = req.customer;

    customer.remove()
        .then((customer) => {
            res.json(customer);
            return next();
        })
        .catch((err) => {
            return next(err);
        });
}

export default {load, get, create, update, list, remove, getBookings};