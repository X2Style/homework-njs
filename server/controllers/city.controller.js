import City from '../models/city';
import Cleaner from '../models/cleaner';
import {validatorErrorFormat} from '../helper';
import {regexUnicodeHelper} from '../helper';

const nameValidationRegex = new RegExp('^[' + regexUnicodeHelper() + "]+(?:[-\\s'][" + regexUnicodeHelper() + "]+)*([']?)$");

const validationRules = {
    'name': {
        notEmpty: true,
        isLength: {
            options: [{min: 2, max: 100}],
            errorMessage: 'City name must be between 2 and 100 chars long'
        },
        matches: {
            options: [nameValidationRegex],
            errorMessage: 'City name is invalid'
        },
        errorMessage: 'Name is required'
    },
    'priceFactor': {
        notEmpty: true,
        isFloat: {
            options: [{gt: 0}],
            errorMessage: 'Price factor must be greater then 0'
        },
        errorMessage: 'Price factor is required'
    }
};

function load(req, res, next, id) {

    City.get(id)
        .then(city => {
            req.city = city;
            return next();
        }).catch(err => {
        return next(err);
    });
}

function get(req, res) {
    return res.json(req.city);
}

function create(req, res, next) {

    req.sanitize('name').trim();
    req.sanitize('priceFactor').trim();

    req.checkBody(validationRules);

    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.status(400).json({
                error: validatorErrorFormat(result.useFirstErrorOnly().array())
            });
            return;
        } else {
            const city = new City({
                name: req.body.name,
                priceFactor: req.body.priceFactor
            });

            city.save()
                .then((city) => {
                    res.statusCode = 201;
                    return res.json(city);
                })
                .catch(err => {
                    return next(err);
                });
        }
    });
}

function update(req, res, next) {

    req.sanitize('name').trim();
    req.sanitize('priceFactor').trim();

    req.checkBody(validationRules);

    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.status(400).json({
                error: validatorErrorFormat(result.useFirstErrorOnly().array())
            });
            return;
        } else {
            const city = req.city;
            city.name = req.body.name;
            city.priceFactor = req.body.priceFactor;

            city.save()
                .then((city) => {
                    return res.json(city);
                })
                .catch(err => {
                    return next(err);
                });
        }
    });
}

function list(req, res, next) {
    const {limit = 50, skip = 0} = req.query;

    City.list({limit, skip})
        .then(city => {
            res.json(city);
        })
        .catch(err => {
            return next(err);
        });
}

function remove(req, res, next) {
    const city = req.city;

    city.remove()
        .then((city) => {
            res.json(city);
            return next();
        })
        .catch((err) => {
            return next(err);
        });
}

function getCleaners(req, res, next) {
    const city = req.city;

    Cleaner.getCleanersInCity(city._id)
        .then((cleaners) => {
            return res.json(cleaners);
        })
        .catch((err) => {
            return next(err);
        });

}

export default {load, get, create, update, list, remove, getCleaners};