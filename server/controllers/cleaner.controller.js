import Cleaner from '../models/cleaner';
import Booking from '../models/booking';
import {validatorErrorFormat} from '../helper';
import {regexUnicodeHelper} from '../helper';

const nameValidationRegex = new RegExp('^[' + regexUnicodeHelper() + "]+(?:[-'][" + regexUnicodeHelper() + "]+)*([']?)$");

const validationRules = {
    'email': {
        notEmpty: true,
        isEmail: {
            errorMessage: 'Enter a valid e-mail address'
        },
        errorMessage: 'E-mail is required'
    },
    'firstName': {
        notEmpty: true,
        isLength: {
            options: [{min: 2, max: 100}],
            errorMessage: 'First name must be between 2 and 100 chars long'
        },
        matches: {
            // options: [/^[a-zA-Z]+(?:['][a-zA-Z]+)*([']?)$/],
            options: [nameValidationRegex],
            errorMessage: 'First name is invalid'
        },
        errorMessage: 'First name is required'
    },
    'lastName': {
        notEmpty: true,
        isLength: {
            options: [{min: 2, max: 100}],
            errorMessage: 'Last name must be between 2 and 100 chars long'
        },
        matches: {
            options: [nameValidationRegex],
            errorMessage: 'Last name is invalid'
        },
        errorMessage: 'Last name is required'
    },
    'qualityScore': {
        notEmpty: true,
        isFloat: {
            options: [{min: 0, max: 5}],
            errorMessage: 'Quality score must be between 0 and 5'
        },
        errorMessage: 'Quality score is required'
    }
};

function load(req, res, next, id) {
    Cleaner.get(id)
        .then(cleaner => {
            req.cleaner = cleaner;
            return next();
        }).catch(err => {
        return next(err);
    });
}

function get(req, res) {
    return res.json(req.cleaner);
}

function getBookings(req, res, next) {
    const cleaner = req.cleaner;

    Booking.find({cleaner: cleaner._id})
        .populate('city')
        .populate('cleaner')
        .populate('customer')
        .then((data) => {
            return res.json(data);
        })
        .catch((err) => {
            return next(err);
        });
}

function create(req, res, next) {

    req.sanitize('firstName').trim();
    req.sanitize('lastName').trim();
    req.sanitize('email').trim();
    req.sanitize('qualityScore').trim();

    req.checkBody(validationRules);

    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.status(400).json({
                error: validatorErrorFormat(result.useFirstErrorOnly().array())
            });
            return;
        } else {
            const cleaner = new Cleaner({
                email: req.body.email,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                qualityScore: req.body.qualityScore,
                cities: req.body.cities
            });

            cleaner.save()
                .then((cleaner) => {
                    res.statusCode = 201;
                    return res.json(cleaner);
                })
                .catch(err => {
                    return next(err);
                });
        }
    });
}

function update(req, res, next) {

    req.sanitize('firstName').trim();
    req.sanitize('lastName').trim();
    req.sanitize('email').trim();
    req.sanitize('phone').trim();

    req.checkBody(validationRules);

    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.status(400).json({
                error: validatorErrorFormat(result.useFirstErrorOnly().array())
            });
            return;
        } else {
            const cleaner = req.cleaner;

            cleaner.email = req.body.email;
            cleaner.firstName = req.body.firstName;
            cleaner.lastName = req.body.lastName;
            cleaner.qualityScore = req.body.qualityScore;
            cleaner.cities = req.body.cities;

            cleaner.save()
                .then((cleaner) => {
                    return res.json(cleaner);
                })
                .catch(err => {
                    return next(err);
                });
        }
    });
}

function list(req, res, next) {
    const {limit = 50, skip = 0} = req.query;
    Cleaner.list({limit, skip})
        .then(cleaner => {
            res.json(cleaner);
        })
        .catch(err => {
            return next(err);
        })
}

function remove(req, res, next) {
    const cleaner = req.cleaner;

    cleaner.remove()
        .then((cleaner) => {
            res.json(cleaner);
            return next();
        })
        .catch((err) => {
            return next(err);
        });
}

export default {load, get, create, update, list, remove, getBookings};