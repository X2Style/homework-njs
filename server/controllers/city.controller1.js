import City from '../models/city';

function load(req, res, next, id) {
    City.get(id)
        .then((city) => {
            req.city = city; // eslint-disable-line no-param-reassign
            return next();
        })
        .catch(e => next(e));
}

function get(req, res) {
    return res.json(req.city);
}

function create(req, res, next) {
    const city = new City({
        name: req.body.name
    });

    city.save()
        .then(savedCity => res.json(savedCity))
        .catch(e => next(e));
}

function update(req, res, next) {
    const city = req.city;
    city.name = req.body.name;

    city.save()
        .then(savedCity => res.json(savedCity))
        .catch(e => next(e));
}

function list(req, res, next) {
    const { limit = 50, skip = 0 } = req.query;
    City.list({ limit, skip })
        .then(cities => res.json(cities))
        .catch(e => next(e));
}

function remove(req, res, next) {
    const city = req.city;
    city.remove()
        .then(deletedCity => res.json(deletedCity))
        .catch(e => next(e));
}

export default { load, get, create, update, list, remove };