import Booking from '../models/booking';
import Cleaner from '../models/cleaner';
import Customer from '../models/customer';
import City from '../models/city';
import moment from 'moment';
import config from '../../config/config';
import {validatorErrorFormat} from '../helper';
import {regexUnicodeHelper} from '../helper';

const nameValidationRegex = new RegExp('^[' + regexUnicodeHelper() + "]+(?:[-'][" + regexUnicodeHelper() + "]+)*([']?)$");

const validationRulesCreate = {
    'dateFrom': {
        notEmpty: true,
        isDateSameOrAfter: {
            errorMessage: 'Invalid date'
        },
        errorMessage: 'Date from is required'
    },
    'duration': {
        notEmpty: true,
        isInt: {
            options: [{min: 1, max: 8}],
            errorMessage: 'Duration must be from 1 to 8 Hours'
        },
        errorMessage: 'Duration is required'
    },
    'city': {
        notEmpty: true,
        isMongoId: {
            errorMessage: 'Invalid city'
        },
        errorMessage: 'City is required'
    },
    'email': {
        notEmpty: true,
        isEmail: {
            errorMessage: 'Enter a valid e-mail address'
        },
        errorMessage: 'E-mail is required'
    },
    'firstName': {
        notEmpty: true,
        isLength: {
            options: [{min: 2, max: 100}],
            errorMessage: 'First name must be between 2 and 100 chars long'
        },
        matches: {
            //options: [/^[a-zA-Z]+(?:['][a-zA-Z]+)*([']?)$/],
            options: [nameValidationRegex],
            errorMessage: 'First name is invalid'
        },
        errorMessage: 'First name is required'
    },
    'lastName': {
        notEmpty: true,
        isLength: {
            options: [{min: 2, max: 100}],
            errorMessage: 'Last name must be between 2 and 100 chars long'
        },
        matches: {
            options: [nameValidationRegex],
            errorMessage: 'Last name is invalid'
        },
        errorMessage: 'Last name is required'
    },
    'phone': {
        optional: true,
        matches: {
            options: [/^\s*((\+?\s*(\(\s*)?3)?[\s-]*(\(\s*)?8[\s-]*)?(\(\s*)?0[\s-\(]*[1-9][\s-]*\d(\s*\))?([\s-]*\d){7}\s*$/],
            errorMessage: 'This is not a valid phone number'
        }
    }
};

const validationRulesUpdate = {
    'dateFrom': {
        notEmpty: true,
        isISO8601: {
            errorMessage: 'Invalid date'
        },
        errorMessage: 'Date from is required'
    },
    'duration': {
        notEmpty: true,
        isInt: {
            options: [{min: 1, max: 8}],
            errorMessage: 'Duration must be from 1 to 8 Hours'
        },
        errorMessage: 'Duration is required'
    },
    'city': {
        notEmpty: true,
        isMongoId: {
            errorMessage: 'Invalid city'
        },
        errorMessage: 'City is required'
    },
    'customer': {
        notEmpty: true,
        isMongoId: {
            errorMessage: 'Invalid customer'
        },
        errorMessage: 'Customer is required'
    },
    'cleaner': {
        notEmpty: true,
        isMongoId: {
            errorMessage: 'Invalid cleaner'
        },
        errorMessage: 'Cleaner is required'
    }
};

function load(req, res, next, id) {
    Booking.get(id)
        .then(booking => {
            req.booking = booking;
            return next();
        }).catch(err => {
        return next(err);
    });
}

function get(req, res) {
    return res.json(req.booking);
}

function create(req, res, next) {

    req.sanitize('dateFrom').trim();
    req.sanitize('duration').trim();
    req.sanitize('duration').toInt();
    req.sanitize('city').trim();
    req.sanitize('firstName').trim();
    req.sanitize('lastName').trim();
    req.sanitize('email').trim();
    req.sanitize('phone').trim();

    req.checkBody(validationRulesCreate);

    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.status(400).json({
                error: validatorErrorFormat(result.useFirstErrorOnly().array())
            });
            return;
        } else {
            var bookingData = {
                dateFrom: req.body.dateFrom,
                dateTo: moment(req.body.dateFrom).add(req.body.duration, 'h'),
                city: req.body.city
            };

            Booking.getBookedCleaners(bookingData)
                .then((bookedCleaners) => {
                    // console.log("Booked cleaners");
                    // console.log(bookedCleaners);
                    var cleanersIds;
                    if (bookedCleaners.length > 0) {
                        cleanersIds = bookedCleaners.map((booking) => {
                            return booking.cleaner;
                        });
                    } else {
                        cleanersIds = [];
                    }
                    return Cleaner.getFreeCleaner(cleanersIds, bookingData.city);
                })
                .then((cleaner) => {
                    if (cleaner.length > 0) {
                        bookingData.cleaner = cleaner[0];
                    } else {
                        const error = new Error('Free cleaner not found. Try to choose another time.');
                        error.name = 'CleanerNotFound';
                        throw error;
                    }
                    // console.log("Current cleaner");
                    // console.log(cleaner);
                    return Customer.findByEmail(req.body.email);
                })
                .then((customerData) => {
                    if (customerData.length > 0) {
                        let customer = new Customer(customerData[0]);
                        customer.firstName = req.body.firstName;
                        customer.lastName = req.body.lastName;
                        customer.phone = req.body.phone;
                        return customer.save();
                    } else {
                        let customer = new Customer({
                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                            email: req.body.email,
                            phone: req.body.phone
                        });
                        return customer.save();
                    }
                })
                .then((customer) => {
                    bookingData.customer = customer;
                    return City.findById(bookingData.city);
                })
                .then((city) => {
                    bookingData.price = Math.floor(req.body.duration) * city.priceFactor * config.pricePerHour;
                    const booking = new Booking(bookingData);
                    // console.log(booking);
                    return booking.save();
                })
                .then((booking) => {
                    res.statusCode = 201;
                    return res.json(booking);
                })
                .catch(err => {
                    return next(err);
                });
        }
    });
}

function update(req, res, next) {

    req.sanitize('dateFrom').trim();
    req.sanitize('duration').trim();
    req.sanitize('duration').toInt();
    req.sanitize('city').trim();
    req.sanitize('customer').trim();
    req.sanitize('cleaner').trim();

    req.checkBody(validationRulesUpdate);

    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            res.status(400).json({
                error: validatorErrorFormat(result.useFirstErrorOnly().array())
            });
            return;
        } else {
            const booking = req.booking;

            const checkCleaner = () => {
                if (booking.cleaner._id == req.body.cleaner) {
                    return new Promise((resolve, reject) => {
                        resolve(booking.cleaner);
                    });
                } else {
                    return Cleaner.get(req.body.cleaner);
                }
            };

            checkCleaner()
                .then((cleaner) => {
                    if (!cleaner) {
                        const error = new Error('Invalid cleaner');
                        error.name = 'CleanerInvalid';
                        throw error;
                    }

                    const checkedCity = cleaner.cities.find((city) => {
                        if (city._id) {
                            return city._id == req.body.city;
                        } else {
                            return city == req.body.city;
                        }
                    });
                    if (!checkedCity) {
                        const error = new Error('This cleaner does not work in this city');
                        error.name = 'ErrorSavingBookingCity';
                        throw error;
                    }

                    booking.dateFrom = req.body.dateFrom;
                    booking.dateTo = moment(req.body.dateFrom).add(Math.floor(req.body.duration), 'h');
                    booking.city = req.body.city;
                    booking.cleaner = req.body.cleaner;
                    booking.customer = req.body.customer;

                    //Get cleaner bookings
                    return Booking.getCleanerBookings(booking);
                })
                .then((bookings) => {
                    if (bookings.length === 0) {
                        return Customer.get(req.body.customer);
                    } else {
                        const error = new Error('Error saving booking. Try to choose another time.');
                        error.name = 'ErrorSavingBooking';
                        throw error;
                    }
                })
                .then((customer) => {
                    if (customer) {
                        return booking.save();
                    } else {
                        const error = new Error('Invalid customer');
                        error.name = 'CustomerInvalid';
                        throw error;
                    }
                })
                .then((booking) => {
                    res.statusCode = 200;
                    return res.json(booking);
                })
                .catch(err => {
                    return next(err);
                });
        }
    });


//Then save

// booking.save(err => {
//     if (!err) {
//         return res.json(booking);
//     } else {
//         if (err.name === 'ValidationError') {
//             res.statusCode = 400;
//             res.json({
//                 error: 'Validation error'
//             });
//         } else {
//             res.statusCode = 500;
//             res.json({
//                 error: 'Server error'
//             });
//         }
//     }
// });
}

function list(req, res, next) {
    const {limit = 50, skip = 0} = req.query;
    Booking.list({limit, skip})
        .then(booking => {
            res.json(booking);
        })
        .catch(err => {
            return next(err);
        });
}

function remove(req, res, next) {
    const booking = req.booking;

    booking.remove()
        .then((booking) => {
            res.json(booking);
            return next();
        })
        .catch((err) => {
            return next(err);
        });
}

export default {load, get, create, update, list, remove};