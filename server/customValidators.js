import {minDate} from './helper';
import moment from 'moment';
import validator from 'validator';

const customValidators = {
    isDateSameOrAfter: (date) => {
        if (date && validator.isISO8601(date)) {
            return moment(date).isSameOrAfter(minDate());
        } else {
            return false;
        }
    }
};

export default customValidators;