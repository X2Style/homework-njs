import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const CustomerSchema = new Schema({
        email: {
            type: String,
            required: [true, 'E-mail is required'],
            index: {
                unique: [true, 'E-mail is unique']
            },
            match: [/^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/, 'Enter a valid e-mail address']
        },
        firstName: {
            type: String,
            required: [true, 'First name is required'],
            minlength: [2, 'First name must be at least 2 characters']
        },
        lastName: {
            type: String,
            required: [true, 'Last name is required'],
            minlength: [2, 'Last name must be at least 2 characters']
        },
        phone: {
            type: String,
            match: [/^\s*((\+?\s*(\(\s*)?3)?[\s-]*(\(\s*)?8[\s-]*)?(\(\s*)?0[\s-\(]*[1-9][\s-]*\d(\s*\))?([\s-]*\d){7}\s*$/, 'This is not a valid phone number']
        }
    },
    {
        versionKey: false
    });

CustomerSchema.statics = {
    get(id, cb) {
        return this.findById(id, cb);
    },

    findByEmail(email) {
        return this.find({email: email}).limit(1);
    },

    list({ skip = 0, limit = 50 } = {}, cb) {
        return this.find()
        //.sort({ createdAt: -1 })
            .skip(+skip)
            .limit(+limit)
            .exec(cb);
    }
};

export default mongoose.model('Customer', CustomerSchema);