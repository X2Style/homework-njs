import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const CitySchema = new Schema({
        name: {
            type: String,
            required: [true, 'Name is required'],
            index: {
                unique: [true, 'Name must be unique']
            },
            minlength: [2, 'City name must be at least 2 characters'],
            maxlength: [100, 'City name must be less then 100 characters']
        },
        priceFactor: {
            type: Number,
            required: [true, 'Price factor is required'],
            min: [0.1, 'Price factor must be greater then 0']
        },
        cleaners: {
            type: Schema.Types.ObjectId,
            ref: "cleaner"
        }
    },
    {
        versionKey: false
    });

CitySchema.statics = {
    get(id, cb) {
        return this.findById(id, cb);
    },

    list({skip = 0, limit = 50} = {}, cb) {
        return this.find()
        //.sort({ createdAt: -1 })
            .skip(+skip)
            .limit(+limit)
            .exec(cb);
    }
};

export default mongoose.model('City', CitySchema);