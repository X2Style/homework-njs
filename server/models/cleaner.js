import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const CleanerSchema = new Schema({
        email: {
            type: String,
            required: [true, 'E-mail is required'],
            index: {
                unique: [true, 'E-mail is unique']
            },
            match: [/^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/, 'Enter a valid e-mail address']
        },
        firstName: {
            type: String,
            required: [true, 'First name is required'],
            minlength: [2, 'First name must be at least 2 characters']
        },
        lastName: {
            type: String,
            required: [true, 'Last name is required'],
            minlength: [2, 'Last name must be at least 2 characters']
        },
        qualityScore: {
            type: Number,
            required: [true, 'Quality score is required'],
            min: [0, 'Quality score must be between 0 and 5'],
            max: [5, 'Quality score must be between 0 and 5']
        },
        cities: [{
            type: Schema.Types.ObjectId,
            required: true,
            ref: "City"
        }]
    },
    {
        versionKey: false
    });

CleanerSchema.statics = {
    get(id, cb) {
        return this.findById(id, cb).populate('cities');
    },

    getFreeCleaner(excludeIds, city) {
        //return this.find({_id: {$nin: excludeIds}}).exec(cb);
        return this.find({_id: {$nin: excludeIds}, cities: {_id: city}})
            .populate('cities')
            .sort({qualityScore: -1})
            .limit(1);
            //.exec(cb);
    },

    getCleanersInCity(city) {
        return this.find({cities: {_id: city}});
    },

    list({skip = 0, limit = 50} = {}, cb) {
        return this.find()
        //.sort({ createdAt: -1 })
            .skip(+skip)
            .limit(+limit)
            .exec(cb);
    }
};

export default mongoose.model('Cleaner', CleanerSchema);