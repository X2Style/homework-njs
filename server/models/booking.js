import mongoose from 'mongoose';
import moment from 'moment';

const Schema = mongoose.Schema;

const BookingSchema = new Schema({
        dateFrom: {
            type: Date,
            required: [true, 'Date from is required'],
            // validate: {
            //     validator: (value) => {
            //         return moment(value).isSameOrAfter(minDate());
            //     },
            //     message: 'Invalid date'
            // }
        },
        dateTo: {
            type: Date,
            required: [true, 'Duration is required'],
            validate: [durationValidator, 'Duration must be from 1 to 8 Hours']
        },
        price: {
            type: Number,
            required: [true, 'Price is required'],
            min: [0.1, 'Price must be greater then 0']
        },
        city: {
            type: Schema.Types.ObjectId,
            ref: "City",
            required: [true, 'City is required']
        },
        cleaner: {
            type: Schema.Types.ObjectId,
            ref: "Cleaner",
            required: [true, 'Cleaner is required']
        },
        customer: {
            type: Schema.Types.ObjectId,
            ref: "Customer",
            required: [true, 'Customer is required']
        }
    },
    {
        versionKey: false
    });

//Validation helper functions
function durationValidator(value) {
    const duration = moment(value).diff(moment(this.dateFrom), 'hours');
    return (duration > 0 && duration < 9);
}

BookingSchema.statics = {
    get(id, cb) {
        return this.findById(id, cb).populate('city').populate('cleaner').populate('customer');
    },

    getBookedCleaners(booking) {
        return this.find({
            $or: [
                {$and: [{dateFrom: {$gte: booking.dateFrom}}, {dateTo: {$lte: booking.dateTo}}]},
                {$and: [{dateFrom: {$lt: booking.dateFrom}}, {dateTo: {$gt: booking.dateFrom}}]},
                {$and: [{dateFrom: {$lt: booking.dateTo}}, {dateTo: {$gt: booking.dateTo}}]},
                {$and: [{dateFrom: {$lt: booking.dateFrom}}, {dateTo: {$gt: booking.dateTo}}]}
            ]
        })
            .populate('city')
            .populate('cleaner')
            .populate('customer');
        //.exec(cb);
    },

    getCleanerBookings(booking) {
        return this.find({
            $and: [
                {cleaner: booking.cleaner},
                {_id: {$ne: booking._id}},
                {
                    $or: [
                        {$and: [{dateFrom: {$gte: booking.dateFrom}}, {dateTo: {$lte: booking.dateTo}}]},
                        {$and: [{dateFrom: {$lt: booking.dateFrom}}, {dateTo: {$gt: booking.dateFrom}}]},
                        {$and: [{dateFrom: {$lt: booking.dateTo}}, {dateTo: {$gt: booking.dateTo}}]},
                        {$and: [{dateFrom: {$lt: booking.dateFrom}}, {dateTo: {$gt: booking.dateTo}}]}
                    ]
                }
            ]
        });
    },

    list({skip = 0, limit = 50} = {}, cb) {
        return this.find()
            .sort({dateFrom: -1})
            .skip(+skip)
            .limit(+limit)
            .populate('city')
            .populate('cleaner')
            .populate('customer')
            .exec(cb);
    }
};

export default mongoose.model('Booking', BookingSchema);