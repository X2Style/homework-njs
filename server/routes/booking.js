import {Router} from 'express';
import bookingController from '../controllers/booking.controller';

const router = Router();

router.route('/')
    .get(bookingController.list)
    .post(bookingController.create);

router.route('/:id')
    .get(bookingController.get)
    .put(bookingController.update)
    .delete(bookingController.remove);

router.param('id', bookingController.load);

export default router;