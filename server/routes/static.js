import {Router} from 'express';
import path from 'path';

const routerStatic = Router();

routerStatic.route('*')
    .get(staticFiles);

function staticFiles(req, res) {
    res.sendFile(path.resolve('public', 'index.html'));
}

export default routerStatic;