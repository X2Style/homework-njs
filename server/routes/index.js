import {Router} from 'express';
import authCheckMiddleware from '../middleware/auth-check';
import bookingRoutes from './booking';
import cityRoutes from './city';
import cleanerRoutes from './cleaner';
import customerRoutes from './customer';
import authRoutes from './auth';
import clientRoutes from './client';

const router = Router();

router.get('/', (req, res) => {
    res.json({'status': 'Ok'});
});

//Auth-check
router.use('/booking', authCheckMiddleware);
router.use('/city', authCheckMiddleware);
router.use('/cleaner', authCheckMiddleware);
router.use('/customer', authCheckMiddleware);

router.use('/booking', bookingRoutes);
router.use('/city', cityRoutes);
router.use('/cleaner', cleanerRoutes);
router.use('/customer', customerRoutes);
router.use('/client', clientRoutes);
router.use('/', authRoutes);

export default router;