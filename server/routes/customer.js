import {Router} from 'express';
import customerController from '../controllers/customer.controller';

const router = Router();

router.route('/')
    .get(customerController.list)
    .post(customerController.create);

router.route('/:id/bookings')
    .get(customerController.getBookings);

router.route('/:id')
    .get(customerController.get)
    .put(customerController.update)
    .delete(customerController.remove);

router.param('id', customerController.load);

export default router;