import {Router} from 'express';
import cityController from '../controllers/city.controller';
import bookingController from '../controllers/booking.controller';

const router = Router();

router.route('/city').get(cityController.list);
router.route('/booking').post(bookingController.create);

export default router;