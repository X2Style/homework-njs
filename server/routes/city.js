import {Router} from 'express';
import cityController from '../controllers/city.controller';

const router = Router();

router.route('/')
    .get(cityController.list)
    .post(cityController.create);

router.route('/:id/cleaners')
    .get(cityController.getCleaners);

router.route('/:id')
    .get(cityController.get)
    .put(cityController.update)
    .delete(cityController.remove);

router.param('id', cityController.load);

export default router;