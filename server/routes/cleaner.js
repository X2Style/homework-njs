import {Router} from 'express';
import cleanerController from '../controllers/cleaner.controller';

const router = Router();

router.route('/')
    .get(cleanerController.list)
    .post(cleanerController.create);

router.route('/:id/bookings')
    .get(cleanerController.getBookings);

router.route('/:id')
    .get(cleanerController.get)
    .put(cleanerController.update)
    .delete(cleanerController.remove);

router.param('id', cleanerController.load);

export default router;