process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
let app = require('../../server/app').default;
let should = chai.should();
let User = require('../../server/models/user').default;
let Cleaner = require('../../server/models/cleaner').default;
let City = require('../../server/models/city').default;
let config = require('../../config/config').default;

chai.use(chaiHttp);

//Our parent block
describe('Cleaners', () => {

    const testCities = [];
    let token = '';

    before((done) => {
        const city1 = new City({
            name: 'Kyiv',
            priceFactor: 2.8
        });

        const city2 = new City({
            name: 'Dnipro',
            priceFactor: 1.6
        });

        const user = new User({
            name: 'Test',
            email: 'test@test.com',
            password: 'testtest'
        });

        user.save((err, user) => {
            chai.request(app)
                .post('/api/login')
                .send({
                    email: 'test@test.com',
                    password: 'testtest'
                })
                .end((err, res) => {
                    token = res.body.token;

                    city1.save((err, city) => {
                        testCities.push(city);
                        city2.save((err, city) => {
                            testCities.push(city);
                            done();
                        });
                    });

                });
        });
    });

    after((done) => {
        User.remove({}, (err) => {
            City.remove({}, (err) => {
                done();
            });
        });
    });

    beforeEach((done) => {
        Cleaner.remove({}, (err) => {
            done();
        });
    });

    describe('Get all cleaners', () => {
        it('it should GET all the cleaners', (done) => {
            chai.request(app)
                .get('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.to.equal(0);
                    done();
                });
        });

        it('it should add 2 cleaners and get all', (done) => {
            const cleaner1 = new Cleaner({
                email: 'valid1@mail.com',
                firstName: 'Firstname1',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            });
            const cleaner2 = new Cleaner({
                email: 'valid2@mail.com',
                firstName: 'Firstname2',
                lastName: 'Lastname',
                qualityScore: 1.6,
                cities: [testCities[0]._id]
            });
            cleaner1.save((err, cleaner) => {
                cleaner2.save((err, cleaner) => {
                    chai.request(app)
                        .get('/api/cleaner')
                        .set('Authorization', 'Bearer ' + token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.to.equal(2);
                            done();
                        });
                });
            });
        });
    });

    describe('Create cleaner', () => {

        it('it should not add a cleaner without firstname', (done) => {
            let cleaner = {
                email: 'valid2@mail.com',
                lastName: 'Lastname',
                qualityScore: 1.6,
                cities: [testCities[0]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is required');
                    done();
                });
        });

        it('it should not add a cleaner with firstname length < 2', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'F',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a cleaner with firstname length < 100', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijz',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a cleaner with firstname contains digits', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'F1rst',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner with firstname contains forbidden symbols', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Fir$t',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        // it('it should not add a cleaner with firstname contains -', (done) => {
        //     let cleaner = {
        //         email: 'valid1@mail.com',
        //         firstName: 'Fir-st',
        //         lastName: 'Lastname',
        //         qualityScore: 1.5,
        //         cities: [testCities[0]._id, testCities[1]._id]
        //     };
        //     chai.request(app)
        //         .post('/api/cleaner')
        //         .set('Authorization', 'Bearer ' + token)
        //         .send(cleaner)
        //         .end((err, res) => {
        //             res.should.have.status(400);
        //             res.body.should.be.a('object');
        //             res.body.should.have.property('error');
        //             res.body.error.should.be.a('array');
        //             res.body.error[0].should.be.a('object');
        //             res.body.error[0].should.have.property('name').equal('firstName');
        //             res.body.error[0].should.have.property('message').equal('First name is invalid');
        //             done();
        //         });
        // });

        it('it should not add a cleaner with firstname contains \'\'', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Fir\'\'st',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner with firstname begins with \'', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: '\'First',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner with firstname contains begins with -', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: '-Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner with firstname contains ends with -', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Firstname-',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner without lastname', (done) => {
            let cleaner = {
                email: 'valid2@mail.com',
                firstName: 'Lastname',
                qualityScore: 1.6,
                cities: [testCities[0]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is required');
                    done();
                });
        });

        it('it should not add a cleaner with lastname length < 2', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: 'L',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a cleaner with lastname length > 100', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: 'Abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijz',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a cleaner with lastname contains digits', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: 'Las1name',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner with lastname contains forbidden symbols', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: 'L@$tn@me',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner with lastname contains \'\'', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: 'Last\'\'name',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner with lastname contains begins with \'', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: '\'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner with lastname contains begins with -', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: '-Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner with lastname contains ends with -', (done) => {
            let cleaner = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname-',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a cleaner without email', (done) => {
            let cleaner = {
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('E-mail is required');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email', (done) => {
            let cleaner = {
                email: 'fail',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like "Abc.example.com"', (done) => {
            let cleaner = {
                email: 'Abc.example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like "A@b@c@example.com"', (done) => {
            let cleaner = {
                email: 'A@b@c@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like "just"not"right@example.com"', (done) => {
            let cleaner = {
                email: 'just"not"right@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like "a"b(c)d,e:f;g<h>i[j\k]l@example.com"', (done) => {
            let cleaner = {
                email: 'a"b(c)d,e:f;g<h>i[j\k]l@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like "this is"not\allowed@example.com"', (done) => {
            let cleaner = {
                email: 'this is"not\allowed@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like "this\ still\"not\\allowed@example.com"', (done) => {
            let cleaner = {
                email: 'this\ still\"not\\allowed@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like "john..doe@example.com"', (done) => {
            let cleaner = {
                email: 'john..doe@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like ".john.doe@example.com"', (done) => {
            let cleaner = {
                email: '.john.doe@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like "example@localhost"', (done) => {
            let cleaner = {
                email: 'example@localhost',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with invalid email like "john.doe@example..com"', (done) => {
            let cleaner = {
                email: 'john.doe@example..com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a cleaner with same email', (done) => {
            const cleaner = new Cleaner({
                email: 'double@email.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.5,
                cities: [testCities[0]._id, testCities[1]._id]
            });
            cleaner.save((err, cleaner) => {
                chai.request(app)
                    .post('/api/cleaner/')
                    .set('Authorization', 'Bearer ' + token)
                    .send(cleaner)
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('Record already exists');
                        done();
                    });
            });
        });

        it('it should not add a cleaner without quality score', (done) => {
            let cleaner = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                cities: [testCities[0]._id, testCities[1]._id]
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('qualityScore');
                    res.body.error[0].should.have.property('message').equal('Quality score is required');
                    done();
                });
        });

        it('it should not add a cleaner with quality score contain symbols', (done) => {
            let cleaner = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 'A'
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('qualityScore');
                    res.body.error[0].should.have.property('message').equal('Quality score must be between 0 and 5');
                    done();
                });
        });

        it('it should not add a cleaner with quality score < 0', (done) => {
            let cleaner = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: -1
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('qualityScore');
                    res.body.error[0].should.have.property('message').equal('Quality score must be between 0 and 5');
                    done();
                });
        });

        it('it should not add a cleaner with quality score > 5', (done) => {
            let cleaner = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 5.1
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('qualityScore');
                    res.body.error[0].should.have.property('message').equal('Quality score must be between 0 and 5');
                    done();
                });
        });

        it('it should add a cleaner with cyrillic firstname', (done) => {
            let cleaner = {
                email: 'valid@mail.com',
                firstName: 'Иван',
                lastName: 'Some-Name',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 4.2
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Иван');
                    res.body.should.have.property('lastName').equal('Some-Name');
                    res.body.should.have.property('cities');
                    res.body.cities.should.be.a('array');
                    res.body.cities.length.should.equal(2);
                    res.body.should.have.property('qualityScore').equal(4.2);
                    done();
                });
        });

        it('it should add a cleaner with lastname like "Some-Name"', (done) => {
            let cleaner = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Some-Name',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 4.2
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Firstname');
                    res.body.should.have.property('lastName').equal('Some-Name');
                    res.body.should.have.property('cities');
                    res.body.cities.should.be.a('array');
                    res.body.cities.length.should.equal(2);
                    res.body.should.have.property('qualityScore').equal(4.2);
                    done();
                });
        });

        it('it should add a cleaner with lastname like "d\'Arc"', (done) => {
            let cleaner = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'd\'Arc',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 4.2
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Firstname');
                    res.body.should.have.property('lastName').equal('d\'Arc');
                    res.body.should.have.property('cities');
                    res.body.cities.should.be.a('array');
                    res.body.cities.length.should.equal(2);
                    res.body.should.have.property('qualityScore').equal(4.2);
                    done();
                });
        });

        it('it should add a cleaner with cyrillic lastname', (done) => {
            let cleaner = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Иванов',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 4.2
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Firstname');
                    res.body.should.have.property('lastName').equal('Иванов');
                    res.body.should.have.property('cities');
                    res.body.cities.should.be.a('array');
                    res.body.cities.length.should.equal(2);
                    res.body.should.have.property('qualityScore').equal(4.2);
                    done();
                });
        });

        it('it should trim all cleaner data', (done) => {
            let cleaner = {
                email: '   valid@mail.com   ',
                firstName: '   Firstname   ',
                lastName: '   Lastname   ',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: '   4.2   '
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Firstname');
                    res.body.should.have.property('lastName').equal('Lastname');
                    res.body.should.have.property('cities');
                    res.body.cities.should.be.a('array');
                    res.body.cities.length.should.equal(2);
                    res.body.should.have.property('qualityScore').equal(4.2);
                    done();
                });
        });

        it('it should add a cleaner with valid data', (done) => {
            let cleaner = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 4.2
            };
            chai.request(app)
                .post('/api/cleaner')
                .set('Authorization', 'Bearer ' + token)
                .send(cleaner)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Firstname');
                    res.body.should.have.property('lastName').equal('Lastname');
                    res.body.should.have.property('cities');
                    res.body.cities.should.be.a('array');
                    res.body.cities.length.should.equal(2);
                    res.body.should.have.property('qualityScore').equal(4.2);
                    done();
                });
        });

    });

    describe('Get cleaner by ID', () => {

        it('it should GET cleaner by valid ID', (done) => {
            const newCleaner = new Cleaner({
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 4.2
            });
            newCleaner.save((err, cleaner) => {
                chai.request(app)
                    .get('/api/cleaner/' + cleaner._id)
                    .set('Authorization', 'Bearer ' + token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('email').equal('valid@mail.com');
                        res.body.should.have.property('firstName').equal('Firstname');
                        res.body.should.have.property('lastName').equal('Lastname');
                        res.body.should.have.property('cities');
                        res.body.cities.should.be.a('array');
                        res.body.cities.length.should.equal(2);
                        res.body.should.have.property('qualityScore').equal(4.2);
                        done();
                    });
            });
        });

        it('it should fail GET cleaner by invalid ID', (done) => {
            chai.request(app)
                .get('/api/cleaner/invalid')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });
    });

    describe('Edit cleaner', () => {

        it('it should update cleaner by valid ID with valid data', (done) => {
            const cleaner = new Cleaner({
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 4.2
            });
            cleaner.save((err, cleaner) => {
                chai.request(app)
                    .put('/api/cleaner/' + cleaner._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid_new@mail.com',
                        firstName: 'FirstNewname',
                        lastName: 'LastNewname',
                        cities: [testCities[0]._id],
                        qualityScore: 4.0
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('email').equal('valid_new@mail.com');
                        res.body.should.have.property('firstName').equal('FirstNewname');
                        res.body.should.have.property('lastName').equal('LastNewname');
                        res.body.cities.should.be.a('array');
                        res.body.cities.length.should.equal(1);
                        res.body.should.have.property('qualityScore').equal(4.0);
                        done();
                    });
            });
        });

        it('it should fail update cleaner by invalid ID', (done) => {
            chai.request(app)
                .put('/api/cleaner/invalid')
                .set('Authorization', 'Bearer ' + token)
                .send({
                    email: 'valid@mail.com',
                    firstName: 'Firstname',
                    lastName: 'Lastname',
                    cities: [testCities[0]._id, testCities[1]._id],
                    qualityScore: 4.2
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });

        it('it should fail update cleaner by valid ID with invalid cleaner firstname', (done) => {
            const cleaner = new Cleaner({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.6,
                cities: [testCities[0]._id]
            });
            cleaner.save((err, cleaner) => {
                chai.request(app)
                    .put('/api/cleaner/' + cleaner._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid2@mail.com',
                        firstName: 'F',
                        lastName: 'Lastname',
                        qualityScore: 1.6,
                        cities: [testCities[0]._id]
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('firstName');
                        res.body.error[0].should.have.property('message').equal('First name must be between 2 and 100 chars long');
                        done();
                    });
            });
        });

        it('it should fail update cleaner by valid ID with invalid cleaner lastname', (done) => {
            const cleaner = new Cleaner({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.6,
                cities: [testCities[0]._id]
            });
            cleaner.save((err, cleaner) => {
                chai.request(app)
                    .put('/api/cleaner/' + cleaner._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid2@mail.com',
                        firstName: 'Firstname',
                        lastName: 'L',
                        qualityScore: 1.6,
                        cities: [testCities[0]._id]
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('lastName');
                        res.body.error[0].should.have.property('message').equal('Last name must be between 2 and 100 chars long');
                        done();
                    });
            });
        });

        it('it should fail update cleaner by valid ID with invalid cleaner email', (done) => {
            const cleaner = new Cleaner({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.6,
                cities: [testCities[0]._id]
            });
            cleaner.save((err, cleaner) => {
                chai.request(app)
                    .put('/api/cleaner/' + cleaner._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'invalid',
                        firstName: 'Firstname',
                        lastName: 'Lastname',
                        qualityScore: 1.6,
                        cities: [testCities[0]._id]
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('email');
                        res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                        done();
                    });
            });
        });

        it('it should fail update cleaner by valid ID with existing email', (done) => {
            const cleaner1 = new Cleaner({
                email: 'valid1@mail.com',
                firstName: 'First',
                lastName: 'Cleaner',
                qualityScore: 1.6,
                cities: [testCities[0]._id]
            });

            const cleaner2 = new Cleaner({
                email: 'valid2@mail.com',
                firstName: 'Second',
                lastName: 'Cleaner',
                qualityScore: 3.2,
                cities: [testCities[0]._id]
            });
            cleaner1.save((err, cleaner) => {
                cleaner2.save((err, cleaner) => {
                    chai.request(app)
                        .put('/api/cleaner/' + cleaner._id)
                        .set('Authorization', 'Bearer ' + token)
                        .send({
                            email: 'valid1@mail.com',
                            firstName: 'Second',
                            lastName: 'Cleaner',
                            qualityScore: 3.2,
                            cities: [testCities[0]._id]
                        })
                        .end((err, res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            res.body.should.have.property('error').equal('Record already exists');
                            done();
                        });
                });
            });
        });

        it('it should fail update cleaner by valid ID with cleaner quality score < 0', (done) => {
            const cleaner = new Cleaner({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.6,
                cities: [testCities[0]._id]
            });
            cleaner.save((err, cleaner) => {
                chai.request(app)
                    .put('/api/cleaner/' + cleaner._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid2@mail.com',
                        firstName: 'Firstname',
                        lastName: 'Lastname',
                        qualityScore: -1,
                        cities: [testCities[0]._id]
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('qualityScore');
                        res.body.error[0].should.have.property('message').equal('Quality score must be between 0 and 5');
                        done();
                    });
            });
        });

        it('it should fail update cleaner by valid ID with cleaner quality score > 5', (done) => {
            const cleaner = new Cleaner({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                qualityScore: 1.6,
                cities: [testCities[0]._id]
            });
            cleaner.save((err, cleaner) => {
                chai.request(app)
                    .put('/api/cleaner/' + cleaner._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid2@mail.com',
                        firstName: 'Firstname',
                        lastName: 'Lastname',
                        qualityScore: 5.1,
                        cities: [testCities[0]._id]
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('qualityScore');
                        res.body.error[0].should.have.property('message').equal('Quality score must be between 0 and 5');
                        done();
                    });
            });
        });
    });

    describe('Delete cleaner', () => {

        it('it should delete cleaner by valid ID', (done) => {
            const cleaner = new Cleaner({
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                cities: [testCities[0]._id, testCities[1]._id],
                qualityScore: 4.2
            });
            cleaner.save((err, cleaner) => {
                chai.request(app)
                    .delete('/api/cleaner/' + cleaner._id)
                    .set('Authorization', 'Bearer ' + token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('email').equal('valid@mail.com');
                        res.body.should.have.property('firstName').equal('Firstname');
                        res.body.should.have.property('lastName').equal('Lastname');
                        res.body.should.have.property('cities');
                        res.body.cities.should.be.a('array');
                        res.body.cities.length.should.equal(2);
                        res.body.should.have.property('qualityScore').equal(4.2);
                        done();
                    });
            });
        });

        it('it should fail delete cleaner by invalid ID', (done) => {
            chai.request(app)
                .delete('/api/cleaner/invalid')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });
    });
});