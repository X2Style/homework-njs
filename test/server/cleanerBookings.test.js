process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
let app = require('../../server/app').default;
let should = chai.should();
let User = require('../../server/models/user').default;
let Booking = require('../../server/models/booking').default;
let Customer = require('../../server/models/customer').default;
let Cleaner = require('../../server/models/cleaner').default;
let City = require('../../server/models/city').default;
let config = require('../../config/config').default;
let moment = require('moment');

chai.use(chaiHttp);

describe('Cleaner Bookings', () => {


    const testCities = [];
    const testCleaners = [];
    const testCustomers = [];
    let token = '';

    before((done) => {

        const user = new User({
            name: 'Test',
            email: 'test@test.com',
            password: 'testtest'
        });

        user.save((err, user) => {
            chai.request(app)
                .post('/api/login')
                .send({
                    email: 'test@test.com',
                    password: 'testtest'
                })
                .end((err, res) => {
                    token = res.body.token;

                    const city1 = new City({
                        name: 'Kyiv',
                        priceFactor: 2.8
                    });

                    const city2 = new City({
                        name: 'Dnipro',
                        priceFactor: 1.6
                    });

                    city1.save((err, city) => {
                        testCities.push(city);
                        city2.save((err, city) => {
                            testCities.push(city);

                            const cleaner1 = new Cleaner({
                                email: 'valid1@mail.com',
                                firstName: 'First',
                                lastName: 'Cleaner',
                                cities: [testCities[0]._id, testCities[1]._id],
                                qualityScore: 4.2
                            });

                            const cleaner2 = new Cleaner({
                                email: 'valid2@mail.com',
                                firstName: 'Second',
                                lastName: 'Cleaner',
                                cities: [testCities[0]._id, testCities[1]._id],
                                qualityScore: 3.2
                            });

                            cleaner1.save((err, cleaner) => {
                                testCleaners.push(cleaner);
                                cleaner2.save((err, cleaner) => {
                                    testCleaners.push(cleaner);

                                    const customer1 = new Customer({
                                        firstName: 'First',
                                        lastName: 'Customer',
                                        email: 'first@customer.com',
                                        phone: '(067) 111-11-11'
                                    });
                                    customer1.save((err, customer) => {
                                        testCustomers.push(customer);
                                        done();
                                    });
                                });
                            });
                        });
                    });
                });
        });
    });

    after((done) => {
        User.remove({}, (err) => {
            City.remove({}, (err) => {
                Cleaner.remove({}, (err) => {
                    Customer.remove({}, (err) => {
                        done();
                    });
                });
            });
        });
    });

    beforeEach((done) => {
        Booking.remove({}, (err) => {
            done();
        });
    });

    describe('Get bookings', () => {

        it('it should get all cleaner bookings', (done) => {
            const booking1 = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            const booking2 = new Booking({
                dateFrom: moment().add(4, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(5, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            booking1.save((err, booking) => {
                booking2.save((err, booking) => {
                    chai.request(app)
                        .get('/api/cleaner/' + testCleaners[0]._id + '/bookings')
                        .set('Authorization', 'Bearer ' + token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.to.equal(2);
                            res.body[0].should.have.property('cleaner');
                            res.body[0].cleaner.should.have.property('email').equal(testCleaners[0].email);
                            res.body[1].should.have.property('cleaner');
                            res.body[1].cleaner.should.have.property('email').equal(testCleaners[0].email);
                            done();
                        })
                });
            });
        });

        it('it should get all cleaner bookings (no bookings for this cleaner)', (done) => {
            chai.request(app)
                .get('/api/cleaner/' + testCleaners[0]._id + '/bookings')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.to.equal(0);
                    done();
                })
        });

        it('it should fail get all cleaner bookings by invalid cleaner ID', (done) => {
            chai.request(app)
                .get('/api/cleaner/invalid/bookings')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                })
        });
    });
});