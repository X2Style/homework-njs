process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
let app = require('../../server/app').default;
let should = chai.should();
let User = require('../../server/models/user').default;
let Customer = require('../../server/models/customer').default;
let Cleaner = require('../../server/models/cleaner').default;
let City = require('../../server/models/city').default;
let Booking = require('../../server/models/booking').default;
let config = require('../../config/config').default;

chai.use(chaiHttp);

//Our parent block
describe('Customers', () => {

    let token = '';

    before((done) => {
        const user = new User({
            name: 'Test',
            email: 'test@test.com',
            password: 'testtest'
        });

        user.save((err, user) => {
            chai.request(app)
                .post('/api/login')
                .send({
                    email: 'test@test.com',
                    password: 'testtest'
                })
                .end((err, res) => {
                    token = res.body.token;
                    done();
                });
        });
    });

    after((done) => {
        User.remove({}, (err) => {
            done();
        })
    });

    beforeEach((done) => {
        Customer.remove({}, (err) => {
            done();
        });
    });

    describe('Get all customers', () => {
        it('it should GET all the customers', (done) => {
            chai.request(app)
                .get('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.to.equal(0);
                    done();
                });
        });

        it('it should add 2 customers and get all', (done) => {
            const customer1 = new Customer({
                email: 'valid1@mail.com',
                firstName: 'First',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            const customer2 = new Customer({
                email: 'valid2@mail.com',
                firstName: 'Second',
                lastName: 'Lastname',
                phone: '(067) 222-22-22'
            });
            customer1.save((err, customer) => {
                customer2.save((err, customer) => {
                    chai.request(app)
                        .get('/api/customer')
                        .set('Authorization', 'Bearer ' + token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.to.equal(2);
                            done();
                        });
                });
            });
        });
    });

    describe('Create customer', () => {

        it('it should not add a customer without firstname', (done) => {
            let customer = {
                email: 'valid2@mail.com',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is required');
                    done();
                });
        });

        it('it should not add a customer with firstname length < 2', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'F',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a customer with firstname length > 100', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'Abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijz',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a customer with firstname contains digits', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'First12',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a customer with firstname contains forbidden symbols', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'Fir$t',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        // it('it should not add a customer with firstname contains -', (done) => {
        //     let customer = {
        //         email: 'valid1@mail.com',
        //         firstName: 'Fir-st',
        //         lastName: 'Lastname',
        //         phone: '(067) 111-11-11'
        //     };
        //     chai.request(app)
        //         .post('/api/customer')
        //         .set('Authorization', 'Bearer ' + token)
        //         .send(customer)
        //         .end((err, res) => {
        //             res.should.have.status(400);
        //             res.body.should.be.a('object');
        //             res.body.should.have.property('error');
        //             res.body.error.should.be.a('array');
        //             res.body.error[0].should.be.a('object');
        //             res.body.error[0].should.have.property('name').equal('firstName');
        //             res.body.error[0].should.have.property('message').equal('First name is invalid');
        //             done();
        //         });
        // });

        it('it should not add a customer with firstname contains \'\'', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'Fir\'\'st',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a customer with firstname begins with \'', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: '\'First',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a customer with firstname begins with -', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: '-First',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a customer with firstname ends with -', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'First-',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a customer without lastname', (done) => {
            let customer = {
                email: 'valid2@mail.com',
                firstName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is required');
                    done();
                });
        });

        it('it should not add a customer with lastname length < 2', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: 'L',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a customer with lastname length > 100', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'Firstname',
                lastName: 'Abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijz',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a customer with lastname contains digits', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'First',
                lastName: 'Las1name',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a customer with lastname contains forbidden symbols', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'First',
                lastName: 'L@$tname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a customer with lastname contains \'\'', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'First',
                lastName: 'Last\'\'name',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a customer with lastname begins with \'', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'First',
                lastName: '\'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a customer with lastname begins with -', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'First',
                lastName: '-Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a customer with lastname ends with -', (done) => {
            let customer = {
                email: 'valid1@mail.com',
                firstName: 'First',
                lastName: 'Lastname-',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a customer without email', (done) => {
            let customer = {
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('E-mail is required');
                    done();
                });
        });

        it('it should not add a customer with invalid email like "Abc.example.com"', (done) => {
            let customer = {
                email: 'Abc.example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with invalid email like "A@b@c@example.com"', (done) => {
            let customer = {
                email: 'A@b@c@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with invalid email like "just"not"right@example.com"', (done) => {
            let customer = {
                email: 'just"not"right@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with invalid email like "a"b(c)d,e:f;g<h>i[j\k]l@example.com"', (done) => {
            let customer = {
                email: 'a"b(c)d,e:f;g<h>i[j\k]l@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with invalid email like "this is"not\allowed@example.com"', (done) => {
            let customer = {
                email: 'this is"not\allowed@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with invalid email like "this\ still\"not\\allowed@example.com"', (done) => {
            let customer = {
                email: 'this\ still\"not\\allowed@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with invalid email like "john..doe@example.com"', (done) => {
            let customer = {
                email: 'john..doe@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with invalid email like ".john.doe@example.com"', (done) => {
            let customer = {
                email: '.john.doe@example.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with invalid email like "example@localhost"', (done) => {
            let customer = {
                email: 'example@localhost',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with invalid email like "john.doe@example..com"', (done) => {
            let customer = {
                email: 'john.doe@example..com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a customer with same email', (done) => {
            const customer = new Customer({
                email: 'double@email.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            customer.save((err, customer) => {
                chai.request(app)
                    .post('/api/customer/')
                    .set('Authorization', 'Bearer ' + token)
                    .send(customer)
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('Record already exists');
                        done();
                    });
            });
        });

        it('it should not add a customer with invalid phone', (done) => {
            let customer = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '123-45-67'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('phone');
                    res.body.error[0].should.have.property('message').equal('This is not a valid phone number');
                    done();
                });
        });

        it('it should not add a customer with phone contain symbols', (done) => {
            let customer = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) abc-de-fg'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('phone');
                    res.body.error[0].should.have.property('message').equal('This is not a valid phone number');
                    done();
                });
        });

        it('it should add a customer with cyrillic firstname', (done) => {
            let customer = {
                email: 'valid@mail.com',
                firstName: 'Иван',
                lastName: "Last",
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Иван');
                    res.body.should.have.property('lastName').equal("Last");
                    res.body.should.have.property('phone').equal('(067) 111-11-11');
                    done();
                });
        });

        it('it should add a customer with lastname like "Some-Name"', (done) => {
            let customer = {
                email: 'valid@mail.com',
                firstName: 'First',
                lastName: 'Some-Name',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('First');
                    res.body.should.have.property('lastName').equal('Some-Name');
                    res.body.should.have.property('phone').equal('(067) 111-11-11');
                    done();
                });
        });

        it('it should add a customer with lastname like "d\'Arc"', (done) => {
            let customer = {
                email: 'valid@mail.com',
                firstName: 'First',
                lastName: "d'Arc",
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('First');
                    res.body.should.have.property('lastName').equal("d'Arc");
                    res.body.should.have.property('phone').equal('(067) 111-11-11');
                    done();
                });
        });

        it('it should add a customer with cyrillic lastname', (done) => {
            let customer = {
                email: 'valid@mail.com',
                firstName: 'First',
                lastName: "Иванов",
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('First');
                    res.body.should.have.property('lastName').equal("Иванов");
                    res.body.should.have.property('phone').equal('(067) 111-11-11');
                    done();
                });
        });

        it('it should add a customer with valid data', (done) => {
            let customer = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Firstname');
                    res.body.should.have.property('lastName').equal('Lastname');
                    res.body.should.have.property('phone').equal('(067) 111-11-11');
                    done();
                });
        });

        it('it should trim all customer data', (done) => {
            let customer = {
                email: '   valid@mail.com   ',
                firstName: '   Firstname   ',
                lastName: '   Lastname   ',
                phone: '   (067) 111-11-11   '
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Firstname');
                    res.body.should.have.property('lastName').equal('Lastname');
                    res.body.should.have.property('phone').equal('(067) 111-11-11');
                    done();
                });
        });

        it('it should add a customer with valid data and without phone', (done) => {
            let customer = {
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname'
            };
            chai.request(app)
                .post('/api/customer')
                .set('Authorization', 'Bearer ' + token)
                .send(customer)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').equal('valid@mail.com');
                    res.body.should.have.property('firstName').equal('Firstname');
                    res.body.should.have.property('lastName').equal('Lastname');
                    res.body.should.not.have.property('phone');
                    done();
                });
        });

    });

    describe('Get customer by ID', () => {

        it('it should GET customer by valid ID', (done) => {
            const newCustomer = new Customer({
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            newCustomer.save((err, customer) => {
                chai.request(app)
                    .get('/api/customer/' + customer._id)
                    .set('Authorization', 'Bearer ' + token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('email').equal('valid@mail.com');
                        res.body.should.have.property('firstName').equal('Firstname');
                        res.body.should.have.property('lastName').equal('Lastname');
                        res.body.should.have.property('phone').equal('(067) 111-11-11');
                        done();
                    });
            });
        });

        it('it should fail GET customer by invalid ID', (done) => {
            chai.request(app)
                .get('/api/customer/invalid')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });
    });

    describe('Edit customer', () => {

        it('it should update customer by valid ID with valid data', (done) => {
            const customer = new Customer({
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            customer.save((err, customer) => {
                chai.request(app)
                    .put('/api/customer/' + customer._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid_new@mail.com',
                        firstName: 'FirstNewname',
                        lastName: 'LastNewname',
                        phone: '(067) 222-22-22'
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('email').equal('valid_new@mail.com');
                        res.body.should.have.property('firstName').equal('FirstNewname');
                        res.body.should.have.property('lastName').equal('LastNewname');
                        res.body.should.have.property('phone').equal('(067) 222-22-22');
                        done();
                    });
            });
        });

        it('it should fail update customer by invalid ID', (done) => {
            chai.request(app)
                .put('/api/customer/invalid')
                .set('Authorization', 'Bearer ' + token)
                .send({
                    email: 'valid@mail.com',
                    firstName: 'Firstname',
                    lastName: 'Lastname',
                    phone: '(067) 111-11-11'
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });

        it('it should fail update customer by valid ID with invalid customer firstname', (done) => {
            const customer = new Customer({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            customer.save((err, customer) => {
                chai.request(app)
                    .put('/api/customer/' + customer._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid2@mail.com',
                        firstName: 'F',
                        lastName: 'Lastname',
                        phone: '(067) 111-11-11'
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('firstName');
                        res.body.error[0].should.have.property('message').equal('First name must be between 2 and 100 chars long');
                        done();
                    });
            });
        });

        it('it should fail update customer by valid ID with invalid customer lastname', (done) => {
            const customer = new Customer({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            customer.save((err, customer) => {
                chai.request(app)
                    .put('/api/customer/' + customer._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid2@mail.com',
                        firstName: 'Firstname',
                        lastName: 'L',
                        phone: '(067) 111-11-11'
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('lastName');
                        res.body.error[0].should.have.property('message').equal('Last name must be between 2 and 100 chars long');
                        done();
                    });
            });
        });

        it('it should fail update customer by valid ID with invalid customer email', (done) => {
            const customer = new Customer({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            customer.save((err, customer) => {
                chai.request(app)
                    .put('/api/customer/' + customer._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'invalid',
                        firstName: 'Firstname',
                        lastName: 'Lastname',
                        phone: '(067) 111-11-11'
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('email');
                        res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                        done();
                    });
            });
        });

        it('it should fail update customer by valid ID with existing email', (done) => {
            const customer1 = new Customer({
                email: 'valid1@mail.com',
                firstName: 'First',
                lastName: 'Customer',
                phone: '(067) 111-11-11'
            });

            const customer2 = new Customer({
                email: 'valid2@mail.com',
                firstName: 'Second',
                lastName: 'Customer',
                phone: '(067) 222-22-22'
            });
            customer1.save((err, customer) => {
                customer2.save((err, customer) => {
                    chai.request(app)
                        .put('/api/customer/' + customer._id)
                        .set('Authorization', 'Bearer ' + token)
                        .send({
                            email: 'valid1@mail.com',
                            firstName: 'Second',
                            lastName: 'Customer',
                            phone: '(067) 222-22-22'
                        })
                        .end((err, res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            res.body.should.have.property('error').equal('Record already exists');
                            done();
                        });
                });
            });
        });

        it('it should update customer by valid ID with empty phone', (done) => {
            const customer = new Customer({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            customer.save((err, customer) => {
                chai.request(app)
                    .put('/api/customer/' + customer._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid_new@mail.com',
                        firstName: 'FirstNewname',
                        lastName: 'LastNewname'
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('email').equal('valid_new@mail.com');
                        res.body.should.have.property('firstName').equal('FirstNewname');
                        res.body.should.have.property('lastName').equal('LastNewname');
                        res.body.should.not.have.property('phone');
                        done();
                    });
            });
        });

        it('it should fail update customer by valid ID and add phone', (done) => {
            const customer = new Customer({
                email: 'valid2@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname'
            });
            customer.save((err, customer) => {
                chai.request(app)
                    .put('/api/customer/' + customer._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        email: 'valid_new@mail.com',
                        firstName: 'FirstNewname',
                        lastName: 'LastNewname',
                        phone: '(067) 111-11-11'
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('email').equal('valid_new@mail.com');
                        res.body.should.have.property('firstName').equal('FirstNewname');
                        res.body.should.have.property('lastName').equal('LastNewname');
                        res.body.should.have.property('phone').equal('(067) 111-11-11');
                        done();
                    });
            });
        });
    });

    describe('Delete customer', () => {

        it('it should delete customer by valid ID', (done) => {
            const customer = new Customer({
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            customer.save((err, customer) => {
                chai.request(app)
                    .delete('/api/customer/' + customer._id)
                    .set('Authorization', 'Bearer ' + token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('email').equal('valid@mail.com');
                        res.body.should.have.property('firstName').equal('Firstname');
                        res.body.should.have.property('lastName').equal('Lastname');
                        res.body.should.have.property('phone').equal('(067) 111-11-11');
                        done();
                    });
            });
        });

        it('it should fail delete customer by invalid ID', (done) => {
            chai.request(app)
                .delete('/api/customer/invalid')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });
    });
});