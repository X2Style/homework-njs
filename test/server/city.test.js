process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
let app = require('../../server/app').default;
let should = chai.should();
let City = require('../../server/models/city').default;
let User = require('../../server/models/user').default;
let config = require('../../config/config').default;


chai.use(chaiHttp);

//Our parent block
describe('Cities', () => {

    let token = '';

    before((done) => {
        const user = new User({
            name: 'Test',
            email: 'test@test.com',
            password: 'testtest'
        });

        user.save((err, user) => {
            chai.request(app)
                .post('/api/login')
                .send({
                    email: 'test@test.com',
                    password: 'testtest'
                })
                .end((err, res) => {
                    token = res.body.token;
                    done();
                });
        });
    });

    after((done) => {
            User.remove({}, (err) => {
                done();
            })
        });

    beforeEach((done) => {
        City.remove({}, (err) => {
            done();
        });
    });

    describe('Get all cities', () => {

        it('it should GET all the cities', (done) => {
            chai.request(app)
                .get('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.to.equal(0);
                    done();
                });
        });

        it('it should add 2 cities and get all', (done) => {
            const city1 = new City({
                name: 'Cityname1',
                priceFactor: 1.5
            });
            const city2 = new City({
                name: 'Cityname2',
                priceFactor: 1.6
            });
            city1.save((err, city) => {
                city2.save((err, city) => {
                    chai.request(app)
                        .get('/api/city')
                        .set('Authorization', 'Bearer ' + token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.to.equal(2);
                            done();
                        });
                });
            });
        });
    });

    describe('Create city', () => {
        it('it should not add a city without name', (done) => {
            let city = {
                priceFactor: 1
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('name');
                    res.body.error[0].should.have.property('message').equal('Name is required');
                    done();
                });
        });

        it('it should not add a city with name length < 2', (done) => {
            let city = {
                name: 'A',
                priceFactor: 1
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('name');
                    res.body.error[0].should.have.property('message').equal('City name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a city with name length > 100', (done) => {
            let city = {
                name: 'Abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijz',
                priceFactor: 1
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('name');
                    res.body.error[0].should.have.property('message').equal('City name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a city with name contain digits', (done) => {
            let city = {
                name: '123',
                priceFactor: 1
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('name');
                    res.body.error[0].should.have.property('message').equal('City name is invalid');
                    done();
                });
        });

        it('it should not add a city with name ends with -', (done) => {
            let city = {
                name: 'Cityname-',
                priceFactor: 1
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('name');
                    res.body.error[0].should.have.property('message').equal('City name is invalid');
                    done();
                });
        });

        it('it should not add a city without price factor', (done) => {
            let city = {
                name: 'Cityname'
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('priceFactor');
                    res.body.error[0].should.have.property('message').equal('Price factor is required');
                    done();
                });
        });

        it('it should not add a city with price factor < 0.1', (done) => {
            let city = {
                name: 'Cityname',
                priceFactor: 0
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('priceFactor');
                    res.body.error[0].should.have.property('message').equal('Price factor must be greater then 0');
                    done();
                });
        });

        it('it should fail add city with same name', (done) => {
            const city = new City({
                name: 'Cityname',
                priceFactor: 1.5
            });
            city.save((err, city) => {
                chai.request(app)
                    .post('/api/city/')
                    .set('Authorization', 'Bearer ' + token)
                    .send(city)
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('Record already exists');
                        done();
                    });
            });
        });

        it('it should add a city with trimmed name', (done) => {
            let city = {
                name: '   Cityname   ',
                priceFactor: 1.5
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').equal('Cityname');
                    res.body.should.have.property('priceFactor').equal(1.5);
                    done();
                });
        });

        it('it should add a city with name like "New York"', (done) => {
            let city = {
                name: 'New York',
                priceFactor: 1.5
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').equal('New York');
                    res.body.should.have.property('priceFactor').equal(1.5);
                    done();
                });
        });

        it('it should add a city with name like "Melitopol\'"', (done) => {
            let city = {
                name: 'Melitopol\'',
                priceFactor: 1.5
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').equal('Melitopol\'');
                    res.body.should.have.property('priceFactor').equal(1.5);
                    done();
                });
        });

        it('it should add a city with name like "Ivano-Frankivsk"', (done) => {
            let city = {
                name: 'Ivano-Frankivsk',
                priceFactor: 1.5
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').equal("Ivano-Frankivsk");
                    res.body.should.have.property('priceFactor').equal(1.5);
                    done();
                });
        });

        it('it should add a city with name like "Mohyliv-Podil\'s\'kyi"', (done) => {
            let city = {
                name: "Mohyliv-Podil's'kyi",
                priceFactor: 1.5
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').equal("Mohyliv-Podil's'kyi");
                    res.body.should.have.property('priceFactor').equal(1.5);
                    done();
                });
        });

        it('it should add a city with name like "Дніпро"', (done) => {
            let city = {
                name: "Дніпро",
                priceFactor: 1.5
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').equal("Дніпро");
                    res.body.should.have.property('priceFactor').equal(1.5);
                    done();
                });
        });

        it('it should add a city with trimmed price factor', (done) => {
            let city = {
                name: '   Cityname   ',
                priceFactor: '   1.5   '
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').equal('Cityname');
                    res.body.should.have.property('priceFactor').equal(1.5);
                    done();
                });
        });

        it('it should add a city with valid data', (done) => {
            let city = {
                name: 'Cityname',
                priceFactor: 1.5
            };
            chai.request(app)
                .post('/api/city')
                .set('Authorization', 'Bearer ' + token)
                .send(city)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').equal('Cityname');
                    res.body.should.have.property('priceFactor').equal(1.5);
                    done();
                });
        });

    });

    describe('Get city by ID', () => {

        it('it should GET city by valid ID', (done) => {
            const city = new City({
                name: 'Cityname',
                priceFactor: 1.5
            });
            city.save((err, city) => {
                chai.request(app)
                    .get('/api/city/' + city._id)
                    .set('Authorization', 'Bearer ' + token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('name').equal('Cityname');
                        res.body.should.have.property('priceFactor').equal(1.5);
                        done();
                    });
            });
        });

        it('it should fail GET city by invalid ID', (done) => {
            chai.request(app)
                .get('/api/city/invalid')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });
    });

    describe('Edit city', () => {

        it('it should update city by valid ID with valid data', (done) => {
            const city = new City({
                name: 'Cityname',
                priceFactor: 1.5
            });
            city.save((err, city) => {
                chai.request(app)
                    .put('/api/city/' + city._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        name: 'Newcityname',
                        priceFactor: 1.4
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('name').equal('Newcityname');
                        res.body.should.have.property('priceFactor').equal(1.4);
                        done();
                    });
            });
        });

        it('it should fail put city by invalid ID', (done) => {
            chai.request(app)
                .put('/api/city/invalid')
                .set('Authorization', 'Bearer ' + token)
                .send({
                    name: 'Newcityname',
                    priceFactor: 1.4
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });

        it('it should fail update city with existing name', (done) => {
            const city1 = new City({
                name: 'Cityname',
                priceFactor: 1.5
            });
            const city2 = new City({
                name: 'City-Newname',
                priceFactor: 1.4
            });
            city1.save((err, city1) => {
                city2.save((err, city2) => {
                    chai.request(app)
                        .put('/api/city/' + city1._id)
                        .set('Authorization', 'Bearer ' + token)
                        .send({
                            name: city2.name,
                            priceFactor: 1.4
                        })
                        .end((err, res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            res.body.should.have.property('error').equal('Record already exists');
                            done();
                        });
                });
            });
        });

        it('it should fail update city by valid ID with invalid city name', (done) => {
            const city = new City({
                name: 'Cityname',
                priceFactor: 1.5
            });
            city.save((err, city) => {
                chai.request(app)
                    .put('/api/city/' + city._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        name: 'N',
                        priceFactor: 1.4
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('name');
                        res.body.error[0].should.have.property('message').equal('City name must be between 2 and 100 chars long');
                        done();
                    });
            });
        });

        it('it should fail update city by valid ID with invalid city price factor', (done) => {
            const city = new City({
                name: 'Cityname',
                priceFactor: 1.5
            });
            city.save((err, city) => {
                chai.request(app)
                    .put('/api/city/' + city._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        name: 'Cityname',
                        priceFactor: 0
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error[0].should.be.a('object');
                        res.body.error[0].should.have.property('name').equal('priceFactor');
                        res.body.error[0].should.have.property('message').equal('Price factor must be greater then 0');
                        done();
                    });
            });
        });
    });

    describe('Delete city', () => {

        it('it should delete city by valid ID', (done) => {
            const city = new City({
                name: 'Cityname',
                priceFactor: 1.5
            });
            city.save((err, city) => {
                chai.request(app)
                    .delete('/api/city/' + city._id)
                    .set('Authorization', 'Bearer ' + token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('name').equal('Cityname');
                        res.body.should.have.property('priceFactor').equal(1.5);
                        done();
                    });
            });
        });

        it('it should fail delete city by invalid ID', (done) => {
            chai.request(app)
                .delete('/api/city/invalid')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });
    });
});