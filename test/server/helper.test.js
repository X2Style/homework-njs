process.env.NODE_ENV = 'test';
let chai = require('chai');
let should = chai.should();
let moment = require('moment');
let minDate = require('../../server/helper').minDate;
let mongooseErrorFormat = require('../../server/helper').mongooseErrorFormat;

describe('Helper functions', function () {

    it('should return min date + 2 hours', (done) => {
        const testDate = moment().minutes(31).seconds(0).milliseconds(0);
        const result = moment(minDate(testDate)).toISOString();
        result.should.be.equal(testDate.minutes(0).add('2', 'h').toISOString());
        done();
    });

    it('should return min date + 1 hours', (done) => {
        const testDate = moment().minutes(29).seconds(0).milliseconds(0);
        const result = moment(minDate(testDate)).toISOString();
        result.should.be.equal(testDate.minutes(0).add('1', 'h').toISOString());
        done();
    });

    it('should return formatted error from mongoose', (done) => {
        const mongooseError = {
            errors: {
                priceFactor: {
                    message: 'Price factor is required',
                    name: 'ValidatorError',
                    properties: '[Object]',
                    kind: 'required',
                    path: 'priceFactor',
                    value: undefined,
                    reason: undefined
                }
            },
            message: 'City validation failed',
            name: 'ValidationError'
        };
        const formattedError = mongooseErrorFormat(mongooseError);
        formattedError.should.be.a('array');
        formattedError[0].should.be.a('object');
        formattedError[0].should.have.property('name').equal('priceFactor');
        formattedError[0].should.have.property('message').equal('Price factor is required');
        done();
    });

});
