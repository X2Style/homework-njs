process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
let app = require('../../server/app').default;
let should = chai.should();
let User = require('../../server/models/user').default;


describe('Authorization', function () {

    before((done) => {
        const user = new User({
            name: 'Test',
            email: 'test@test.com',
            password: 'testtest'
        });

        user.save((err, user) => {
            done();
        });
    });

    after(
        (done) => {
            User.remove({}, (err) => {
                done();
            })
        });

    it('should pass unsecured without token', (done) => {
        chai.request(app)
            .get('/api')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status').equal('Ok');
                done();
            });
    });

    it('should login with valid data', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'test@test.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(true);
                res.body.should.have.property('message').equal('You have successfully logged in!');
                res.body.should.have.property('token');
                res.body.user.should.be.a('object');
                res.body.user.should.have.property('name').equal('Test');
                done();
            });
    });

    it('should fail login with invalid password', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'test@test.com',
                password: 'invalidpassword'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Incorrect email or password');
                done();
            });
    });

    it('should fail login with invalid email', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'invalid@test.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Incorrect email or password');
                done();
            });
    });

    it('should fail login without email & password', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({})
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('Please provide your email address.');
                res.body.errors.should.have.property('password').equal('Please provide your password.');
                done();
            });
    });

    it('should fail login without password', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'test@test.com'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('password').equal('Please provide your password.');
                done();
            });
    });

    it('should fail login without email', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('Please provide your email address.');
                done();
            });
    });

    it('should fail email validation', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'invalid@test',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like "Abc.example.com"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'Abc.example.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like "A@b@c@example.com"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'A@b@c@example.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like "just"not"right@example.com"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'just"not"right@example.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like "a"b(c)d,e:f;g<h>i[j\k]l@example.com"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'a"b(c)d,e:f;g<h>i[j\k]l@example.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like "this is"not\allowed@example.com"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'this is"not\allowed@example.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like "this\ still\"not\\allowed@example.com"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'this\ still\"not\\allowed@example.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like "john..doe@example.com"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'john..doe@example.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like ".john.doe@example.com"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: '.john.doe@example.com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like "example@localhost"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'example@localhost',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail email validation with email like "john.doe@example..com"', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'john.doe@example..com',
                password: 'testtest'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('email').equal('E-mail address is invalid');
                done();
            });
    });

    it('should fail validate password with length < 8', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'test@test.com',
                password: 'testtes'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('password').equal('Password must be between 8 and 64 chars long');
                done();
            });
    });

    it('should fail validate password with length > 64', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'test@test.com',
                password: 'abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij12345'
            })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('success').equal(false);
                res.body.should.have.property('message').equal('Check the form for errors.');
                res.body.should.have.property('errors');
                res.body.errors.should.be.a('object');
                res.body.errors.should.have.property('password').equal('Password must be between 8 and 64 chars long');
                done();
            });
    });

    it('should fail without token', (done) => {
        chai.request(app)
            .get('/api/city')
            .end((err, res) => {
                res.should.have.status(401);
                res.body.should.be.a('object');
                res.body.should.have.property('error').equal('Access is denied');
                done();
            });
    });

    it('should pass with token', (done) => {
        chai.request(app)
            .post('/api/login')
            .send({
                email: 'test@test.com',
                password: 'testtest'
            }).end((err, res) => {
            chai.request(app)
                .get('/api/city')
                .set('Authorization', 'Bearer ' + res.body.token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    it('should fail with broken token', (done) => {
        chai.request(app)
            .get('/api/city')
            .set('Authorization', 'Bearer eyXXXBROKENXXXTOKENXXXR5cCI6IkpXVCJ9.eyJzdWIiOiI1OTJkODAwZTA4NWQyNDE0YmMzNGY5ODYiLCJpYXQiOjE0OTYxNTQyMjZ9.3HEVZXhY8DDm8Q4Hb349n2K-slJS7_Qn0LUQ-_Kzw6o')
            .end((err, res) => {
                res.should.have.status(401);
                res.body.should.be.a('object');
                res.body.should.have.property('error').equal('Token mismatch');
                done();
            });
    });

    it('should fail with valid token but user not exist', (done) => {
        chai.request(app)
            .get('/api/city')
            .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1OTI4M2U0NGNiZGNlMDFhNjA5ZGQ4YzciLCJpYXQiOjE0OTYxNTY2MzN9.evt2rq8GfTZVy6AeL-e-xu871t90XImz-mek5riyFYc')
            .end((err, res) => {
                res.should.have.status(401);
                res.body.should.be.a('object');
                res.body.should.have.property('error').equal('Access is denied');
                done();
            });
    });
});
