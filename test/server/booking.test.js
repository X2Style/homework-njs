process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
let app = require('../../server/app').default;
let should = chai.should();
let User = require('../../server/models/user').default;
let Booking = require('../../server/models/booking').default;
let Customer = require('../../server/models/customer').default;
let Cleaner = require('../../server/models/cleaner').default;
let City = require('../../server/models/city').default;
let config = require('../../config/config').default;
let moment = require('moment');
let minDate = require('../../server/helper').minDate;

chai.use(chaiHttp);

describe('Bookings', () => {

    const testCities = [];
    const testCleaners = [];
    const testCustomers = [];
    let token = '';

    before((done) => {

        const user = new User({
            name: 'Test',
            email: 'test@test.com',
            password: 'testtest'
        });

        user.save((err, user) => {
            chai.request(app)
                .post('/api/login')
                .send({
                    email: 'test@test.com',
                    password: 'testtest'
                })
                .end((err, res) => {
                    token = res.body.token;

                    const city1 = new City({
                        name: 'Kyiv',
                        priceFactor: 2.8
                    });

                    const city2 = new City({
                        name: 'Dnipro',
                        priceFactor: 1.6
                    });

                    city1.save((err, city) => {
                        testCities.push(city);
                        city2.save((err, city) => {
                            testCities.push(city);

                            const cleaner1 = new Cleaner({
                                email: 'valid1@mail.com',
                                firstName: 'First',
                                lastName: 'Cleaner',
                                cities: [testCities[0]._id],
                                qualityScore: 4.2
                            });

                            const cleaner2 = new Cleaner({
                                email: 'valid2@mail.com',
                                firstName: 'Second',
                                lastName: 'Cleaner',
                                cities: [testCities[0]._id, testCities[1]._id],
                                qualityScore: 3.2
                            });

                            cleaner1.save((err, cleaner) => {
                                testCleaners.push(cleaner);
                                cleaner2.save((err, cleaner) => {
                                    testCleaners.push(cleaner);

                                    const customer1 = new Customer({
                                        firstName: 'First',
                                        lastName: 'Customer',
                                        email: 'first@customer.com',
                                        phone: '(067) 111-11-11'
                                    });
                                    customer1.save((err, customer) => {
                                        testCustomers.push(customer);
                                        done();
                                    });
                                });
                            });
                        });
                    });
                });
        });
    });

    after((done) => {
        User.remove({}, (err) => {
            City.remove({}, (err) => {
                Cleaner.remove({}, (err) => {
                    Customer.remove({}, (err) => {
                        done();
                    });
                });
            });
        });
    });

    beforeEach((done) => {
        Booking.remove({}, (err) => {
            done();
        });
    });

    describe('Get all bookings', () => {
        it('it should GET all the bookings', (done) => {
            chai.request(app)
                .get('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.to.equal(0);
                    done();
                });
        });
    });

    describe('Create booking', () => {

        it('it should not add a booking without customer firstname', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                lastName: 'Customer',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is required');
                    done();
                });
        });

        it('it should not add a booking with customer firstname length < 2', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'F',
                lastName: 'Customer',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a booking with customer firstname length > 100', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'Abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijz',
                lastName: 'Customer',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a booking with customer firstname contains digits', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'F1rst',
                lastName: 'Customer',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer firstname contains forbidden symbols', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'Fir$t',
                lastName: 'Customer',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer firstname contains \'\'', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'Fir\'\'st',
                lastName: 'Customer',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer firstname begins with \'', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: '\'First',
                lastName: 'Customer',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer firstname begins with -', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: '-First',
                lastName: 'Lastname',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer firstname ends with -', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First-',
                lastName: 'Lastname',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('firstName');
                    res.body.error[0].should.have.property('message').equal('First name is invalid');
                    done();
                });
        });

        it('it should add a booking with customer cyrillic firstname', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'Иван',
                lastName: 'Last',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('customer');
                    res.body.customer.should.have.property('firstName').equal('Иван');
                    done();
                });
        });

        it('it should not add a booking without customer lastname', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is required');
                    done();
                });
        });

        it('it should not add a booking with customer lastname length < 2', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'C',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a booking with customer lastname length > 100', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijz',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name must be between 2 and 100 chars long');
                    done();
                });
        });

        it('it should not add a booking with customer lastname contains digits', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Las1name',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer lastname contains forbidden symbols', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'L@$tname',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer lastname contains \'\'', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Last\'\'name',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer lastname begins with \'', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: '\'Lastname',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer lastname begins with -', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: '-Lastname',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should not add a booking with customer lastname ends with -', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Lastname-',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('lastName');
                    res.body.error[0].should.have.property('message').equal('Last name is invalid');
                    done();
                });
        });

        it('it should add a booking with customer cyrillic lastname', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Иванов',
                email: 'customer@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('customer');
                    res.body.customer.should.have.property('lastName').equal('Иванов');
                    done();
                });
        });

        it('it should not add a booking without customer email', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('E-mail is required');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like "Abc.example.com"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'Abc.example.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like "A@b@c@example.com"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'A@b@c@example.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like "just"not"right@example.com"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'just"not"right@example.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like "a"b(c)d,e:f;g<h>i[j\k]l@example.com"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'a"b(c)d,e:f;g<h>i[j\k]l@example.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like "this is"not\allowed@example.com"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'this is"not\allowed@example.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like "this\ still\"not\\allowed@example.com"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'this\ still\"not\\allowed@example.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like "john..doe@example.com"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'john..doe@example.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like ".john.doe@example.com"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: '.john.doe@example.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like "example@localhost"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'example@localhost'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer email like "john.doe@example..com"', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'john.doe@example..com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('email');
                    res.body.error[0].should.have.property('message').equal('Enter a valid e-mail address');
                    done();
                });
        });

        it('it should not add a booking with invalid customer phone', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'john.doe@example.com',
                phone: '123-45-67'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('phone');
                    res.body.error[0].should.have.property('message').equal('This is not a valid phone number');
                    done();
                });
        });

        it('it should not add a booking with customer phone contain symbols', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'john.doe@example.com',
                phone: '(067) abc-de-fg'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('phone');
                    res.body.error[0].should.have.property('message').equal('This is not a valid phone number');
                    done();
                });
        });

        it('it should not add a booking without duration', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'email@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('duration');
                    res.body.error[0].should.have.property('message').equal('Duration is required');
                    done();
                });
        });

        it('it should not add a booking with duration < 1', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 0.5,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'email@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('duration');
                    res.body.error[0].should.have.property('message').equal('Duration must be from 1 to 8 Hours');
                    done();
                });
        });

        it('it should not add a booking with duration > 8', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 9,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'email@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('duration');
                    res.body.error[0].should.have.property('message').equal('Duration must be from 1 to 8 Hours');
                    done();
                });
        });

        it('it should not add a booking with duration contain symbols', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 'a',
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'email@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('duration');
                    res.body.error[0].should.have.property('message').equal('Duration is required');
                    done();
                });
        });

        it('it should add a booking and floor float duration', (done) => {
            let booking = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1.5,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'email@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('dateTo');
                    moment(res.body.dateTo).minutes().should.to.equal(0);
                    moment(res.body.dateTo).seconds().should.to.equal(0);
                    moment(res.body.dateTo).milliseconds().should.to.equal(0);
                    done();
                });
        });

        it('it should not add a booking without date from', (done) => {
            let booking = {
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'valid@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('dateFrom');
                    res.body.error[0].should.have.property('message').equal('Date from is required');
                    done();
                });
        });

        it('it should not add a booking with invalid date from', (done) => {
            let booking = {
                dateFrom: '12-12-12',
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'valid@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('dateFrom');
                    res.body.error[0].should.have.property('message').equal('Invalid date');
                    done();
                });
        });

        it('it should not add a booking with date time < now + minimum time to reach destination', (done) => {
            let booking = {
                dateFrom: moment(minDate()).subtract(1, 's'),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'valid@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('dateFrom');
                    res.body.error[0].should.have.property('message').equal('Invalid date');
                    done();
                });
        });

        it('it should not add a booking without city', (done) => {
            let booking = {
                dateFrom: moment(minDate()).add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                firstName: 'First',
                lastName: 'Customer',
                email: 'valid@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('city');
                    res.body.error[0].should.have.property('message').equal('City is required');
                    done();
                });
        });

        it('it should not add a booking with invalid city id', (done) => {
            let booking = {
                dateFrom: moment(minDate()).add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: 'invalidid',
                firstName: 'First',
                lastName: 'Customer',
                email: 'valid@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error');
                    res.body.error.should.be.a('array');
                    res.body.error[0].should.be.a('object');
                    res.body.error[0].should.have.property('name').equal('city');
                    res.body.error[0].should.have.property('message').equal('Invalid city');
                    done();
                });
        });

        it('it should not add a booking with valid city id, but city does not exist', (done) => {
            let booking = {
                dateFrom: moment(minDate()).add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: '5919b54d4e6d6c0c5c8c0000',
                firstName: 'First',
                lastName: 'Customer',
                email: 'valid@mail.com'
            };
            chai.request(app)
                .post('/api/booking')
                .set('Authorization', 'Bearer ' + token)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Free cleaner not found. Try to choose another time.');
                    done();
                });
        });

        it('it should not add a booking - all cleaners busy at this time', (done) => {
            let booking3 = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[0]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'first@customer.com'
            };

            const booking1 = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            const booking2 = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                booking2.save((err, booking) => {
                    chai.request(app)
                        .post('/api/booking')
                        .set('Authorization', 'Bearer ' + token)
                        .send(booking3)
                        .end((err, res) => {
                            res.should.have.status(404);
                            res.body.should.be.a('object');
                            res.body.should.have.property('error').equal('Free cleaner not found. Try to choose another time.');
                            done();
                        });
                });
            });

        });

        it('it should not add a booking - all cleaners busy at this time (starts before)', (done) => {
            let bookingTest = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 2,
                city: testCities[1]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'first@customer.com'
            };

            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[1].priceFactor,
                city: testCities[1]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                chai.request(app)
                    .post('/api/booking')
                    .set('Authorization', 'Bearer ' + token)
                    .send(bookingTest)
                    .end((err, res) => {
                        res.should.have.status(404);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('Free cleaner not found. Try to choose another time.');
                        done();
                    });
            });

        });

        it('it should not add a booking - all cleaners busy at this time (ends after)', (done) => {
            let bookingTest = {
                dateFrom: moment().add(5, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 2,
                city: testCities[1]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'first@customer.com'
            };

            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[1].priceFactor,
                city: testCities[1]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                chai.request(app)
                    .post('/api/booking')
                    .set('Authorization', 'Bearer ' + token)
                    .send(bookingTest)
                    .end((err, res) => {
                        res.should.have.status(404);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('Free cleaner not found. Try to choose another time.');
                        done();
                    });
            });

        });

        it('it should not add a booking - all cleaners busy at this time (starts after, ends before)', (done) => {
            let bookingTest = {
                dateFrom: moment().add(4, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 1,
                city: testCities[1]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'first@customer.com'
            };

            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[1].priceFactor,
                city: testCities[1]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                chai.request(app)
                    .post('/api/booking')
                    .set('Authorization', 'Bearer ' + token)
                    .send(bookingTest)
                    .end((err, res) => {
                        res.should.have.status(404);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('Free cleaner not found. Try to choose another time.');
                        done();
                    });
            });

        });

        it('it should not add a booking - all cleaners busy at this time (starts before, ends after)', (done) => {
            let bookingTest = {
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                duration: 8,
                city: testCities[1]._id,
                firstName: 'First',
                lastName: 'Customer',
                email: 'first@customer.com'
            };

            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[1].priceFactor,
                city: testCities[1]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                chai.request(app)
                    .post('/api/booking')
                    .set('Authorization', 'Bearer ' + token)
                    .send(bookingTest)
                    .end((err, res) => {
                        res.should.have.status(404);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('Free cleaner not found. Try to choose another time.');
                        done();
                    });
            });

        });

        it('it should add a booking and update customer information by email', (done) => {
            const customer = new Customer({
                email: 'valid@mail.com',
                firstName: 'Firstname',
                lastName: 'Lastname',
                phone: '(067) 111-11-11'
            });
            customer.save((err, customer) => {
                let booking = {
                    dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                    duration: 1,
                    city: testCities[0]._id,
                    firstName: 'First',
                    lastName: 'Customer',
                    email: 'valid@mail.com',
                    phone: '(067) 222-22-22'
                };
                chai.request(app)
                    .post('/api/booking')
                    .set('Authorization', 'Bearer ' + token)
                    .send(booking)
                    .end((err, res) => {
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        res.body.should.have.property('dateFrom').equal(moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0).toISOString());
                        res.body.should.have.property('dateTo').equal(moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0).toISOString());
                        res.body.should.have.property('cleaner');
                        res.body.customer.should.be.a('object');
                        res.body.customer.should.have.property('email').equal('valid@mail.com');
                        res.body.customer.should.have.property('firstName').equal('First');
                        res.body.customer.should.have.property('lastName').equal('Customer');
                        res.body.customer.should.have.property('phone').equal('(067) 222-22-22');
                        done();
                    });
            });
        });

    });

    describe('Get booking by ID', () => {

        it('it should get booking by valid ID', (done) => {
            const bookingTest = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0).toDate(),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0).toDate(),
                price: config.pricePerHour * testCities[1].priceFactor,
                city: testCities[1]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            bookingTest.save((err, booking) => {
                chai.request(app)
                    .get('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('_id').equal(bookingTest._id.toString());
                        res.body.should.have.property('dateFrom');
                        res.body.should.have.property('dateTo');
                        res.body.should.have.property('price');
                        res.body.should.have.property('city');
                        res.body.should.have.property('customer');
                        res.body.should.have.property('cleaner');
                        done();
                    });
            });

        });

        it('it should fail GET booking by invalid ID', (done) => {
            chai.request(app)
                .get('/api/booking/invalid')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });
    });

    describe('Edit booking', () => {

        it('it should not update booking by invalid ID', (done) => {
            chai.request(app)
                .put('/api/booking/invalid')
                .set('Authorization', 'Bearer ' + token)
                .send({
                    dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                    duration: 1,
                    city: testCities[0]._id,
                    customer: testCustomers[0]._id,
                    cleaner: testCleaners[0]._id
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });

        it('it should not update booking without date from', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        duration: 1,
                        city: testCities[0]._id,
                        customer: testCustomers[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('dateFrom');
                        res.body.error[0].should.have.property('message').equal('Date from is required');
                        done();
                    });
            });
        });

        it('it should not update booking with invalid date from', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: '12-12-12',
                        duration: 1,
                        city: testCities[0]._id,
                        customer: testCustomers[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('dateFrom');
                        res.body.error[0].should.have.property('message').equal('Invalid date');
                        done();
                    });
            });
        });

        it('it should not update booking without duration', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        city: testCities[0]._id,
                        customer: testCustomers[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('duration');
                        res.body.error[0].should.have.property('message').equal('Duration is required');
                        done();
                    });
            });
        });

        it('it should not update booking with duration < 1', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 0.5,
                        city: testCities[0]._id,
                        customer: testCustomers[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('duration');
                        res.body.error[0].should.have.property('message').equal('Duration must be from 1 to 8 Hours');
                        done();
                    });
            });
        });

        it('it should not update booking with duration > 8', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 9,
                        city: testCities[0]._id,
                        customer: testCustomers[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('duration');
                        res.body.error[0].should.have.property('message').equal('Duration must be from 1 to 8 Hours');
                        done();
                    });
            });
        });

        it('it should not update booking with duration contain symbols', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 'a',
                        city: testCities[0]._id,
                        customer: testCustomers[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('duration');
                        res.body.error[0].should.have.property('message').equal('Duration is required');
                        done();
                    });
            });
        });

        it('it should not update booking without city', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 1,
                        customer: testCustomers[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('city');
                        res.body.error[0].should.have.property('message').equal('City is required');
                        done();
                    });
            });
        });

        it('it should not update booking with city invalid id', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 1,
                        city: 'invalidid',
                        customer: testCustomers[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('city');
                        res.body.error[0].should.have.property('message').equal('Invalid city');
                        done();
                    });
            });
        });

        it('it should not update booking with city does not exist', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 1,
                        city: '5919b54d4e6d6c0c5c8c0000',
                        customer: testCustomers[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('This cleaner does not work in this city');
                        done();
                    });
            });
        });

        it('it should not update booking without customer', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 1,
                        city: testCities[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('customer');
                        res.body.error[0].should.have.property('message').equal('Customer is required');
                        done();
                    });
            });
        });

        it('it should not update booking with invalid customer ID', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 1,
                        customer: 'invalidid',
                        city: testCities[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('customer');
                        res.body.error[0].should.have.property('message').equal('Invalid customer');
                        done();
                    });
            });
        });

        it('it should not update booking with customer does not exist', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 1,
                        customer: '5919b54d4e6d6c0c5c8c0000',
                        city: testCities[0]._id,
                        cleaner: testCleaners[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('Invalid customer');
                        done();
                    });
            });
        });

        it('it should not update booking without cleaner', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 1,
                        city: testCities[0]._id,
                        customer: testCustomers[0]._id
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('cleaner');
                        res.body.error[0].should.have.property('message').equal('Cleaner is required');
                        done();
                    });
            });
        });

        it('it should not update booking with invalid cleaner ID', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 1,
                        city: testCities[0]._id,
                        customer: testCustomers[0]._id,
                        cleaner: 'invalidid'
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error');
                        res.body.error.should.be.a('array');
                        res.body.error.length.should.to.equal(1);
                        res.body.error[0].should.have.property('name').equal('cleaner');
                        res.body.error[0].should.have.property('message').equal('Invalid cleaner');
                        done();
                    });
            });
        });

        it('it should not update booking with cleaner does not exist', (done) => {
            let bookingTest = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });
            bookingTest.save((err, booking) => {
                chai.request(app)
                    .put('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .send({
                        dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                        duration: 1,
                        city: testCities[0]._id,
                        customer: testCustomers[0]._id,
                        cleaner: '5919b54d4e6d6c0c5c8c0000'
                    })
                    .end((err, res) => {
                        res.should.have.status(400);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').equal('Invalid cleaner');
                        done();
                    });
            });
        });

        it('it should not update a booking (change time exact) - all cleaners busy at this time', (done) => {
            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(4, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            const booking2 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(4, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            const booking3 = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                booking2.save((err, booking) => {
                    booking3.save((err, booking) => {
                        chai.request(app)
                            .put('/api/booking/' + booking._id)
                            .set('Authorization', 'Bearer ' + token)
                            .send({
                                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                                duration: 1,
                                city: testCities[0]._id,
                                customer: testCustomers[0]._id,
                                cleaner: testCleaners[1]._id
                            })
                            .end((err, res) => {
                                res.should.have.status(400);
                                res.body.should.be.a('object');
                                res.body.should.have.property('error').equal('Error saving booking. Try to choose another time.');
                                done();
                            });
                    });
                });
            });

        });

        it('it should not update a booking (change time, starts before) - all cleaners busy at this time', (done) => {
            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            const booking2 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            const booking3 = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                booking2.save((err, booking) => {
                    booking3.save((err, booking) => {
                        chai.request(app)
                            .put('/api/booking/' + booking._id)
                            .set('Authorization', 'Bearer ' + token)
                            .send({
                                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                                duration: 2,
                                city: testCities[0]._id,
                                customer: testCustomers[0]._id,
                                cleaner: testCleaners[1]._id
                            })
                            .end((err, res) => {
                                res.should.have.status(400);
                                res.body.should.be.a('object');
                                res.body.should.have.property('error').equal('Error saving booking. Try to choose another time.');
                                done();
                            });
                    });
                });
            });

        });

        it('it should not update a booking (change time inside range) - all cleaners busy at this time', (done) => {
            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            const booking2 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            const booking3 = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                booking2.save((err, booking) => {
                    booking3.save((err, booking) => {
                        chai.request(app)
                            .put('/api/booking/' + booking._id)
                            .set('Authorization', 'Bearer ' + token)
                            .send({
                                dateFrom: moment().add(4, 'h').minutes(0).seconds(0).milliseconds(0),
                                duration: 1,
                                city: testCities[0]._id,
                                customer: testCustomers[0]._id,
                                cleaner: testCleaners[1]._id
                            })
                            .end((err, res) => {
                                res.should.have.status(400);
                                res.body.should.be.a('object');
                                res.body.should.have.property('error').equal('Error saving booking. Try to choose another time.');
                                done();
                            });
                    });
                });
            });

        });

        it('it should not update a booking (change time ends after) - all cleaners busy at this time', (done) => {
            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            const booking2 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            const booking3 = new Booking({
                dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                booking2.save((err, booking) => {
                    booking3.save((err, booking) => {
                        chai.request(app)
                            .put('/api/booking/' + booking._id)
                            .set('Authorization', 'Bearer ' + token)
                            .send({
                                dateFrom: moment().add(5, 'h').minutes(0).seconds(0).milliseconds(0),
                                duration: 3,
                                city: testCities[0]._id,
                                customer: testCustomers[0]._id,
                                cleaner: testCleaners[1]._id
                            })
                            .end((err, res) => {
                                res.should.have.status(400);
                                res.body.should.be.a('object');
                                res.body.should.have.property('error').equal('Error saving booking. Try to choose another time.');
                                done();
                            });
                    });
                });
            });

        });

        it('it should not update a booking (change cleaner) - all cleaners busy at this time', (done) => {
            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            const booking2 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking1.save((err, booking) => {
                booking2.save((err, booking) => {
                    chai.request(app)
                        .put('/api/booking/' + booking._id)
                        .set('Authorization', 'Bearer ' + token)
                        .send({
                            dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                            duration: 3,
                            city: testCities[0]._id,
                            customer: testCustomers[0]._id,
                            cleaner: testCleaners[0]._id
                        })
                        .end((err, res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            res.body.should.have.property('error').equal('Error saving booking. Try to choose another time.');
                            done();
                        });
                });
            });

        });

        it('it should not update a booking (change city) - all cleaners busy at this time', (done) => {
            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            const booking2 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking2.save((err, booking) => {
                booking1.save((err, booking) => {
                    chai.request(app)
                        .put('/api/booking/' + booking._id)
                        .set('Authorization', 'Bearer ' + token)
                        .send({
                            dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                            duration: 3,
                            city: testCities[1]._id,
                            customer: testCustomers[0]._id,
                            cleaner: testCleaners[0]._id
                        })
                        .end((err, res) => {
                            res.should.have.status(400);
                            res.body.should.be.a('object');
                            res.body.should.have.property('error').equal('This cleaner does not work in this city');
                            done();
                        });
                });
            });

        });

        it('it should update a booking with valid data', (done) => {
            const booking1 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[0]._id
            });

            const booking2 = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0),
                price: config.pricePerHour * testCities[0].priceFactor,
                city: testCities[0]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            booking2.save((err, booking) => {
                booking1.save((err, booking) => {
                    chai.request(app)
                        .put('/api/booking/' + booking._id)
                        .set('Authorization', 'Bearer ' + token)
                        .send({
                            dateFrom: moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0),
                            duration: 1,
                            city: testCities[0]._id,
                            customer: testCustomers[0]._id,
                            cleaner: testCleaners[0]._id
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('dateFrom').equal(moment().add(2, 'h').minutes(0).seconds(0).milliseconds(0).toISOString());
                            res.body.should.have.property('dateTo').equal(moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0).toISOString());
                            res.body.should.have.property('city').equal(testCities[0]._id.toString());
                            res.body.should.have.property('customer').equal(testCustomers[0]._id.toString());
                            res.body.should.have.property('cleaner').equal(testCleaners[0]._id.toString());
                            done();
                        });
                });
            });

        });

    });

    describe('Delete booking', () => {

        it('it should delete booking by valid ID', (done) => {
            const bookingTest = new Booking({
                dateFrom: moment().add(3, 'h').minutes(0).seconds(0).milliseconds(0).toDate(),
                dateTo: moment().add(6, 'h').minutes(0).seconds(0).milliseconds(0).toDate(),
                price: config.pricePerHour * testCities[1].priceFactor,
                city: testCities[1]._id,
                customer: testCustomers[0]._id,
                cleaner: testCleaners[1]._id
            });

            bookingTest.save((err, booking) => {
                chai.request(app)
                    .delete('/api/booking/' + booking._id)
                    .set('Authorization', 'Bearer ' + token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('_id').equal(bookingTest._id.toString());
                        res.body.should.have.property('dateFrom');
                        res.body.should.have.property('dateTo');
                        res.body.should.have.property('price');
                        res.body.should.have.property('city');
                        res.body.should.have.property('customer');
                        res.body.should.have.property('cleaner');
                        done();
                    });
            });
        });

        it('it should fail delete booking by invalid ID', (done) => {
            chai.request(app)
                .delete('/api/booking/invalid')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').equal('Not Found');
                    done();
                });
        });
    });
})
;