process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
let app = require('../../server/app').default;
let should = chai.should();


describe('Load client part', function () {

    it('should pass index route to react', (done) => {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                res.type.should.equal('text/html');
                done();
            });
    });

    it('should pass not found route to react', (done) => {
        chai.request(app)
            .get('/404')
            .end((err, res) => {
                res.should.have.status(200);
                res.type.should.equal('text/html');
                done();
            });
    });
});
