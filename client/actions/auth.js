import axios from 'axios';
import Auth from '../modules/Auth';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGIN_SET_TOKEN = 'LOGIN_SET_TOKEN';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';

const ROOT_URL = '/api';

export function loginRequest(values, history) {
    const request = axios.post(ROOT_URL + '/login', values);

    return (dispatch) => {
        dispatch({type: LOGIN_REQUEST, payload: {isLoading: true, isError: null}})

        request.then(
            (data) => {
                dispatch({type: LOGIN_SUCCESS, payload: data});
                Auth.authenticateUser(data.data.token, data.data.user.name);
                history.push('/admin');
            },
            (err) => {
                dispatch({type: LOGIN_FAILURE, payload: err});
            });
    };
}

export function logoutRequest() {
    return (dispatch) => {
        dispatch({type: LOGOUT_REQUEST, payload: {}});
        Auth.deauthenticateUser();
    };
}

export function initForm(data) {
    return (dispatch) => {
        dispatch({
            type: '@@redux-form/INITIALIZE',
            meta: {form: 'FormLogin'},
            payload: data,
        })
    }
}