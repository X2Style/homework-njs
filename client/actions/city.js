import axios from 'axios';
import Auth from '../modules/Auth';

export const FETCH_CITIES = 'FETCH_CITIES';
export const FETCH_CITIES_SUCCESS = 'FETCH_CITIES_SUCCESS';
export const FETCH_CITIES_ERROR = 'FETCH_CITIES_ERROR';
export const FETCH_CITY_CLEANERS = 'FETCH_CITY_CLEANERS';
export const FETCH_CITY_CLEANERS_SUCCESS = 'FETCH_CITY_CLEANERS_SUCCESS';
export const FETCH_CITY_CLEANERS_ERROR = 'FETCH_CITY_CLEANERS_ERROR';
export const FETCH_CITY = 'FETCH_CITY';
export const FETCH_CITY_SUCCESS = 'FETCH_CITY_SUCCESS';
export const FETCH_CITY_ERROR = 'FETCH_CITY_ERROR';
export const CREATE_CITY = 'CREATE_CITY';
export const CREATE_CITY_SUCCESS = 'CREATE_CITY_SUCCESS';
export const CREATE_CITY_ERROR = 'CREATE_CITY_ERROR';
export const UPDATE_CITY = 'UPDATE_CITY';
export const UPDATE_CITY_SUCCESS = 'UPDATE_CITY_SUCCESS';
export const UPDATE_CITY_ERROR = 'UPDATE_CITY_ERROR';
export const DELETE_CITY = 'DELETE_CITY';
export const DELETE_CITY_SUCCESS = 'DELETE_CITY_SUCCESS';
export const DELETE_CITY_ERROR = 'DELETE_CITY_ERROR';
export const RESET_CITY = 'RESET_CITY';
export const RESET_CITIES = 'RESET_CITIES';

const ROOT_URL = '/api/city';

export function fetchCities() {
    const request = axios({
        url: ROOT_URL,
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_CITIES, payload: {isLoading: true, isError: null}})

        request.then(
            (data) => {
                dispatch({type: FETCH_CITIES_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CITIES_ERROR, payload: err});
            });
    };
}

export function fetchCityCleaners(city) {
    const request = axios({
        url: ROOT_URL + '/' + city + '/cleaners',
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_CITY_CLEANERS, payload: {isLoading: true, isError: null}})

        request.then(
            (data) => {
                dispatch({type: FETCH_CITY_CLEANERS_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CITY_CLEANERS_ERROR, payload: err});
            });
    };
}

export function fetchCity(id) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_CITY, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: FETCH_CITY_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CITY_ERROR, payload: err});
            });
    };
}

export function createCity(values, history) {
    const request = axios({
        url: ROOT_URL,
        method: 'post',
        data: values,
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: CREATE_CITY, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: CREATE_CITY_SUCCESS, payload: data});
                history.push('/admin/city');
            },
            (err) => {
                dispatch({type: CREATE_CITY_ERROR, payload: err});
            });
    };
}

export function updateCity(id, values, history) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'put',
        data: values,
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: UPDATE_CITY, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: UPDATE_CITY_SUCCESS, payload: data});
                history.push('/admin/city');
            },
            (err) => {
                dispatch({type: UPDATE_CITY_ERROR, payload: err});
            });
    };
}

export function resetCity() {
    return (dispatch) => {
        dispatch({type: RESET_CITY, payload: null});
    };
}

export function resetCities() {
    return (dispatch) => {
        dispatch({type: RESET_CITIES, payload: null});
    };
}

export function deleteCity(id, history = null) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'delete',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: DELETE_CITY, payload: null});

        request.then(
            (data) => {
                dispatch({type: DELETE_CITY_SUCCESS, payload: data});
                if (history) {
                    history.push('/admin/city');
                }
            },
            (err) => {
                dispatch({type: DELETE_CITY_ERROR, payload: err});
            });
    };
}

export function initForm(data) {
    return (dispatch) => {
        dispatch({
            type: '@@redux-form/INITIALIZE',
            meta: {form: 'CityEdit'},
            payload: data,
        })
    }
}


