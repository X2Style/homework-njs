import axios from 'axios';
import Auth from '../modules/Auth';

export const FETCH_CLEANERS = 'FETCH_CLEANERS';
export const FETCH_CLEANERS_SUCCESS = 'FETCH_CLEANERS_SUCCESS';
export const FETCH_CLEANERS_ERROR = 'FETCH_CLEANERS_ERROR';
export const FETCH_CLEANER_BOOKINGS = 'FETCH_CLEANER_BOOKINGS';
export const FETCH_CLEANER_BOOKINGS_SUCCESS = 'FETCH_CLEANER_BOOKINGS_SUCCESS';
export const FETCH_CLEANER_BOOKINGS_ERROR = 'FETCH_CLEANER_BOOKINGS_ERROR';
export const FETCH_CLEANER = 'FETCH_CLEANER';
export const FETCH_CLEANER_SUCCESS = 'FETCH_CLEANER_SUCCESS';
export const FETCH_CLEANER_ERROR = 'FETCH_CLEANER_ERROR';
export const CREATE_CLEANER = 'CREATE_CLEANER';
export const CREATE_CLEANER_SUCCESS = 'CREATE_CLEANER_SUCCESS';
export const CREATE_CLEANER_ERROR = 'CREATE_CLEANER_ERROR';
export const UPDATE_CLEANER = 'UPDATE_CLEANER';
export const UPDATE_CLEANER_SUCCESS = 'UPDATE_CLEANER_SUCCESS';
export const UPDATE_CLEANER_ERROR = 'UPDATE_CLEANER_ERROR';
export const DELETE_CLEANER = 'DELETE_CLEANER';
export const DELETE_CLEANER_SUCCESS = 'DELETE_CLEANER_SUCCESS';
export const DELETE_CLEANER_ERROR = 'DELETE_CLEANER_ERROR';
export const RESET_CLEANER = 'RESET_CLEANER';
export const RESET_CLEANERS = 'RESET_CLEANERS';

const ROOT_URL = '/api/cleaner';

export function fetchCleaners() {
    const request = axios({
        url: ROOT_URL,
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_CLEANERS, payload: {isLoading: true, isError: null}})

        request.then(
            (data) => {
                dispatch({type: FETCH_CLEANERS_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CLEANERS_ERROR, payload: err});
            });
    };
}

export function fetchCleanerBookings(cleaner) {
    const request = axios({
        url: ROOT_URL + '/' + cleaner + '/bookings',
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_CLEANER_BOOKINGS, payload: {isLoading: true, isError: null}})

        request.then(
            (data) => {
                dispatch({type: FETCH_CLEANER_BOOKINGS_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CLEANER_BOOKINGS_ERROR, payload: err});
            });
    };
}

export function fetchCleaner(id) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_CLEANER, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: FETCH_CLEANER_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CLEANER_ERROR, payload: err});
            });
    };
}

export function createCleaner(values, history) {
    const request = axios({
        url: ROOT_URL,
        method: 'post',
        data: values,
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: CREATE_CLEANER, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: CREATE_CLEANER_SUCCESS, payload: data});
                history.push('/admin/cleaner');
            },
            (err) => {
                dispatch({type: CREATE_CLEANER_ERROR, payload: err});
            });
    };
}

export function updateCleaner(id, values, history) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'put',
        data: values,
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: UPDATE_CLEANER, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: UPDATE_CLEANER_SUCCESS, payload: data});
                history.push('/admin/cleaner');
            },
            (err) => {
                dispatch({type: UPDATE_CLEANER_ERROR, payload: err});
            });
    };
}

export function resetCleaner() {
    return (dispatch) => {
        dispatch({type: RESET_CLEANER, payload: null});
    };
}

export function resetCleaners() {
    return (dispatch) => {
        dispatch({type: RESET_CLEANERS, payload: null});
    };
}

export function deleteCleaner(id, history = null) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'delete',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: DELETE_CLEANER, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: DELETE_CLEANER_SUCCESS, payload: data});
                if (history) {
                    history.push('/admin/cleaner');
                }
            },
            (err) => {
                dispatch({type: DELETE_CLEANER_ERROR, payload: err});
            });
    };
}

export function  initForm(data) {
    return (dispatch) => {
        dispatch({
            type: '@@redux-form/INITIALIZE',
            meta: {form: 'CleanerEdit'},
            payload: data,
          })
    }
}


