import axios from 'axios';
import Auth from '../modules/Auth';

export const FETCH_CUSTOMERS = 'FETCH_CUSTOMERS';
export const FETCH_CUSTOMERS_SUCCESS = 'FETCH_CUSTOMERS_SUCCESS';
export const FETCH_CUSTOMERS_ERROR = 'FETCH_CUSTOMERS_ERROR';
export const FETCH_CUSTOMER_BOOKINGS = 'FETCH_CUSTOMER_BOOKINGS';
export const FETCH_CUSTOMER_BOOKINGS_SUCCESS = 'FETCH_CUSTOMER_BOOKINGS_SUCCESS';
export const FETCH_CUSTOMER_BOOKINGS_ERROR = 'FETCH_CUSTOMER_BOOKINGS_ERROR';
export const FETCH_CUSTOMER = 'FETCH_CUSTOMER';
export const FETCH_CUSTOMER_SUCCESS = 'FETCH_CUSTOMER_SUCCESS';
export const FETCH_CUSTOMER_ERROR = 'FETCH_CUSTOMER_ERROR';
export const CREATE_CUSTOMER = 'CREATE_CUSTOMER';
export const CREATE_CUSTOMER_SUCCESS = 'CREATE_CUSTOMER_SUCCESS';
export const CREATE_CUSTOMER_ERROR = 'CREATE_CUSTOMER_ERROR';
export const UPDATE_CUSTOMER = 'UPDATE_CUSTOMER';
export const UPDATE_CUSTOMER_SUCCESS = 'UPDATE_CUSTOMER_SUCCESS';
export const UPDATE_CUSTOMER_ERROR = 'UPDATE_CUSTOMER_ERROR';
export const DELETE_CUSTOMER = 'DELETE_CUSTOMER';
export const DELETE_CUSTOMER_SUCCESS = 'DELETE_CUSTOMER_SUCCESS';
export const DELETE_CUSTOMER_ERROR = 'DELETE_CUSTOMER_ERROR';
export const RESET_CUSTOMER = 'RESET_CUSTOMER';
export const RESET_CUSTOMERS = 'RESET_CUSTOMERS';

const ROOT_URL = '/api/customer';

export function fetchCustomers() {
    const request = axios({
        url: ROOT_URL,
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_CUSTOMERS, payload: {isLoading: true, isError: null}})

        request.then(
            (data) => {
                dispatch({type: FETCH_CUSTOMERS_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CUSTOMERS_ERROR, payload: err});
            });
    };
}

export function fetchCustomerBookings(customer) {
    const request = axios({
        url: ROOT_URL + '/' + customer + '/bookings',
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_CUSTOMER_BOOKINGS, payload: {isLoading: true, isError: null}})

        request.then(
            (data) => {
                dispatch({type: FETCH_CUSTOMER_BOOKINGS_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CUSTOMER_BOOKINGS_ERROR, payload: err});
            });
    };
}

export function fetchCustomer(id) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_CUSTOMER, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: FETCH_CUSTOMER_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CUSTOMER_ERROR, payload: err});
            });
    };
}

export function createCustomer(values, history) {
    const request = axios({
        url: ROOT_URL,
        method: 'post',
        data: values,
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: CREATE_CUSTOMER, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: CREATE_CUSTOMER_SUCCESS, payload: data});
                history.push('/admin/customer');
            },
            (err) => {
                dispatch({type: CREATE_CUSTOMER_ERROR, payload: err});
            });
    };
}

export function updateCustomer(id, values, history) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'put',
        data: values,
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: UPDATE_CUSTOMER, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: UPDATE_CUSTOMER_SUCCESS, payload: data});
                history.push('/admin/customer');
            },
            (err) => {
                dispatch({type: UPDATE_CUSTOMER_ERROR, payload: err});
            });
    };
}

export function resetCustomer() {
    return (dispatch) => {
        dispatch({type: RESET_CUSTOMER, payload: null});
    };
}

export function resetCustomers() {
    return (dispatch) => {
        dispatch({type: RESET_CUSTOMERS, payload: null});
    };
}

export function deleteCustomer(id, history = null) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'delete',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: DELETE_CUSTOMER, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: DELETE_CUSTOMER_SUCCESS, payload: data});
                if (history) {
                    history.push('/admin/customer');
                }
            },
            (err) => {
                dispatch({type: DELETE_CUSTOMER_ERROR, payload: err});
            });
    };
}

export function  initForm(data) {
    return (dispatch) => {
        dispatch({
            type: '@@redux-form/INITIALIZE',
            meta: {form: 'CustomerEdit'},
            payload: data,
          })
    }
}


