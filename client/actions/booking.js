import axios from 'axios';
import Auth from '../modules/Auth';

export const FETCH_BOOKINGS = 'FETCH_BOOKINGS';
export const FETCH_BOOKINGS_SUCCESS = 'FETCH_BOOKINGS_SUCCESS';
export const FETCH_BOOKINGS_ERROR = 'FETCH_BOOKINGS_ERROR';
export const FETCH_BOOKING_BOOKINGS = 'FETCH_BOOKING_BOOKINGS';
export const FETCH_BOOKING_BOOKINGS_SUCCESS = 'FETCH_BOOKING_BOOKINGS_SUCCESS';
export const FETCH_BOOKING_BOOKINGS_ERROR = 'FETCH_BOOKING_BOOKINGS_ERROR';
export const FETCH_BOOKING = 'FETCH_BOOKING';
export const FETCH_BOOKING_SUCCESS = 'FETCH_BOOKING_SUCCESS';
export const FETCH_BOOKING_ERROR = 'FETCH_BOOKING_ERROR';
export const CREATE_BOOKING = 'CREATE_BOOKING';
export const CREATE_BOOKING_SUCCESS = 'CREATE_BOOKING_SUCCESS';
export const CREATE_BOOKING_ERROR = 'CREATE_BOOKING_ERROR';
export const UPDATE_BOOKING = 'UPDATE_BOOKING';
export const UPDATE_BOOKING_SUCCESS = 'UPDATE_BOOKING_SUCCESS';
export const UPDATE_BOOKING_ERROR = 'UPDATE_BOOKING_ERROR';
export const DELETE_BOOKING = 'DELETE_BOOKING';
export const DELETE_BOOKING_SUCCESS = 'DELETE_BOOKING_SUCCESS';
export const DELETE_BOOKING_ERROR = 'DELETE_BOOKING_ERROR';
export const RESET_BOOKING = 'RESET_BOOKING';
export const RESET_BOOKINGS = 'RESET_BOOKINGS';

const ROOT_URL = '/api/booking';

export function fetchBookings() {
    const request = axios({
        url: ROOT_URL,
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_BOOKINGS, payload: {isLoading: true, isError: null}})

        request.then(
            (data) => {
                dispatch({type: FETCH_BOOKINGS_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_BOOKINGS_ERROR, payload: err});
            });
    };
}

export function fetchBooking(id) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'get',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: FETCH_BOOKING, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: FETCH_BOOKING_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_BOOKING_ERROR, payload: err});
            });
    };
}

export function createBooking(values, history) {
    const request = axios({
        url: ROOT_URL,
        method: 'post',
        data: values,
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: CREATE_BOOKING, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: CREATE_BOOKING_SUCCESS, payload: data});
                history.push('/admin/booking');
            },
            (err) => {
                dispatch({type: CREATE_BOOKING_ERROR, payload: err});
            });
    };
}

export function updateBooking(id, values, history) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'put',
        data: values,
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: UPDATE_BOOKING, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: UPDATE_BOOKING_SUCCESS, payload: data});
                history.push('/admin/booking');
            },
            (err) => {
                dispatch({type: UPDATE_BOOKING_ERROR, payload: err});
            });
    };
}

export function resetBooking() {
    return (dispatch) => {
        dispatch({type: RESET_BOOKING, payload: null});
    };
}

export function resetBookings() {
    return (dispatch) => {
        dispatch({type: RESET_BOOKINGS, payload: null});
    };
}

export function deleteBooking(id, history = null) {
    const request = axios({
        url: ROOT_URL + '/' + id,
        method: 'delete',
        headers: Auth.generateHeader()
    });

    return (dispatch) => {
        dispatch({type: DELETE_BOOKING, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: DELETE_BOOKING_SUCCESS, payload: data});
                if (history) {
                    history.push('/admin/booking');
                }
            },
            (err) => {
                dispatch({type: DELETE_BOOKING_ERROR, payload: err});
            });
    };
}

export function initForm(data) {
    return (dispatch) => {
        dispatch({
            type: '@@redux-form/INITIALIZE',
            meta: {form: 'BookingEdit'},
            payload: data,
        })
    }
}


