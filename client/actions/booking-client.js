import axios from 'axios';

// export const FETCH_BOOKINGS = 'FETCH_BOOKINGS';
// export const FETCH_BOOKINGS_SUCCESS = 'FETCH_BOOKINGS_SUCCESS';
// export const FETCH_BOOKINGS_ERROR = 'FETCH_BOOKINGS_ERROR';
// export const FETCH_BOOKING_BOOKINGS = 'FETCH_BOOKING_BOOKINGS';
// export const FETCH_BOOKING_BOOKINGS_SUCCESS = 'FETCH_BOOKING_BOOKINGS_SUCCESS';
// export const FETCH_BOOKING_BOOKINGS_ERROR = 'FETCH_BOOKING_BOOKINGS_ERROR';
// export const FETCH_BOOKING = 'FETCH_BOOKING';
// export const FETCH_BOOKING_SUCCESS = 'FETCH_BOOKING_SUCCESS';
// export const FETCH_BOOKING_ERROR = 'FETCH_BOOKING_ERROR';
export const CREATE_BOOKING_CLIENT = 'CREATE_BOOKING_CLIENT';
export const CREATE_BOOKING_CLIENT_SUCCESS = 'CREATE_BOOKING_CLIENT_SUCCESS';
export const CREATE_BOOKING_CLIENT_ERROR = 'CREATE_BOOKING_CLIENT_ERROR';
export const FETCH_CITIES_CLIENT = 'FETCH_CITIES_CLIENT';
export const FETCH_CITIES_CLIENT_SUCCESS = 'FETCH_CITIES_CLIENT_SUCCESS';
export const FETCH_CITIES_CLIENT_ERROR = 'FETCH_CITIES_CLIENT_ERROR';
// export const UPDATE_BOOKING = 'UPDATE_BOOKING';
// export const UPDATE_BOOKING_SUCCESS = 'UPDATE_BOOKING_SUCCESS';
// export const UPDATE_BOOKING_ERROR = 'UPDATE_BOOKING_ERROR';
// export const DELETE_BOOKING = 'DELETE_BOOKING';
// export const DELETE_BOOKING_SUCCESS = 'DELETE_BOOKING_SUCCESS';
// export const DELETE_BOOKING_ERROR = 'DELETE_BOOKING_ERROR';
export const SET_BOOKING_INPUT_CLIENT = 'SET_BOOKING_INPUT_CLIENT';
export const RESET_BOOKING_INPUT_CLIENT = 'RESET_BOOKING_INPUT_CLIENT';
export const RESET_BOOKING_CLIENT = 'RESET_BOOKING_CLIENT';
export const RESET_BOOKINGS_CLIENT = 'RESET_BOOKINGS_CLIENT';
export const RECALCULATE_PRICE = 'RECALCULATE_PRICE';

const ROOT_URL = '/api/client';

// export function fetchBookings() {
//     const request = axios.get(ROOT_URL);
//
//     return (dispatch) => {
//         dispatch({type: FETCH_BOOKINGS, payload: {isLoading: true, isError: null}})
//
//         request.then(
//             (data) => {
//                 dispatch({type: FETCH_BOOKINGS_SUCCESS, payload: data});
//             },
//             (err) => {
//                 dispatch({type: FETCH_BOOKINGS_ERROR, payload: err});
//             });
//     };
// }
//
// export function fetchBooking(id) {
//     const request = axios.get(ROOT_URL + '/' + id);
//
//     return (dispatch) => {
//         dispatch({type: FETCH_BOOKING, payload: {isLoading: true, isError: null}});
//
//         request.then(
//             (data) => {
//                 dispatch({type: FETCH_BOOKING_SUCCESS, payload: data});
//             },
//             (err) => {
//                 dispatch({type: FETCH_BOOKING_ERROR, payload: err});
//             });
//     };
// }

export function createBooking(values, history) {
    const request = axios.post(ROOT_URL + '/booking', values);

    return (dispatch) => {
        dispatch({type: CREATE_BOOKING_CLIENT, payload: {isLoading: true, isError: null}});

        request.then(
            (data) => {
                dispatch({type: CREATE_BOOKING_CLIENT_SUCCESS, payload: data});
                history.push('/success');
            },
            (err) => {
                dispatch({type: CREATE_BOOKING_CLIENT_ERROR, payload: err});
                dispatch({type: SET_BOOKING_INPUT_CLIENT, payload: values});
                history.push('/failure');
            });
    };
}

export function fetchCities() {
    const request = axios.get(ROOT_URL + '/city');

    return (dispatch) => {
        dispatch({type: FETCH_CITIES_CLIENT, payload: {isLoading: true, isError: null}})

        request.then(
            (data) => {
                dispatch({type: FETCH_CITIES_CLIENT_SUCCESS, payload: data});
            },
            (err) => {
                dispatch({type: FETCH_CITIES_CLIENT_ERROR, payload: err});
            });
    };
}

// export function updateBooking(id, values, history) {
//     const request = axios.put(ROOT_URL + '/' + id, values);
//
//     return (dispatch) => {
//         dispatch({type: UPDATE_BOOKING, payload: {isLoading: true, isError: null}});
//
//         request.then(
//             (data) => {
//                 dispatch({type: UPDATE_BOOKING_SUCCESS, payload: data});
//                 history.push('/admin/booking');
//             },
//             (err) => {
//                 dispatch({type: UPDATE_BOOKING_ERROR, payload: err});
//             });
//     };
// }

export function resetBooking() {
    return (dispatch) => {
        dispatch({type: RESET_BOOKING_CLIENT, payload: null});
    };
}

export function resetBookingInput() {
    return (dispatch) => {
        dispatch({type: RESET_BOOKING_INPUT_CLIENT, payload: null});
    };
}

export function resetBookings() {
    return (dispatch) => {
        dispatch({type: RESET_BOOKINGS_CLIENT, payload: null});
    };
}

// export function deleteBooking(id, history = null) {
//     const request = axios.delete(ROOT_URL + '/' + id);
//
//     return (dispatch) => {
//         dispatch({type: DELETE_BOOKING, payload: {isLoading: true, isError: null}});
//
//         request.then(
//             (data) => {
//                 dispatch({type: DELETE_BOOKING_SUCCESS, payload: data});
//                 if (history) {
//                     history.push('/admin/booking');
//                 }
//             },
//             (err) => {
//                 dispatch({type: DELETE_BOOKING_ERROR, payload: err});
//             });
//     };
// }

export function initForm(data) {
    return (dispatch) => {
        dispatch({
            type: '@@redux-form/INITIALIZE',
            meta: {form: 'BookingEdit'},
            payload: data,
          })
    }
}

export function recalculatePrice(data) {
    return (dispatch) => {
        dispatch({
            type: RECALCULATE_PRICE,
            payload: data
        });
    }
}


