"use strict";

import React, {Component} from 'react';
import {render} from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
//import promise from 'redux-promise';
import rootReducer from './reducers';

import App from './components/App';

// const store = createStore(
//     rootReducer,
//     compose(
//         applyMiddleware(thunk),
//         //applyMiddleware(promise),
//         window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
//     )
// );

const store = createStore(
    rootReducer,
    applyMiddleware(thunk)
);

render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('app')
);