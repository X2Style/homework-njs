class Auth {

    /**
     * Authenticate a user. Save a token string in Local Storage
     *
     * @param {string} token
     */
    static authenticateUser(token, userName) {
        localStorage.setItem('token', token);
        localStorage.setItem('name', userName);
    }

    /**
     * Check if a user is authenticated - check if a token is saved in Local Storage
     *
     * @returns {boolean}
     */
    static isUserAuthenticated() {
        return localStorage.getItem('token') !== null;
    }

    /**
     * Deauthenticate a user. Remove a token from Local Storage.
     *
     */
    static deauthenticateUser() {
        localStorage.removeItem('token');
        localStorage.removeItem('name');
    }

    /**
     * Get a token value.
     *
     * @returns {string}
     */

    static getToken() {
        return localStorage.getItem('token');
    }

    static getUserName() {
        return localStorage.getItem('name');
    }

    static generateHeader() {
        if (this.getToken()) {
            return {'Authorization': `Bearer ${this.getToken()}`};
        }
        return {};
    }

}

export default Auth;