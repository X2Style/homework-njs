import React, {Component} from 'react';
import {PropTypes} from 'prop-types';

class MultiCheckboxField extends Component {
    constructor() {
        super();

        this.getCurrentValues = this.getCurrentValues.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    getCurrentValues() {
        const isUndefined = val => typeof(val) === 'undefined';
        const isArray = val => Array.isArray(val);
        const {field} = this.props;
        const {value, initialValue} = field;

        let previousValues = [];

        if (!isUndefined(value) && value !== '') {
            previousValues = value;
        }
        else if (!isUndefined(initialValue) && initialValue !== '') {
            previousValues = initialValue;
        }

        const currentValues = isArray(previousValues) ? [...previousValues] : [previousValues];
        return currentValues;
    }

    handleChange(event, id) {
        const {field} = this.props;
        const {onChange} = field;
        const values = this.getCurrentValues();

        if (event.target.checked) {
            values.push(id);
        }
        else {
            values.splice(values.findIndex((element) => {
                if (element._id === id._id) {
                    return true;
                }
            }), 1);
        }

        return onChange(values);
    }

    render() {
        const {label, options, field} = this.props;
        const {onBlur} = field;
        const values = this.getCurrentValues();
        return (
            <div className="form-group clearfix">
                {label &&
                <label>{label}</label>
                }
                {options.map((option) => {
                    const isChecked = (values.findIndex((element) => {
                            if (element._id === option._id) {
                                return true;
                            } else {
                                return false;
                            }
                        })) > -1;

                    return (
                        <div
                            key={option._id}
                            className="col-md-4"
                        >
                            <label className="checkbox-inline">
                                <input
                                    {...field}
                                    type="checkbox"
                                    onChange={event => this.handleChange(event, option)}
                                    onBlur={() => onBlur(values)}
                                    checked={isChecked}
                                    value={option._id}
                                />
                                {option.name}
                            </label>
                        </div>
                    );
                })}
            </div>
        );
    }
}

// MultiCheckboxField.propTypes = {
//     label: PropTypes.oneOfType([
//         PropTypes.string,
//         PropTypes.node
//     ]),
//     options: PropTypes.arrayOf(
//         PropTypes.shape({
//             _id: PropTypes.oneOfType([
//                 PropTypes.string,
//                 PropTypes.number
//             ]).isRequired,
//             name: PropTypes.oneOfType([
//                 PropTypes.string,
//                 PropTypes.node
//             ]).isRequired
//         })
//     ).isRequired,
//     field: PropTypes.object.isRequired
// };

export default MultiCheckboxField;