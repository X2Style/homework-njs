import React, {Component} from 'react';
import PropTypes from 'prop-types';
import DateTimeField from 'react-bootstrap-datetimepicker';
import moment from 'moment';

class bootstrapDatepicker extends Component {
    static propTypes = {
        input: PropTypes.shape({
            onChange: PropTypes.func.isRequired,
            value: PropTypes.string.isRequired,
        }).isRequired,
        meta: PropTypes.shape({
            touched: PropTypes.bool,
            error: PropTypes.bool,
        }),
        placeholder: PropTypes.string,
    }

    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(date) {
        this.props.input.onChange(moment(date).format('DD-MM-YYYY HH:00'))
    }

    render() {
        const {
            input, placeholder, defaultValue, dateFormat, timeFormat, locale,
            meta: {touched, error}
        } = this.props;

        return (
            <div className="form-group">
                <DateTimeField
                    {...input}/>
                {touched && error && <span>{error}</span>}
            </div>
        )
    }
}

export default bootstrapDatepicker