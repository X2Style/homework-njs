import React from 'react';

export default function renderFieldSelect({input, label, placeholder, disabledOption, options, type, meta: {touched, valid, error, invalid, warning}}) {
    const optionsList = options.map((option) => {
        return (
            <option key={option.id} value={option.id} data-v={option.data}>{option.label}</option>
        );
    });
    return (
        <div className={`form-group ${touched && invalid ? 'validation-error' : ''} ${valid ? 'validation-success' : ''}`}>
            <label className="control-label">{label}</label>
            <div>
                <select {...input} className="form-control" placeholder={placeholder} type={type}>
                    {disabledOption && (
                    <option disabled="true"></option>
                    )}
                    {optionsList}
                </select>
                <div className="help-block">
                    {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
                </div>
            </div>
        </div>
    );
}