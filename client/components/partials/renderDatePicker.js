import React from 'react';
import PropTypes from 'prop-types';
import Datetime from 'react-datetime';
import moment from 'moment';

class renderDatePicker extends React.Component {
    static propTypes = {
        input: PropTypes.shape({
            onChange: PropTypes.func.isRequired,
            value: PropTypes.string.isRequired,
        }).isRequired,
        meta: PropTypes.shape({
            touched: PropTypes.bool,
            error: PropTypes.bool,
        }),
        placeholder: PropTypes.string,
    }

    static defaultProps = {
        placeholder: ''
    }

    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(date) {
        this.props.input.onChange(moment(date).format('DD-MM-YYYY HH:00'))
    }

    render() {
        const {
            input, placeholder, defaultValue, dateFormat, timeFormat, locale,
            meta: {touched, error}
        } = this.props;

        return (
            <div className="form-group">
                <Datetime
                    {...input}
                    placeholder={placeholder}
                    defaultValue={defaultValue}
                    dateFormat={dateFormat}
                    timeFormat={timeFormat}
                    locale={locale}
                    onChange={this.handleChange}
                />
                {touched && error && <span>{error}</span>}
            </div>
        )
    }
}

export default renderDatePicker