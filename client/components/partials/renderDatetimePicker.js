import React, {Component} from 'react';
import moment from 'moment';
import {DateTimePicker} from 'react-widgets';
import momentLocaliser from 'react-widgets/lib/localizers/moment';

moment().locale('en-gb');
momentLocaliser(moment);

const renderDateTimePicker = ({input: {onChange, value}, label, showTime, step, min}) =>
    <div className="form-group">
        <label className="control-label">{label}</label>
        <DateTimePicker
            onChange={onChange}
            format="DD-MM-YYYY HH:mm"
            culture="en-gb"
            step={step}
            min={min}
            time={showTime}
            value={!value ? null : new Date(value)}
        />
    </div>

export default renderDateTimePicker;