import React from 'react';
import PropTypes from 'prop-types';
import Datetime from 'react-datetime';
import moment from 'moment';

const renderDatePicker = ({input, placeholder, defaultValue, meta: {touched, error} }) => (
    <div>
        <Datetime {...input} dateFormat="MM-DD-YYYY" timeFormat="HH:mm" selected={input.value ? moment(input.value) : null} />
        {touched && error && <span>{error}</span>}
    </div>
);

export default renderDatePicker