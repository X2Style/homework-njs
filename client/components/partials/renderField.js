import React from 'react';

export function renderField({ input, inputOptions, label, placeholder, type, meta: { touched, valid, error, invalid, warning } }) {
    return (
        <div className={`form-group ${touched && invalid ? 'validation-error' : ''} ${valid ? 'validation-success' : ''}`}>
            <label className="control-label">{label}</label>
            <div>
                <input {...input} {...inputOptions} className="form-control" placeholder={placeholder} type={type}/>
                <div className="help-block">
                    {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
                </div>
            </div>
        </div>
    );
}