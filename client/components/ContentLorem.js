import React, {Component} from 'react';

export default class ContentLorem extends Component {
    render() {
        return (
            <div className="container">
                <div className="row text-center">
                    <div className="col-md-6 col-md-offset-3"><h1>WHAT WE DO</h1></div>
                </div>
                <hr/>
                <div className="row">
                    <div className="text-justify col-sm-3">
                        <h3>Office cleaning</h3>
                        <p className="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint.</p>
                        <div className="text-right">
                            <button className="btn btn-green">Read more</button>
                        </div>
                    </div>
                    <div className="col-sm-3 text-justify">
                        <h3>Residental cleaning</h3>
                        <p className="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.</p>
                        <div className="text-right">
                            <button className="btn btn-green">Read more</button>
                        </div>
                    </div>
                    <div className="col-sm-3 text-justify">
                        <h3>Commercial cleanings</h3>
                        <p className="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur.</p>
                        <div className="text-right">
                            <button className="btn btn-green">Read more</button>
                        </div>
                    </div>
                    <div className="col-sm-3 text-justify">
                        <h3>Construction Clean-up</h3>
                        <p className="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.</p>
                        <div className="text-right">
                            <button className="btn btn-green">Read more</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};