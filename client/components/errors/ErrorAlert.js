import React, {Component} from 'react';

export default class ErrorAlert extends Component {

    render() {
        const {errorMessage} = this.props;

        return (
            <div className="alert alert-danger">
                { Array.isArray(errorMessage) ? (
                        <ul>
                            {errorMessage.map((error) => {
                                return (
                                    <li key={error.name}>{error.message}</li>
                                );
                            })}
                        </ul>
                    ) : (
                        'Error: ' + errorMessage
                    )}
            </div>
        );
    }
}