import React, {Component} from 'react';

export default class ErrorAlert extends Component {

    render() {
        const {errorMessage} = this.props;
        const errors = [];
        for (var key in errorMessage.errors) {
            if (!errorMessage.errors.hasOwnProperty(key)) continue;
            errors.push({name: key, message: errorMessage.errors[key]});
        }
        return (
            <div className="alert alert-danger">
                <div>{errorMessage.message}
                    {errors.length > 0 &&
                    <ul>
                        {errors.map((error) => {
                            return (
                                <li key={error.name}>{error.message}</li>
                            );
                        })}
                    </ul>
                    }
                </div>
            </div>
        );
    }
}