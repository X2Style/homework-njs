import React, {Component} from 'react';

export default class NotFound extends Component {
    render() {
        return (
            <div className="row content-container">
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-danger">
                        Error: Not Found
                    </div>
                </div>
            </div>
        );
    }
};