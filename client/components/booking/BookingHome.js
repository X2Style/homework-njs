import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import moment from 'moment';
import {createBooking, fetchCities} from '../../actions/booking-client';
import FormBooking from './FormBooking';
import ContentLorem from '../ContentLorem';

class BookingHome extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.fetchCities();
    }

    onSubmit(values) {
        this.props.createBooking(values, this.context.router.history);
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    };

    minDate() {
        const min = moment().seconds(0).milliseconds(0);
        if (min.minutes() > 30) {
            min.minutes(0).add('2', 'h');
        } else {
            min.minutes(0).add('1', 'h');
        }
        return min.toDate();
    }

    render() {
        const {client, initForm} = this.props;
        if (client.cities.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (client.cities.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-danger">Error: {client.cities.isError.error}</div>
                </div>
            );
        }
        return (
            <div>
                <div className="row booking-form-container">
                    <div className="col-md-5 col-md-offset-1">
                        <div className="booking-form">
                            <h2 className="text-center">Book now</h2>
                            <FormBooking
                                onSubmit={this.onSubmitHandler.bind(this)}
                                cities={client.cities.data}
                                initForm={initForm}
                                minDate={this.minDate()}
                                btnClass="btn-green"
                                btnText="Book now"
                                initialValues={{
                                    "dateFrom": this.minDate()
                                }}/>
                        </div>
                    </div>
                    <div className="col-md-4 col-md-offset-1 slogan text-center">
                        <h1>Enjoy your time off.</h1>
                        <h3>Relax knowing your home will be cleaned on schedule.</h3>
                    </div>
                </div>
                <ContentLorem/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {client: state.client};
}

export default withRouter(connect(mapStateToProps, {createBooking, fetchCities})(BookingHome));