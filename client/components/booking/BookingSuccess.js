import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import moment from 'moment';
import {resetBooking} from '../../actions/booking-client';

class BookingSuccess extends Component {

    componentWillUnmount() {
        this.props.resetBooking();
    }

    render() {
        const {client} = this.props;
        if (!client.booking.data) {
            return (
                <Redirect to="/"/>
            );
        }
        let stars = [];
        for (var i = 0; i < Math.round(client.booking.data.cleaner.qualityScore); i++) {
            stars.push(
                <span key={i} className="glyphicon glyphicon-star"></span>
            );
        }
        const duration = moment(client.booking.data.dateTo).diff(moment(client.booking.data.dateFrom), 'hours');
        return (
            <div className="booking-details row">
                <div className="col-md-8 col-md-offset-2">
                    <div className="row">
                        <div className="col-md-10 col-md-offset-1 booking-info">
                            <div className="booking-details-message">
                                <h1 className="text-center" data-select-like-a-boss="1">Your booking accepted</h1>
                                <h2 className="text-center">We'll be in touch you soon</h2>
                            </div>
                            <div className="panel panel-success">
                                <div className="panel-heading">Booking details</div>
                                <div className="panel-body">
                                    <table className="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td className="text-right"><strong>Date:</strong></td>
                                            <td>{moment(client.booking.data.dateFrom).format('DD-MM-YYYY')}</td>
                                        </tr>
                                        <tr>
                                            <td className="text-right"><strong>Time:</strong></td>
                                            <td>{moment(client.booking.data.dateFrom).format('HH:mm')}</td>
                                        </tr>
                                        <tr>
                                            <td className="text-right"><strong>Duration:</strong></td>
                                            <td>{duration > 1 ? duration + ' Hours' : duration + ' Hour'}</td>
                                        </tr>
                                        <tr>
                                            <td className="text-right"><strong>Price:</strong></td>
                                            <td>{ +(Math.round(client.booking.data.price + "e+2")  + "e-2")} $</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <p>Cleaner details</p>
                                    <table className="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td className="text-right"><strong>Name:</strong></td>
                                            <td>{client.booking.data.cleaner.firstName} {client.booking.data.cleaner.lastName}</td>
                                        </tr>
                                        <tr>
                                            <td className="text-right"><strong>Rating:</strong></td>
                                            <td>
                                                {stars} {client.booking.data.cleaner.qualityScore} Points
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {client: state.client};
}

export default connect(mapStateToProps, {resetBooking})(BookingSuccess);