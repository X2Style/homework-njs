import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import moment from 'moment';
import FormBooking from './FormBooking';
import {resetBookingInput, fetchCities, createBooking} from '../../actions/booking-client';

class BookingFailure extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.fetchCities();
    }

    componentWillUnmount() {
        this.props.resetBookingInput();
    }

    onSubmit(values) {
        this.props.createBooking(values, this.context.router.history);
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    };

    minDate() {
        const min = moment().seconds(0).milliseconds(0);
        if (min.minutes() > 30) {
            min.minutes(0).add('2', 'h');
        } else {
            min.minutes(0).add('1', 'h');
        }
        return min.toDate();
    }

    render() {
        const {client, initForm} = this.props;
        if (!client.input) {
            return (
                <Redirect to="/"/>
            );
        }
        if (client.cities.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (client.cities.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-danger">Error: {client.cities.isError.error}</div>
                </div>
            );
        }
        return (
            <div className="booking-details row">
                <div className="col-md-8 col-md-offset-2">
                    <div className="row">
                        <div className="col-md-10 col-md-offset-1 booking-info">
                            {client.booking.isError &&
                            <div className="alert alert-danger">{client.booking.isError.error}</div>
                            }
                            <div className="panel panel-danger">
                                <div className="panel-heading">Booking details</div>
                                <div className="panel-body">
                                    <FormBooking
                                        onSubmit={this.onSubmitHandler.bind(this)}
                                        cities={client.cities.data}
                                        initForm={initForm}
                                        minDate={this.minDate()}
                                        btnClass="btn-green"
                                        btnText="Try again"
                                        initialValues={client.input}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {client: state.client};
}

export default connect(mapStateToProps, {fetchCities, createBooking, resetBookingInput})(BookingFailure);