import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {renderField} from '../partials/renderField';
import renderDatetimePicker from '../partials/renderDatetimePicker';
import renderFieldSelect from '../partials/renderFieldSelect';
import validator from 'validator';
import {isDateSameOrAfter} from '../../helper';
import {regexUnicodeHelper} from '../../helper';
import {recalculatePrice} from '../../actions/booking-client';

class FormBooking extends Component {
    componentDidMount() {
        if (this.props.initForm) {
            this.props.initForm(this.props.initialValues);
        }
    }

    onChangeCity(v) {
        console.log(v.target.options[v.target.options.selectedIndex].dataset.v);
    }

    onChangeDuration() {

    }
    render() {
        const {handleSubmit, cities, minDate, btnClass, btnText} = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <div className="row">
                    <div className="col-md-6">
                        <Field
                            name="dateFrom"
                            label="Date & time"
                            step={60}
                            component={renderDatetimePicker}
                            min={minDate}
                        />
                        <Field
                            name="duration"
                            label="Duration"
                            component={renderFieldSelect}
                            disabledOption={true}
                            options={[
                                {id: 1, label: '1 Hour'},
                                {id: 2, label: '2 Hours'},
                                {id: 3, label: '3 Hours'},
                                {id: 4, label: '4 Hours'},
                                {id: 5, label: '5 Hours'},
                                {id: 6, label: '6 Hours'},
                                {id: 7, label: '7 Hours'},
                                {id: 8, label: '8 Hours'},
                            ]}
                        />
                        <Field
                            name="city"
                            label="City"
                            component={renderFieldSelect}
                            onChange={this.onChangeCity.bind(this)}
                            disabledOption={true}
                            options={
                                cities.map((city) => {
                                    return (
                                        {id: city._id, label: city.name, data: city.priceFactor}
                                    );
                                })
                            }
                        />
                        <div>Price: </div>
                    </div>
                    <div className="col-md-6">
                        <Field
                            name="firstName"
                            type="text"
                            component={renderField}
                            label="First Name"
                            className="form-control"
                            placeholder="First Name"
                        />
                        <Field
                            name="lastName"
                            type="text"
                            component={renderField}
                            label="Last Name"
                            className="form-control"
                            placeholder="Last Name"
                        />
                        <Field
                            name="email"
                            type="text"
                            component={renderField}
                            label="E-mail"
                            className="form-control"
                            placeholder="E-mail"
                        />
                        <Field
                            name="phone"
                            type="text"
                            component={renderField}
                            label="Phone"
                            className="form-control"
                            placeholder="Phone"
                            normalize={normalizePhone}
                        />
                    </div>
                </div>
                <div className="text-center">
                    <button className={`btn btn-lg ${btnClass}`}>{btnText}</button>
                </div>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};
    const nameValidationRegex = new RegExp('^[' + regexUnicodeHelper() + "]+(?:[-\\s'][" + regexUnicodeHelper() + "]+)*([']?)$");

    if (!values.dateFrom) {
        errors.dateFrom = 'Required';
    } else if (!(isDateSameOrAfter(values.dateFrom))) {
        errors.dateFrom = 'Invalid date';
    }

    if (!values.duration) {
        errors.duration = 'Required';
    } else if (values.duration.trim() < 1 || values.duration.trim() > 8) {
        errors.duration = 'Must be from 1 to 8 Hours';
    } else if (!validator.isInt(values.duration.trim())) {
        errors.duration = 'Must be from 1 to 8 Hours';
    }

    if (!values.city) {
        errors.city = 'Required';
    } else if (!validator.isMongoId(values.city.trim())) {
        errors.city = 'Invalid city';
    }

    if (!values.firstName) {
        errors.firstName = 'Required';
    } else if (values.firstName.trim().length < 2) {
        errors.firstName = 'Must be at least 2 characters';
    } else if (values.firstName.trim().length > 100) {
        errors.firstName = 'Must be not more than 100 characters';
    } else if (!nameValidationRegex.test(values.firstName.trim())) {
        errors.firstName = 'First name is invalid';
    }

    if (!values.lastName) {
        errors.lastName = 'Required';
    } else if (values.lastName.trim().length < 2) {
        errors.lastName = 'Must be at least 2 characters';
    } else if (values.lastName.trim().length > 100) {
        errors.lastName = 'Must be not more than 100 characters';
    } else if (!nameValidationRegex.test(values.lastName.trim())) {
        errors.lastName = 'Last name is invalid';
    }

    if (!values.email) {
        errors.email = 'Required';
    } else if (!validator.isEmail(values.email.trim())) {
        errors.email = 'Enter a valid e-mail address';
    }

    if (values.phone && !/^\s*((\+?\s*(\(\s*)?3)?[\s-]*(\(\s*)?8[\s-]*)?(\(\s*)?0[\s-\(]*[1-9][\s-]*\d(\s*\))?([\s-]*\d){7}\s*$/.test(values.phone.trim())) {
        errors.phone = 'Enter a valid phone number';
    }

    return errors;
}

function normalizePhone(value) {
    if (!value) {
        return value
    }

    const onlyNums = value.replace(/[^\d]/g, '')
    if (onlyNums.length <= 3) {
        return `(${onlyNums}`;
    }
    if (onlyNums.length <= 6) {
        return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3)}`;
    }
    if (onlyNums.length <= 8) {
        return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(6)}`;
    }
    return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(6, 8)}-${onlyNums.slice(8, 10)}`;
}

export default reduxForm({
    form: 'BookingEdit',
    validate,
    fields: ['dateFrom', 'dateTo', 'customer', 'cleaner', 'city']
})(FormBooking);