import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link, Switch, Route, withRouter} from 'react-router-dom';
import Navigation from './Navigation';
import NavigationAdmin from './NavigationAdmin';
import {toggleMenu} from '../../actions/navbar';

class Header extends Component {

    onToggleNav() {
        this.props.toggleMenu();
        //console.log(this.props);
    };

    render() {
        return (
            <header>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" onClick={() => {
                                this.onToggleNav()
                            }}><span
                                className="sr-only">Toggle navigation</span><span
                                className="icon-bar"></span><span className="icon-bar"></span><span
                                className="icon-bar"></span>
                            </button>
                            <Link className="navbar-brand" to="/">
                                <img src="/images/logo_full.png" alt="Homework"/>
                            </Link>
                        </div>
                        <div>
                        <Switch>
                            <Route path="/admin" component={NavigationAdmin}/>}/>
                            <Route path="/" component={Navigation}/>}/>
                        </Switch>
                        </div>
                    </div>
                </nav>
            </header>
        );
    }
}

function mapStateToProps(state) {
    return {navbar: state.navbar};
}

export default withRouter(connect(mapStateToProps, {toggleMenu})(Header));