import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Auth from '../../modules/Auth';

class NavigationAdmin extends Component {
    render() {
        const {navCollapsed} = this.props.navbar;

        return (
            <div className={(navCollapsed ? 'collapse' : '') + ' navbar-collapse'}>
                <ul className="nav navbar-nav">
                    <li><Link to="/admin/booking">Booking</Link></li>
                    <li><Link to="/admin/city">City</Link></li>
                    <li><Link to="/admin/cleaner">Cleaner</Link></li>
                    <li><Link to="/admin/customer">Customer</Link></li>
                </ul>
                <ul className="nav navbar-nav navbar-right">
                    <li>
                        <Link to="/admin"><span className="glyphicon glyphicon-user"></span> {Auth.getUserName()}</Link>
                    </li>
                    <li>
                        <Link to="/logout"><span className="glyphicon glyphicon-log-out"></span> Logout</Link>
                    </li>
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {navbar: state.navbar};
}

export default connect(mapStateToProps)(NavigationAdmin);