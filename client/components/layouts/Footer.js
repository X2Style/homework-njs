import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Footer extends Component {
    render() {
        return (
            <footer className="container">
                <hr/>
                    <div className="row">
                        <div className="text-center small col-md-6 col-md-offset-3">
                            <p>Copyright © 2017 · All Rights Reserved · <Link to="/">Homework</Link></p>
                        </div>
                    </div>
            </footer>
        );
    }
};