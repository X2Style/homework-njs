import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import List from './booking/List';
import Show from './booking/Show';
import Create from './booking/Create';
import Edit from './booking/Edit';

export default class Booking extends Component {

    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/admin/booking" component={List}/>
                    <Route path="/admin/booking/:id/edit" component={Edit}/>
                    <Route path="/admin/booking/create" component={Create}/>
                    <Route path="/admin/booking/:id" component={Show}/>
                </Switch>
            </div>
        );
    }
};