import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {logoutRequest} from '../../../actions/auth';

class Logout extends Component {

    render() {
        this.props.logoutRequest();
        return (
            <div>
               <Redirect to="/" />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {auth: state.auth};
}

export default connect(mapStateToProps, {logoutRequest})(Logout);