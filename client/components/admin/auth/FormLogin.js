import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {renderField} from '../../partials/renderField';
import validator from 'validator';

class FormLogin extends Component {

    componentDidMount() {
        if (this.props.initForm) {
            this.props.initForm(this.props.initialValues);
        }
    }

    render() {
        const {handleSubmit} = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <div className="col-md-8 col-md-offset-2">
                    <Field
                        name="email"
                        type="text"
                        label="E-mail"
                        component={renderField}
                        className="form-control"
                        placeholder="E-mail"
                    />
                    <Field
                        name="password"
                        type="password"
                        label="Password"
                        component={renderField}
                        className="form-control"
                        placeholder="Password"
                    />
                    <div className="form-group">
                        <div className="col-md-8 col-md-offset-4">
                            <button type="submit" className="btn btn-primary">
                                Login
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};

    if (!values.email) {
        errors.email = 'Required';
    } else if (!validator.isEmail(values.email)) {
        errors.email = 'Enter a valid e-mail address';
    }

    if (!values.password) {
        errors.password = 'Required';
    } else if (values.password.length < 8) {
        errors.password = 'Must be at least 8 characters';
    } else if (values.password.length > 64) {
        errors.password = 'Password is too long';
    }

    return errors;
}

export default reduxForm({
    form: 'FormLogin',
    validate,
    fields: ['email', 'password']
})(FormLogin);