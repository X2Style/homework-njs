import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import {loginRequest} from '../../../actions/auth';
import FormLogin from './FormLogin';
import ErrorAuthAlert from '../../errors/ErrorAuthAlert';

class Login extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    onSubmit(values) {
        this.props.loginRequest(values, this.context.router.history);
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    };

    render() {
        const {auth} = this.props;
        return (
            <div className="row content-container">
                <div className="col-md-8 col-md-offset-2">
                    <div className="panel panel-default">
                        <div className="panel-heading">Login</div>
                        <div className="panel-body">
                            {auth.isError &&
                            <ErrorAuthAlert errorMessage={auth.isError}/>
                            }
                            <FormLogin onSubmit={this.onSubmitHandler.bind(this)}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {auth: state.auth};
}

export default withRouter(connect(mapStateToProps, {loginRequest})(Login));