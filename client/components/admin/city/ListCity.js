import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class ListCity extends Component {
    render() {
        const {handleDelete} = this.props;
        const city = this.props.city;
        return (
            <tr key={city._id}>
                <td>
                    <Link to={{ pathname: '/admin/city/' + city._id }}>{city.name}</Link>
                </td>
                <td>
                    {city.priceFactor}
                </td>
                <td className="text-right">
                    <div className="btn-group-xs" role="group">
                        <Link to={{ pathname: '/admin/city/' + city._id + '/edit' }} className="btn btn-default"><span
                            className="glyphicon glyphicon-edit"></span> Edit</Link>
                        <button className="btn btn-danger" onClick={handleDelete}>
                            <span className="glyphicon glyphicon-trash"></span> Remove
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}