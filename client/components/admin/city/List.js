import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchCities, deleteCity} from '../../../actions/city';
import ListCity from './ListCity';
import ErrorAlert from '../../errors/ErrorAlert';

class List extends Component {

    componentDidMount() {
        this.props.fetchCities();
    };

    handleDelete(id) {
        this.props.deleteCity(id);
    }

    renderCities() {
        return this.props.city.all.data.map((city) => {
            return (
                <ListCity key={city._id} city={city} handleDelete={this.handleDelete.bind(this, city._id)}/>
            );
        });
    }

    render() {
        const {city} = this.props;
        if (city.all.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (city.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={city.all.isError.error}/>
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                <div className="panel panel-default">
                    <div className="panel-heading">Cities list</div>
                    <div className="panel-body">
                        {city.all.data.length > 0 ? (
                                <div className="table-responsive">
                                    <table className="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Price Factor</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {this.renderCities()}
                                        </tbody>
                                    </table>
                                </div>
                            ) : (
                                <div className="well text-center">
                                    No cities yet
                                </div>
                            )}
                        <div className="text-center">
                            <Link className="btn btn-default" to="/admin/city/create">Add city</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {city: state.city};
}

export default connect(mapStateToProps, {fetchCities, deleteCity})(List);