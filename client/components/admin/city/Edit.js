import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {fetchCity, updateCity, resetCity, initForm} from '../../../actions/city';
import FormCity from './FormCity';
import ErrorAlert from '../../errors/ErrorAlert';

/*
 export const mapState = state => ({});
 export const mapActions = dispatch => ({
 initForm: payload => dispatch({
 type: '@@redux-form/INITIALIZE',
 meta: {form: 'addItem'},
 payload
 }),
 changeField: payload => dispatch({
 type: '@@redux-form/CHANGE',
 meta: {form: 'addItem',field: payload.name},
 payload: payload.data
 })
 });
 @connect(mapState,mapActions)
 */
class Edit extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.resetCity();
        // console.log(this.props);
        const {id} = this.props.match.params;
        this.props.fetchCity(id);
    }

    onSubmit(values) {
        const {id} = this.props.match.params;
        this.props.updateCity(id, values, this.context.router.history);
        //this.context.router.history.push('/admin/city');
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    }

    render() {
        const {city, initForm} = this.props;
        if (city.current.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (city.current.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={city.current.isError.error}/>
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                {city.all.isError &&
                <ErrorAlert errorMessage={city.all.isError.error}/>
                }
                <div className="panel panel-default">
                    <div className="panel-heading">Edit city</div>
                    <div className="panel-body">
                        {city.current.data ? (
                                <FormCity initialValues={city.current.data}
                                          initForm={initForm}
                                          onSubmit={this.onSubmitHandler.bind(this)}/>
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>
                            )}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {city: state.city};
}

export default withRouter(
    connect(
        mapStateToProps,
        {
            fetchCity,
            updateCity,
            resetCity,
            initForm
        })(Edit));