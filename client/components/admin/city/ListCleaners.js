import React, {Component} from 'react';
import {connect} from 'react-redux';
import {deleteCleaner} from '../../../actions/cleaner';
import {fetchCityCleaners} from '../../../actions/city'
import ListCleaner from '../cleaner/ListCleaner';

class ListCleaners extends Component {

    componentWillMount() {
        const {cityId} = this.props;
        this.props.fetchCityCleaners(cityId);
    };

    handleDelete(id) {
        this.props.deleteCleaner(id);
    }

    renderCleaners() {
        return this.props.city.cleaners.data.map((cleaner) => {
            return (
                <ListCleaner key={cleaner._id} cleaner={cleaner}
                             handleDelete={this.handleDelete.bind(this, cleaner._id)}/>
            );
        });
    }

    render() {
        const {city} = this.props;
        if (city.cleaners.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (city.cleaners.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-danger">Error: {city.cleaners.isError.error}</div>
                </div>
            );
        }
        return (
            <div>
                {city.cleaners.data.length > 0 ? (
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Quality Score</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.renderCleaners()}
                            </tbody>
                        </table>
                    ) : (
                        <div className="well text-center">
                            <span>No cleaners found</span>
                        </div>
                    )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {city: state.city};
}

export default connect(mapStateToProps, {fetchCityCleaners, deleteCleaner})(ListCleaners);