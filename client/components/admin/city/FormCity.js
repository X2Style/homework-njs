import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {renderField} from '../../partials/renderField';
import {regexUnicodeHelper} from '../../../helper';
import validator from 'validator';

class FormCity extends Component {
    componentDidMount() {
        if (this.props.initForm) {
            this.props.initForm(this.props.initialValues);
        }
    }

    /*
     componentDidUpadate() {
     this.props.initForm(this.props.initialValues)
     }
     */

    render() {
        const {handleSubmit} = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <Field
                    name="name"
                    type="text"
                    component={renderField}
                    label="Name"
                    className="form-control"
                    placeholder="Name"/>
                <Field
                    name="priceFactor"
                    type="number"
                    inputOptions={{
                        min: 0.1,
                        step: 0.1
                    }}
                    component={renderField}
                    label="Price Factor"
                    className="form-control"
                    placeholder="Price Factor"/>
                <div className="form-group text-center">
                    <button type="submit" className="btn btn-default">Save</button>
                </div>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};
    const nameValidationRegex = new RegExp('^[' + regexUnicodeHelper() + "]+(?:[-\\s'][" + regexUnicodeHelper() + "]+)*([']?)$");

    if (!values.name) {
        errors.name = 'Required';
    } else if (values.name.trim().length < 2) {
        errors.name = 'Must be at least 2 characters'
    } else if (values.name.trim().length > 100) {
        errors.name = 'Must be not more than 100 characters';
    } else if (!nameValidationRegex.test(values.name.trim())) {
        errors.name = 'City name is invalid';
    }

    if (!values.priceFactor) {
        errors.priceFactor = 'Required';
    } else if (!validator.isFloat(String(values.priceFactor).trim())) {
        errors.priceFactor = 'Must be a number';
    } else if (!(values.priceFactor > 0)) {
        errors.priceFactor = 'Must be greater then 0';
    }
    return errors;
}

export default reduxForm({
    form: 'CityEdit',
    validate,
    fields: ['name']
})(FormCity);