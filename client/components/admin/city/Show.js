import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchCity, resetCity, deleteCity} from '../../../actions/city';
import PropTypes from 'prop-types';
import ListCleaners from './ListCleaners';
import ErrorAlert from '../../errors/ErrorAlert';

class Show extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.resetCity();
        const {id} = this.props.match.params;
        this.props.fetchCity(id);
    };

    handleDelete(id) {
        this.props.deleteCity(id, this.context.router.history);
    }

    render() {
        const {city} = this.props;
        const {id} = this.props.match.params;
        if (city.current.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (city.current.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={city.current.isError.error} />
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                <div className="panel panel-default">
                    <div className="panel-heading">City information</div>
                    <div className="panel-body">
                        {city.current.data ? (
                                <div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Name:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {city.current.data.name}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Price Factor:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {city.current.data.priceFactor}
                                        </div>
                                    </div>
                                    <div className="form-group text-center">
                                        <div className="btn-group" role="group">
                                            <Link
                                                to={{pathname: '/admin/city/' + city.current.data._id + '/edit'}}
                                                className="btn btn-default"><span
                                                className="glyphicon glyphicon-edit"></span> Edit</Link>
                                            <button type="submit" className="btn btn-danger"
                                                    onClick={this.handleDelete.bind(this, city.current.data._id)}>
                                                <span className="glyphicon glyphicon-trash"></span> Remove
                                            </button>
                                        </div>
                                    </div>
                                    <p><strong>Cleaners:</strong></p>
                                    <ListCleaners cityId={id}/>
                                </div>
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>

                            )}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {city: state.city};
}

export default connect(mapStateToProps, {fetchCity, resetCity, deleteCity})(Show);