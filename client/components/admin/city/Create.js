import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import {createCity} from '../../../actions/city';
import FormCity from './FormCity';
import ErrorAlert from '../../errors/ErrorAlert';

class Create extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    // onSubmit(values) {
    //     this.props.createCity(values)
    //         .then((response) => {
    //             this.props.createCitySuccess(response.payload)
    //         })
    //         .then(() => {
    //             this.context.router.history.push('/admin/city');
    //         });
    // }

    onSubmit(values) {
        this.props.createCity(values, this.context.router.history);
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    };

    render() {
        const {city} = this.props;
        return (
            <div className="col-md-10 col-md-offset-1">
                {city.current.isError &&
                <ErrorAlert errorMessage={city.current.isError.error}/>
                }
                <div className="panel panel-default">
                    <div className="panel-heading">Add city</div>
                    <div className="panel-body">
                        <FormCity onSubmit={this.onSubmitHandler.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {city: state.city};
}

export default withRouter(connect(mapStateToProps, {createCity})(Create));