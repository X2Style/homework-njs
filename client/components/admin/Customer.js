import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import List from './customer/List';
import Show from './customer/Show';
import Create from './customer/Create';
import Edit from './customer/Edit';

export default class Customer extends Component {

    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/admin/customer" component={List}/>
                    <Route path="/admin/customer/:id/edit" component={Edit}/>
                    <Route path="/admin/customer/create" component={Create}/>
                    <Route path="/admin/customer/:id" component={Show}/>
                </Switch>
            </div>
        );
    }
};