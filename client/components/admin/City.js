import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import List from './city/List';
import Show from './city/Show';
import Create from './city/Create';
import Edit from './city/Edit';

export default class City extends Component {

    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/admin/city" component={List}/>
                    <Route path="/admin/city/:id/edit" component={Edit}/>
                    <Route path="/admin/city/create" component={Create}/>
                    <Route path="/admin/city/:id" component={Show}/>
                </Switch>
            </div>
        );
    }
};