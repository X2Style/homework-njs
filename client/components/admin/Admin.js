import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Auth from '../../modules/Auth';
import Dashboard from './Dashboard';
import Booking from './Booking';
import City from './City';
import Cleaner from './Cleaner';
import Customer from './Customer';
import Logout from './auth/Logout';
import Login from './auth/Login';

export default class Admin extends Component {

    render() {
        return (
            <div className="row content-container">
                {Auth.isUserAuthenticated() ? (
                <Switch>
                    <Route path="/logout" component={Logout}/>
                    <Route exact path="/admin" component={Dashboard}/>
                    <Route path="/admin/booking" component={Booking}/>
                    <Route path="/admin/city" component={City}/>
                    <Route path="/admin/cleaner" component={Cleaner}/>
                    <Route path="/admin/customer" component={Customer}/>
                </Switch>
                    ) : (
                        <Redirect to="/login"/>
                    )}
            </div>
        );
    }
};