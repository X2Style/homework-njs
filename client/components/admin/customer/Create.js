import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import {createCustomer} from '../../../actions/customer';
import FormCustomer from './FormCustomer';
import ErrorAlert from '../../errors/ErrorAlert';

class Create extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    onSubmit(values) {
        this.props.createCustomer(values, this.context.router.history);
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    };

    render() {
        const {customer} = this.props;
        return (
            <div className="col-md-10 col-md-offset-1">
                {customer.current.isError &&
                <ErrorAlert errorMessage={customer.current.isError.error} />
                }
                <div className="panel panel-default">
                    <div className="panel-heading">Add customer</div>
                    <div className="panel-body">
                        <FormCustomer onSubmit={this.onSubmitHandler.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {customer: state.customer};
}

export default withRouter(connect(mapStateToProps, {createCustomer})(Create));