import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {renderField} from '../../partials/renderField';
import {regexUnicodeHelper} from '../../../helper';
import validator from 'validator';

class FormCustomer extends Component {
    componentDidMount() {
        if (this.props.initForm) {
            this.props.initForm(this.props.initialValues);
        }
    }

    render() {
        const {handleSubmit} = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <Field
                    name="firstName"
                    type="text"
                    component={renderField}
                    label="First Name"
                    className="form-control"
                    placeholder="First Name"/>
                <Field
                    name="lastName"
                    type="text"
                    component={renderField}
                    label="Last Name"
                    className="form-control"
                    placeholder="Last Name"/>
                <Field
                    name="email"
                    type="email"
                    component={renderField}
                    label="E-mail"
                    className="form-control"
                    placeholder="E-mail"/>
                <Field
                    name="phone"
                    type="text"
                    component={renderField}
                    label="Phone"
                    className="form-control"
                    placeholder="(067) 123-45-67"
                    normalize={normalizePhone}/>
                <div className="form-group text-center">
                    <button type="submit" className="btn btn-default">Save</button>
                </div>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};
    const nameValidationRegex = new RegExp('^[' + regexUnicodeHelper() + "]+(?:[-\\s'][" + regexUnicodeHelper() + "]+)*([']?)$");

    if (!values.firstName) {
        errors.firstName = 'Required';
    } else if (values.firstName.trim().length < 2) {
        errors.firstName = 'Must be at least 2 characters';
    } else if (values.firstName.trim().length > 100) {
        errors.firstName = 'Must be not more than 100 characters';
    } else if (!nameValidationRegex.test(values.firstName.trim())) {
        errors.firstName = 'First name is invalid';
    }

    if (!values.lastName) {
        errors.lastName = 'Required';
    } else if (values.lastName.trim().length < 2) {
        errors.lastName = 'Must be at least 2 characters';
    } else if (values.lastName.trim().length > 100) {
        errors.lastName = 'Must be not more than 100 characters';
    } else if (!nameValidationRegex.test(values.lastName.trim())) {
        errors.lastName = 'Last name is invalid';
    }

    if (!values.email) {
        errors.email = 'Required';
    } else if (!validator.isEmail(values.email.trim())) {
        errors.email = 'Enter a valid e-mail address';
    }

    if (values.phone && !/^\s*((\+?\s*(\(\s*)?3)?[\s-]*(\(\s*)?8[\s-]*)?(\(\s*)?0[\s-\(]*[1-9][\s-]*\d(\s*\))?([\s-]*\d){7}\s*$/.test(values.phone.trim())) {
        errors.phone = 'Enter a valid phone number';
    }

    return errors;
}

function normalizePhone(value) {
    if (!value) {
        return value
    }

    const onlyNums = value.replace(/[^\d]/g, '')
    if (onlyNums.length <= 3) {
        return `(${onlyNums}`;
    }
    if (onlyNums.length <= 6) {
        return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3)}`;
    }
    if (onlyNums.length <= 8) {
        return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(6)}`;
    }
    return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(6, 8)}-${onlyNums.slice(8, 10)}`;
}

export default reduxForm({
    form: 'CustomerEdit',
    validate,
    fields: ['firstName', 'lastName', 'email', 'phone']
})(FormCustomer);