import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class ListCustomer extends Component {
    render() {
        const {handleDelete} = this.props;
        const customer = this.props.customer;
        return (
            <tr key={customer._id}>
                <td>
                    <Link to={{ pathname: '/admin/customer/' + customer._id }}>{customer.firstName} {customer.lastName}</Link>
                </td>
                <td>
                    {customer.email}
                </td>
                <td>
                    {customer.phone}
                </td>
                <td className="text-right">
                    <div className="btn-group-xs" role="group">
                        <Link to={{ pathname: '/admin/customer/' + customer._id + '/edit' }} className="btn btn-default"><span
                            className="glyphicon glyphicon-edit"></span> Edit</Link>
                        <button className="btn btn-danger" onClick={handleDelete}>
                            <span className="glyphicon glyphicon-trash"></span> Remove
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}