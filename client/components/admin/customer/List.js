import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchCustomers, deleteCustomer} from '../../../actions/customer';
import ListCustomer from './ListCustomer';
import ErrorAlert from '../../errors/ErrorAlert';

class List extends Component {

    componentDidMount() {
        this.props.fetchCustomers();
    };

    handleDelete(id) {
        this.props.deleteCustomer(id);
    }

    renderCustomers() {
        return this.props.customer.all.data.map((customer) => {
            return (
                <ListCustomer key={customer._id} customer={customer}
                              handleDelete={this.handleDelete.bind(this, customer._id)}/>
            );
        });
    }

    render() {
        const {customer} = this.props;
        if (customer.all.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (customer.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={customer.all.isError.error}/>
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                <div className="panel panel-default">
                    <div className="panel-heading">Customers list</div>
                    <div className="panel-body">
                        {customer.all.data.length > 0 ? (
                                <div className="table-responsive">
                                    <table className="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>E-mail</th>
                                            <th>Phone</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {this.renderCustomers()}
                                        </tbody>
                                    </table>
                                </div>
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>
                            )}
                        <div className="text-center">
                            <Link className="btn btn-default" to="/admin/customer/create">Add customer</Link>
                        </div>
                        <div className="text-center"></div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {customer: state.customer};
}

export default connect(mapStateToProps, {fetchCustomers, deleteCustomer})(List);