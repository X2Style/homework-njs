import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchCustomerBookings} from '../../../actions/customer';
import {Link} from 'react-router-dom';
import moment from 'moment';

class List extends Component {

    componentWillMount() {
        const {bookingId} = this.props;
        this.props.fetchCustomerBookings(bookingId);
    };

    renderBookings() {
        return this.props.customer.bookings.data.map((booking) => {
            return (
                <tr key={booking._id}>
                    <td>
                        <Link to={{ pathname: '/admin/cleaner/' + booking.cleaner._id }}>{booking.cleaner.firstName} {booking.cleaner.lastName}</Link>
                    </td>
                    <td>
                        {booking.cleaner.email}
                    </td>
                    <td>
                        <Link to={{ pathname: '/admin/city/' + booking.city._id }}>{booking.city.name}</Link>
                    </td>
                    <td>
                        {moment(booking.dateFrom).format('DD-MM-YYYY HH:mm')}
                    </td>
                    <td>
                        {moment(booking.dateTo).format('DD-MM-YYYY HH:mm')}
                    </td>
                </tr>
            );
        });
    }

    render() {
        const {customer} = this.props;
        if (customer.bookings.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (customer.bookings.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-danger">Error: {customer.bookings.isError.error}</div>
                </div>
            );
        }
        return (
            <div>
                {customer.bookings.data.length > 0 ? (
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th colSpan="2" className="text-center">Cleaner</th>
                                <th className="text-center">City</th>
                                <th colSpan="2" className="text-center">Date</th>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th></th>
                                <th>From</th>
                                <th>To</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.renderBookings()}
                            </tbody>
                        </table>
                    ) : (
                        <div className="well text-center">
                            <span>No bookings found</span>
                        </div>
                    )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {customer: state.customer};
}

export default connect(mapStateToProps, {fetchCustomerBookings})(List);