import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {fetchCustomer, updateCustomer, resetCustomer, initForm} from '../../../actions/customer';
import FormCustomer from './FormCustomer';
import ErrorAlert from '../../errors/ErrorAlert';

class Edit extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.resetCustomer();
        const {id} = this.props.match.params;
        this.props.fetchCustomer(id);
    }

    onSubmit(values) {
        const {id} = this.props.match.params;
        this.props.updateCustomer(id, values, this.context.router.history);
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    }

    render() {
        const {customer, initForm} = this.props;
        if (customer.current.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (customer.current.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={customer.current.isError.error} />
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                {customer.all.isError &&
                <ErrorAlert errorMessage={customer.all.isError.error} />
                }
                <div className="panel panel-default">
                    <div className="panel-heading">Edit customer</div>
                    <div className="panel-body">
                        {customer.current.data ? (
                                <FormCustomer initialValues={customer.current.data}
                                              initForm={initForm}
                                              onSubmit={this.onSubmitHandler.bind(this)}/>
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>
                            )}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {customer: state.customer};
}

export default withRouter(
    connect(
        mapStateToProps,
        {
            fetchCustomer,
            updateCustomer,
            resetCustomer,
            initForm
        })(Edit));