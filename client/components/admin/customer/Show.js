import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchCustomer, resetCustomer, deleteCustomer} from '../../../actions/customer';
import PropTypes from 'prop-types';
import ListBookings from './ListBookings';
import ErrorAlert from '../../errors/ErrorAlert';

class Show extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.resetCustomer();
        const {id} = this.props.match.params;
        this.props.fetchCustomer(id);
    };

    handleDelete(id) {
        this.props.deleteCustomer(id, this.context.router.history);
    }

    render() {
        const {customer} = this.props;
        const {id} = this.props.match.params;
        if (customer.current.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (customer.current.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={customer.current.isError.error} />
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                <div className="panel panel-default">
                    <div className="panel-heading">Customer information</div>
                    <div className="panel-body">
                        {customer.current.data ? (
                                <div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>First name:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {customer.current.data.firstName}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Last name:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {customer.current.data.lastName}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>E-mail:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {customer.current.data.email}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Phone:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {customer.current.data.phone}
                                        </div>
                                    </div>
                                    <div className="form-group text-center">
                                        <div className="btn-group" role="group">
                                            <Link
                                                to={{pathname: '/admin/customer/' + customer.current.data._id + '/edit'}}
                                                className="btn btn-default"><span
                                                className="glyphicon glyphicon-edit"></span> Edit</Link>
                                            <button type="submit" className="btn btn-danger"
                                                    onClick={this.handleDelete.bind(this, customer.current.data._id)}>
                                                <span className="glyphicon glyphicon-trash"></span> Remove
                                            </button>
                                        </div>
                                    </div>
                                    <p><strong>Bookings:</strong></p>
                                    <ListBookings bookingId={id}/>
                                </div>
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>
                            )}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {customer: state.customer};
}

export default connect(mapStateToProps, {fetchCustomer, resetCustomer, deleteCustomer})(Show);