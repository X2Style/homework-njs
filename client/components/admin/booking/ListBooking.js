import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';

export default class ListBooking extends Component {
    render() {
        const {handleDelete} = this.props;
        const booking = this.props.booking;
        return (
            <tr key={booking._id}>
                <td>
                    <Link to={{ pathname: '/admin/customer/' + booking.customer._id }}>{booking.customer.firstName} {booking.customer.lastName}</Link>
                </td>
                <td>
                    {booking.customer.email}
                </td>
                <td>
                    {booking.customer.phone}
                </td>
                <td>
                    <Link to={{ pathname: '/admin/cleaner/' + booking.cleaner._id }}>{booking.cleaner.firstName} {booking.cleaner.lastName}</Link>
                </td>
                <td>
                    {booking.cleaner.email}
                </td>
                <td>
                    <Link to={{ pathname: '/admin/city/' + booking.city._id }}>{booking.city.name}</Link>
                </td>
                <td>
                    {moment(booking.dateFrom).format('DD-MM-YYYY HH:mm')}
                </td>
                <td>
                    {moment(booking.dateTo).format('DD-MM-YYYY HH:mm')}
                </td>
                <td>
                    { +(Math.round(booking.price + "e+2")  + "e-2")}
                </td>
                <td className="text-right">
                    <div className="btn-group-xs" role="group">
                        <Link to={{ pathname: '/admin/booking/' + booking._id + '/edit' }} className="btn btn-default"><span
                            className="glyphicon glyphicon-edit"></span> Edit</Link>
                        <button className="btn btn-danger" onClick={handleDelete}>
                            <span className="glyphicon glyphicon-trash"></span> Remove
                        </button>
                    </div>
                </td>
            </tr>
        );
    }
}