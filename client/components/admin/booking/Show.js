import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchBooking, resetBooking, deleteBooking} from '../../../actions/booking';
import PropTypes from 'prop-types';
import moment from 'moment';
import ErrorAlert from '../../errors/ErrorAlert';

class Show extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.resetBooking();
        const {id} = this.props.match.params;
        this.props.fetchBooking(id);
    };

    handleDelete(id) {
        this.props.deleteBooking(id, this.context.router.history);
    }

    render() {
        const {booking} = this.props;
        if (booking.current.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (booking.current.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={booking.current.isError.error} />
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                <div className="panel panel-default">
                    <div className="panel-heading">Booking information</div>
                    <div className="panel-body">
                        {booking.current.data ? (
                                <div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Customer name:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            <Link
                                                to={{pathname: '/admin/customer/' + booking.current.data.customer._id}}>
                                                {booking.current.data.customer.firstName} {booking.current.data.customer.lastName}
                                            </Link>
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Customer email:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {booking.current.data.customer.email}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Customer phone:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {booking.current.data.customer.phone}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Cleaner name:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            <Link
                                                to={{pathname: '/admin/cleaner/' + booking.current.data.customer._id}}>
                                                {booking.current.data.cleaner.firstName} {booking.current.data.cleaner.lastName}
                                            </Link>
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Cleaner email:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {booking.current.data.cleaner.email}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>City:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            <Link
                                                to={{pathname: '/admin/city/' + booking.current.data.city._id}}>
                                                {booking.current.data.city.name}
                                            </Link>
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Date from:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {moment(booking.current.data.dateFrom).format('DD-MM-YYYY HH:mm')}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Date to:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {moment(booking.current.data.dateTo).format('DD-MM-YYYY HH:mm')}
                                        </div>
                                    </div>
                                    <div className="form-group text-center">
                                        <div className="btn-group" role="group">
                                            <Link
                                                to={{pathname: '/admin/booking/' + booking.current.data._id + '/edit'}}
                                                className="btn btn-default"><span
                                                className="glyphicon glyphicon-edit"></span> Edit</Link>
                                            <button type="submit" className="btn btn-danger"
                                                    onClick={this.handleDelete.bind(this, booking.current.data._id)}>
                                                <span className="glyphicon glyphicon-trash"></span> Remove
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>

                            )}
                    </div>
                </div>
            </div>
        );
    }
}
;

function mapStateToProps(state) {
    return {booking: state.booking};
}

export default connect(mapStateToProps, {fetchBooking, resetBooking, deleteBooking})(Show);