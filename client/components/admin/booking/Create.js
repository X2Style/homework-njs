import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import {createBooking} from '../../../actions/booking';
import {fetchCities} from '../../../actions/city';
import FormBooking from './FormBooking';
import ErrorAlert from '../../errors/ErrorAlert';
import moment from 'moment';

class Create extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.fetchCities();
    }

    onSubmit(values) {
        this.props.createBooking(values, this.context.router.history);
        //this.context.router.history.push('/admin/booking');
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    };

    minDate() {
        const min = moment().seconds(0).milliseconds(0);
        if (min.minutes() > 30) {
            min.minutes(0).add('2', 'h');
        } else {
            min.minutes(0).add('1', 'h');
        }
        return min.toDate();
    }

    render() {
        const {city, booking, initForm} = this.props;
        if (city.all.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (city.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={city.all.isError.error} />
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                {booking.current.isError &&
                    <ErrorAlert errorMessage={booking.current.isError.error} />
                }
                <div className="panel panel-default">
                    <div className="panel-heading">Create booking</div>
                    <div className="panel-body">
                        {city.all.data ? (
                                <FormBooking
                                    onSubmit={this.onSubmitHandler.bind(this)}
                                    cities={city.all.data}
                                    initForm={initForm}
                                    minDate={this.minDate()}
                                    initialValues={{
                                        "dateFrom": this.minDate()
                                    }}
                                />
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>
                            )}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {booking: state.booking, city: state.city};
}

export default withRouter(connect(mapStateToProps, {createBooking, fetchCities})(Create));