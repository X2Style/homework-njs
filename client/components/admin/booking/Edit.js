import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {fetchBooking, updateBooking, resetBooking, initForm} from '../../../actions/booking';
import {fetchCities} from '../../../actions/city';
import {fetchCleaners} from '../../../actions/cleaner';
import {fetchCustomers} from '../../../actions/customer';
import FormBookingEdit from './FormBookingEdit';
import ErrorAlert from '../../errors/ErrorAlert';
import moment from 'moment';

class Edit extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.resetBooking();
        // console.log(this.props);
        const {id} = this.props.match.params;
        this.props.fetchBooking(id);
        this.props.fetchCities();
        this.props.fetchCustomers();
        this.props.fetchCleaners();
    }

    onSubmit(values) {
        const {id} = this.props.match.params;
        this.props.updateBooking(id, values, this.context.router.history);
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    }

    render() {
        const {booking, city, customer, cleaner, initForm} = this.props;
        if (booking.current.isLoading || city.all.isLoading || customer.all.isLoading || cleaner.all.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (booking.current.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={booking.current.isError.error}/>
                </div>
            );
        } else if (city.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={city.all.isError.error}/>
                </div>
            );
        } else if (customer.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={customer.all.isError.error}/>
                </div>
            );
        } else if (cleaner.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={cleaner.all.isError.error}/>
                </div>
            );
        }
        if (booking.current.data && city.all.data && customer.all.data && cleaner.all.data) {
            var initialValues = {
                dateFrom: booking.current.data.dateFrom,
                duration: moment(booking.current.data.dateTo).diff(moment(booking.current.data.dateFrom), 'hours'),
                city: booking.current.data.city._id,
                cleaner: booking.current.data.cleaner._id,
                customer: booking.current.data.customer._id,
            }
        }
        ;

        return (
            <div className="col-md-10 col-md-offset-1">
                {booking.all.isError &&
                <ErrorAlert errorMessage={booking.all.isError.error}/>
                }
                <div className="panel panel-default">
                    <div className="panel-heading">Edit booking</div>
                    <div className="panel-body">
                        {booking.current.data ? (
                                <FormBookingEdit
                                    initialValues={initialValues}
                                    cities={city.all.data}
                                    cleaners={cleaner.all.data}
                                    customers={customer.all.data}
                                    initForm={initForm}
                                    onSubmit={this.onSubmitHandler.bind(this)}/>
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>
                            )}
                    </div>
                </div>
            </div>
        );
    }
}
;

function mapStateToProps(state) {
    return {booking: state.booking, city: state.city, cleaner: state.cleaner, customer: state.customer};
}

export default withRouter(
    connect(
        mapStateToProps,
        {
            fetchBooking,
            updateBooking,
            resetBooking,
            fetchCities,
            fetchCustomers,
            fetchCleaners,
            initForm
        })(Edit));