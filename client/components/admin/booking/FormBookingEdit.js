import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {renderField} from '../../partials/renderField';
import renderDatetimePicker from '../../partials/renderDatetimePicker';
import renderFieldSelect from '../../partials/renderFieldSelect';
import validator from 'validator';
import moment from 'moment';

class FormBooking extends Component {
    componentDidMount() {
        if (this.props.initForm) {
            this.props.initForm(this.props.initialValues);
        }
    }

    render() {
        const {handleSubmit, cities, customers, cleaners, minDate} = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <div className="row">
                    <div className="col-md-6">
                        <Field
                            name="dateFrom"
                            label="Date & time"
                            step={60}
                            component={renderDatetimePicker}
                            min={minDate}
                        />
                        <Field
                            name="duration"
                            label="Duration"
                            component={renderFieldSelect}
                            disabledOption={true}
                            options={[
                                {id: 1, label: '1 Hour'},
                                {id: 2, label: '2 Hours'},
                                {id: 3, label: '3 Hours'},
                                {id: 4, label: '4 Hours'},
                                {id: 5, label: '5 Hours'},
                                {id: 6, label: '6 Hours'},
                                {id: 7, label: '7 Hours'},
                                {id: 8, label: '8 Hours'},
                            ]}
                        />
                        <Field
                            name="city"
                            label="City"
                            component={renderFieldSelect}
                            disabledOption={true}
                            options={
                                cities.map((city) => {
                                    return (
                                        {id: city._id, label: city.name}
                                    );
                                })
                            }
                        />
                    </div>
                    <div className="col-md-6">
                        <Field
                            name="cleaner"
                            label="Cleaner"
                            component={renderFieldSelect}
                            disabledOption={true}
                            options={
                                cleaners.map((cleaner) => {
                                    return (
                                        {id: cleaner._id, label: cleaner.firstName + " " + cleaner.lastName}
                                    );
                                })
                            }
                        />
                        <Field
                            name="customer"
                            label="Customer"
                            component={renderFieldSelect}
                            disabledOption={true}
                            options={
                                customers.map((customer) => {
                                    return (
                                        {id: customer._id, label: customer.firstName + " " + customer.lastName}
                                    );
                                })
                            }
                        />
                    </div>
                </div>
                <div className="form-group text-center">
                    <button type="submit" className="btn btn-default">Save</button>
                </div>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};
    if (!values.dateFrom) {
        errors.dateFrom = 'Required';
    }

    if (!values.duration) {
        errors.duration = 'Required';
    } else if (!validator.isInt(String(values.duration).trim())) {
        errors.duration = 'Must be a number';
    } else if (values.duration < 1 || values.duration > 8) {
        errors.duration = 'Must be from 1 to 8 Hours';
    }

    if (!values.city) {
        errors.city = 'Required';
    } else if (!validator.isMongoId(values.city.trim())) {
        errors.city = 'Invalid city';
    }

    if (!values.cleaner) {
        errors.cleaner = 'Required';
    } else if (!validator.isMongoId(values.cleaner.trim())) {
        errors.cleaner = 'Invalid cleaner';
    }

    if (!values.customer) {
        errors.customer = 'Required';
    } else if (!validator.isMongoId(values.customer.trim())) {
        errors.customer = 'Invalid customer';
    }

    return errors;
}

function normalizePhone(value) {
    if (!value) {
        return value
    }

    const onlyNums = value.replace(/[^\d]/g, '')
    if (onlyNums.length <= 3) {
        return `(${onlyNums}`;
    }
    if (onlyNums.length <= 6) {
        return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3)}`;
    }
    if (onlyNums.length <= 8) {
        return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(6)}`;
    }
    return `(${onlyNums.slice(0, 3)}) ${onlyNums.slice(3, 6)}-${onlyNums.slice(6, 8)}-${onlyNums.slice(8, 10)}`;
}

export default reduxForm({
    form: 'BookingEdit',
    validate,
    fields: ['dateFrom', 'dateTo', 'customer', 'cleaner', 'city']
})(FormBooking);