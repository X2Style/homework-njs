import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class ListCleaner extends Component {
    render() {
        const {handleDelete, cleaner, controls} = this.props;
        return (
            <tr key={cleaner._id}>
                <td>
                    <Link to={{ pathname: '/admin/cleaner/' + cleaner._id }}>{cleaner.firstName} {cleaner.lastName}</Link>
                </td>
                <td>
                    {cleaner.email}
                </td>
                <td>
                    {cleaner.qualityScore}
                </td>
                {controls &&
                <td className="text-right">
                    <div className="btn-group-xs" role="group">
                        <Link to={{ pathname: '/admin/cleaner/' + cleaner._id + '/edit' }} className="btn btn-default"><span
                            className="glyphicon glyphicon-edit"></span> Edit</Link>
                        <button className="btn btn-danger" onClick={handleDelete}>
                            <span className="glyphicon glyphicon-trash"></span> Remove
                        </button>
                    </div>
                </td>
                }
            </tr>
        );
    }
}