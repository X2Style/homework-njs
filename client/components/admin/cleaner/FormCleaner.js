import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {renderField} from '../../partials/renderField';
import MultiCheckboxField from '../../partials/MultiCheckboxField';
import {regexUnicodeHelper} from '../../../helper';
import validator from 'validator';

class FormCleaner extends Component {
    componentDidMount() {
        if (this.props.initForm) {
            this.props.initForm(this.props.initialValues);
        }
    }

    render() {
        const {handleSubmit, cities} = this.props;
        const citiesOptions = cities;
        return (
            <form onSubmit={handleSubmit}>
                <Field
                    name="firstName"
                    type="text"
                    component={renderField}
                    label="First Name"
                    className="form-control"
                    placeholder="First Name"/>
                <Field
                    name="lastName"
                    type="text"
                    component={renderField}
                    label="Last Name"
                    className="form-control"
                    placeholder="Last Name"/>
                <Field
                    name="email"
                    type="email"
                    component={renderField}
                    label="E-mail"
                    className="form-control"
                    placeholder="E-mail"/>
                <Field
                    name="qualityScore"
                    type="text"
                    component={renderField}
                    label="Quality Score"
                    className="form-control"
                    placeholder="Quality Score"
                    min="0"
                    max="5"
                    step="0.1"/>
                <hr/>
                <strong>Cities</strong>
                <Field name="cities" component={props =>
                    <MultiCheckboxField
                        options={citiesOptions}
                        field={props.input}
                    />
                }
                />
                <div className="form-group text-center">
                    <button type="submit" className="btn btn-default">Save</button>
                </div>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};
    const nameValidationRegex = new RegExp('^[' + regexUnicodeHelper() + "]+(?:[-\\s'][" + regexUnicodeHelper() + "]+)*([']?)$");

    if (!values.firstName) {
        errors.firstName = 'Required'
    } else if (values.firstName.trim().length < 2) {
        errors.firstName = 'Must be at least 2 characters'
    } else if (values.firstName.trim().length > 100) {
        errors.firstName = 'Must be not more than 100 characters';
    } else if (!nameValidationRegex.test(values.firstName.trim())) {
        errors.firstName = 'First name is invalid';
    }

    if (!values.lastName) {
        errors.lastName = 'Required'
    } else if (values.lastName.trim().length < 2) {
        errors.lastName = 'Must be at least 2 characters'
    } else if (values.lastName.trim().length > 100) {
        errors.lastName = 'Must be not more than 100 characters';
    } else if (!nameValidationRegex.test(values.lastName.trim())) {
        errors.lastName = 'Last name is invalid';
    }

    if (!values.email) {
        errors.email = 'Required';
    } else if (!validator.isEmail(values.email.trim())) {
        errors.email = 'Enter a valid e-mail address';
    }

    if (!values.qualityScore) {
        errors.qualityScore = 'Required';
    } else if (!validator.isFloat(String(values.qualityScore).trim())) {
        errors.qualityScore = 'Must be a number';
    } else if (values.qualityScore < 0 || values.qualityScore > 5) {
        errors.qualityScore = 'Enter a value between 0 and 5';
    }
    return errors;
}

export default reduxForm({
    form: 'CleanerEdit',
    validate,
    fields: ['firstName', 'lastName', 'email', 'qualityScore', 'cities']
})(FormCleaner);