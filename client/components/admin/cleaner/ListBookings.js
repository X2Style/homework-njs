import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchCleanerBookings} from '../../../actions/cleaner';
import {Link} from 'react-router-dom';
import moment from 'moment';

class List extends Component {

    componentWillMount() {
        const {bookingId} = this.props;
        this.props.fetchCleanerBookings(bookingId);
    };

    renderBookings() {
        return this.props.cleaner.bookings.data.map((booking) => {
            return (
                <tr key={booking._id}>
                    <td>
                        <Link to={{ pathname: '/admin/customer/' + booking.customer._id }}>{booking.customer.firstName} {booking.customer.lastName}</Link>
                    </td>
                    <td>
                        {booking.customer.email}
                    </td>
                    <td>
                        {booking.customer.phone}
                    </td>
                    <td>
                        <Link to={{ pathname: '/admin/city/' + booking.city._id }}>{booking.city.name}</Link>
                    </td>
                    <td>
                        {moment(booking.dateFrom).format('DD-MM-YYYY HH:mm')}
                    </td>
                    <td>
                        {moment(booking.dateTo).format('DD-MM-YYYY HH:mm')}
                    </td>
                </tr>
            );
        });
    }

    render() {
        const {cleaner} = this.props;
        if (cleaner.bookings.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (cleaner.bookings.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-danger">Error: {cleaner.bookings.isError.error}</div>
                </div>
            );
        }
        return (
            <div>
                {cleaner.bookings.data.length > 0 ? (
                        <table className="table table-bordered">
                            <thead>
                            <tr>
                                <th colSpan="3" className="text-center">Customer</th>
                                <th className="text-center">City</th>
                                <th colSpan="2" className="text-center">Date</th>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Phone</th>
                                <th></th>
                                <th>From</th>
                                <th>To</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.renderBookings()}
                            </tbody>
                        </table>
                    ) : (
                        <div className="well text-center">
                            <span>No bookings found</span>
                        </div>
                    )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {cleaner: state.cleaner};
}

export default connect(mapStateToProps, {fetchCleanerBookings})(List);