import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchCleaner, resetCleaner, deleteCleaner} from '../../../actions/cleaner';
import PropTypes from 'prop-types';
import ListBookings from './ListBookings';
import ErrorAlert from '../../errors/ErrorAlert';

class Show extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.resetCleaner();
        const {id} = this.props.match.params;
        this.props.fetchCleaner(id);
    };

    handleDelete(id) {
        this.props.deleteCleaner(id, this.context.router.history);
    }

    render() {
        const {cleaner} = this.props;
        const {id} = this.props.match.params;
        if (cleaner.current.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (cleaner.current.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={cleaner.current.isError.error} />
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                <div className="panel panel-default">
                    <div className="panel-heading">Cleaner information</div>
                    <div className="panel-body">
                        {cleaner.current.data ? (
                                <div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>First name:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {cleaner.current.data.firstName}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Last name:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {cleaner.current.data.lastName}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>E-mail:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {cleaner.current.data.email}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Quality score:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            {cleaner.current.data.qualityScore}
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-xs-6 text-right">
                                            <strong>Cities:</strong>
                                        </div>
                                        <div className="col-xs-6">
                                            <ul>
                                                {cleaner.current.data.cities.map(city => {
                                                    return (
                                                        <li key={city._id}>{city.name}</li>
                                                    );
                                                })}
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="form-group text-center">
                                        <div className="btn-group" role="group">
                                            <Link
                                                to={{pathname: '/admin/cleaner/' + cleaner.current.data._id + '/edit'}}
                                                className="btn btn-default"><span
                                                className="glyphicon glyphicon-edit"></span> Edit</Link>
                                            <button type="submit" className="btn btn-danger"
                                                    onClick={this.handleDelete.bind(this, cleaner.current.data._id)}>
                                                <span className="glyphicon glyphicon-trash"></span> Remove
                                            </button>
                                        </div>
                                    </div>
                                    <p><strong>Bookings:</strong></p>
                                    <ListBookings bookingId={id}/>
                                </div>
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>
                            )}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {cleaner: state.cleaner};
}

export default connect(mapStateToProps, {fetchCleaner, resetCleaner, deleteCleaner})(Show);