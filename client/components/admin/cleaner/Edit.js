import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {fetchCleaner, updateCleaner, resetCleaner, initForm} from '../../../actions/cleaner';
import {fetchCities} from '../../../actions/city';
import FormCleaner from './FormCleaner';
import ErrorAlert from '../../errors/ErrorAlert';

class Edit extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.resetCleaner();
        // console.log(this.props);
        const {id} = this.props.match.params;
        this.props.fetchCleaner(id);
        this.props.fetchCities();
    }

    onSubmit(values) {
        const {id} = this.props.match.params;
        this.props.updateCleaner(id, values, this.context.router.history);
        //this.context.router.history.push('/admin/cleaner');
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    }

    render() {
        const {cleaner, city, initForm} = this.props;
        if (cleaner.current.isLoading || city.all.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (cleaner.current.isError || city.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={cleaner.current.isError.error} />
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                {cleaner.all.isError &&
                <ErrorAlert errorMessage={cleaner.all.isError.error} />
                }
                <div className="panel panel-default">
                    <div className="panel-heading">Edit cleaner</div>
                    <div className="panel-body">
                        {cleaner.current.data && city.all.data ? (
                                <FormCleaner
                                    initialValues={cleaner.current.data}
                                    cities={city.all.data}
                                    initForm={initForm}
                                    onSubmit={this.onSubmitHandler.bind(this)}/>
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>
                            )}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {cleaner: state.cleaner, city: state.city};
}

export default withRouter(
    connect(
        mapStateToProps,
        {
            fetchCleaner,
            updateCleaner,
            resetCleaner,
            fetchCities,
            initForm
        })(Edit));