import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchCleaners, deleteCleaner} from '../../../actions/cleaner';
import ListCleaner from './ListCleaner';
import ErrorAlert from '../../errors/ErrorAlert';

class List extends Component {

    componentDidMount() {
        this.props.fetchCleaners();
    };

    handleDelete(id) {
        this.props.deleteCleaner(id);
    }

    renderCleaners() {
        return this.props.cleaner.all.data.map((cleaner) => {
            return (
                <ListCleaner key={cleaner._id} cleaner={cleaner} controls={true}
                             handleDelete={this.handleDelete.bind(this, cleaner._id)}/>
            );
        });
    }

    render() {
        const {cleaner} = this.props;
        if (cleaner.all.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (cleaner.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={cleaner.all.isError.error}/>
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                <div className="panel panel-default">
                    <div className="panel-heading">Cleaners list</div>
                    <div className="panel-body">
                        {cleaner.all.data.length > 0 ? (
                                <div className="table-responsive">
                                    <table className="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>E-mail</th>
                                            <th>Quality Score</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {this.renderCleaners()}
                                        </tbody>
                                    </table>
                                </div>
                            ) : (
                                <div className="well text-center">
                                    No cleaners yet
                                </div>
                            )}
                        <div className="text-center">
                            <Link className="btn btn-default" to="/admin/cleaner/create">Add cleaner</Link>
                        </div>
                        <div className="text-center"></div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {cleaner: state.cleaner};
}

export default connect(mapStateToProps, {fetchCleaners, deleteCleaner})(List);