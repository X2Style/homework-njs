import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import {createCleaner} from '../../../actions/cleaner';
import {fetchCities} from '../../../actions/city';
import FormCleaner from './FormCleaner';
import ErrorAlert from '../../errors/ErrorAlert';

class Create extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidMount() {
        this.props.fetchCities();
    }

    onSubmit(values) {
        this.props.createCleaner(values, this.context.router.history);
    }

    onSubmitHandler(values) {
        this.onSubmit(values);
    };

    render() {
        const {city, cleaner, initForm} = this.props;
        if (city.all.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (city.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <ErrorAlert errorMessage={city.all.isError.error}/>
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                {cleaner.current.isError &&
                <ErrorAlert errorMessage={cleaner.current.isError.error}/>
                }
                <div className="panel panel-default">
                    <div className="panel-heading">Add cleaner</div>
                    <div className="panel-body">
                        {city.all.data ? (
                                <FormCleaner
                                    onSubmit={this.onSubmitHandler.bind(this)}
                                    cities={city.all.data}
                                    initForm={initForm}
                                />
                            ) : (
                                <div className="well text-center">
                                    Loading...
                                </div>
                            )}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {cleaner: state.cleaner, city: state.city};
}

export default withRouter(connect(mapStateToProps, {createCleaner, fetchCities})(Create));