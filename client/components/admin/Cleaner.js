import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import List from './cleaner/List';
import Show from './cleaner/Show';
import Create from './cleaner/Create';
import Edit from './cleaner/Edit';

export default class Cleaner extends Component {

    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/admin/cleaner" component={List}/>
                    <Route path="/admin/cleaner/:id/edit" component={Edit}/>
                    <Route path="/admin/cleaner/create" component={Create}/>
                    <Route path="/admin/cleaner/:id" component={Show}/>
                </Switch>
            </div>
        );
    }
};