import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchBookings, deleteBooking} from '../../actions/booking';
import ListBooking from './booking/ListBooking';

class Dashboard extends Component {

    componentDidMount() {
        this.props.fetchBookings();
    };

    handleDelete(id) {
        this.props.deleteBooking(id);
    }

    renderBookings() {
        return this.props.booking.all.data.map((booking) => {
            return (
                <ListBooking key={booking._id} booking={booking}
                             handleDelete={this.handleDelete.bind(this, booking._id)}/>
            );
        });
    }

    render() {
        const {booking} = this.props;
        if (booking.all.isLoading) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-info">Loading...</div>
                </div>
            );
        } else if (booking.all.isError) {
            return (
                <div className="col-md-10 col-md-offset-1 messages-wrapper text-center">
                    <div className="alert alert-danger">Error: {booking.all.isError.error}</div>
                </div>
            );
        }
        return (
            <div className="col-md-10 col-md-offset-1">
                <div className="panel panel-default">
                    <div className="panel-heading">Dashboard</div>
                    <div className="panel-body">
                        {booking.all.data.length > 0 ? (
                            <div className="table-responsive">
                                <table className="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th colSpan="3" className="text-center">Customer</th>
                                        <th colSpan="2" className="text-center">Cleaner</th>
                                        <th rowSpan="2" className="text-center">City</th>
                                        <th colSpan="2" className="text-center">Date</th>
                                        <th rowSpan="2" className="text-center">Price</th>
                                        <th rowSpan="2"></th>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <th>E-mail</th>
                                        <th>Phone</th>
                                        <th>Name</th>
                                        <th>E-mail</th>
                                        <th>From</th>
                                        <th>To</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.renderBookings()}
                                    </tbody>
                                </table>
                            </div>
                            ) : (
                                <div className="well text-center">
                                    No bookings yet
                                </div>
                            )}
                        <div className="text-center">
                            <Link className="btn btn-default" to="/admin/booking/create">Add booking</Link>
                        </div>
                        <div className="text-center"></div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {booking: state.booking};
}

export default connect(mapStateToProps, {fetchBookings, deleteBooking})(Dashboard);