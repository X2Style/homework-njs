import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';

import Header from './layouts/Header';
import Footer from './layouts/Footer';

import NotFound from './errors/NotFound';

import BookingHome from './booking/BookingHome';
import BookingSuccess from './booking/BookingSuccess';
import BookingFailure from './booking/BookingFailure';
import Admin from './admin/Admin';
import Login from './admin/auth/Login';
import Logout from './admin/auth/Logout';

export default class App extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="container-fluid">
                    <Switch>
                        <Route exact path="/" component={BookingHome}/>
                        <Route path="/success" component={BookingSuccess}/>
                        <Route path="/failure" component={BookingFailure}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/admin" component={Admin}/>
                        <Route path="/logout" component={Logout}/>
                        <Route component={NotFound}/>
                    </Switch>
                </div>
                <Footer />
            </div>
        );
    }
};