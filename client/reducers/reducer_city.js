import {
    FETCH_CITIES, FETCH_CITIES_SUCCESS, FETCH_CITIES_ERROR,
    FETCH_CITY, FETCH_CITY_SUCCESS, FETCH_CITY_ERROR,
    FETCH_CITY_CLEANERS, FETCH_CITY_CLEANERS_SUCCESS, FETCH_CITY_CLEANERS_ERROR,
    CREATE_CITY, CREATE_CITY_SUCCESS, CREATE_CITY_ERROR,
    UPDATE_CITY, UPDATE_CITY_SUCCESS, UPDATE_CITY_ERROR,
    DELETE_CITY, DELETE_CITY_SUCCESS, DELETE_CITY_ERROR,
    RESET_CITY, RESET_CITIES
} from '../actions/city';

const INITIAL_STATE = {
    all: {data: [], isLoading: null, isError: null},
    current: {data: null, isLoading: null, isError: null},
    cleaners: {data: [], isLoading: null, isError: null}
};

export default function (state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_CITIES:
            return {...state, all: {data: [], isLoading: true, isError: null}};
        case FETCH_CITIES_SUCCESS:
            // console.log(action);
            return {...state, all: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CITIES_ERROR:
            // console.log(action);
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: [], isLoading: false, isError: error}};

        case FETCH_CITY_CLEANERS:
            return {...state, cleaners: {data: [], isLoading: true, isError: null}};
        case FETCH_CITY_CLEANERS_SUCCESS:
            return {...state, cleaners: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CITY_CLEANERS_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, cleaners: {data: [], isLoading: false, isError: error}};

        case FETCH_CITY:
            return {...state, current: {data: [], isLoading: true, isError: null}};
        case FETCH_CITY_SUCCESS:
            // console.log(action);
            return {...state, current: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CITY_ERROR:
            // console.log(action);
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, current: {data: [], isLoading: false, isError: error}};

        case RESET_CITY:
            return {...state, current: {data: null, isLoading: null, isError: null}};

        case RESET_CITIES:
            return {...state, all: {data: [], isLoading: null, isError: null}};

        case CREATE_CITY:
            return {...state, current: {data: [], isLoading: true, isError: null}};
        case CREATE_CITY_SUCCESS:
            return {...state, current: {data: action.payload.data, isLoading: true, isError: null}};
        case CREATE_CITY_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, current: {data: [], isLoading: false, isError: error}};

        case UPDATE_CITY:
            return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        case UPDATE_CITY_SUCCESS:
            const data = state.all.data.map((item) => {
                if (item._id === action.payload.data._id) {
                    return action.payload.data;
                } else {
                    return item;
                }
            });
            return {...state, all: {data: data, isLoading: false, isError: null}};
        case UPDATE_CITY_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: state.all.data, isLoading: false, isError: error}};

        case DELETE_CITY:
            return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        case DELETE_CITY_SUCCESS:
            const allCities = state.all.data.filter((item) => {
                return item._id !== action.payload.data._id;
            });
            return {...state, current: {data: null}, all: {data: allCities}};
        case DELETE_CITY_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: [], isLoading: false, isError: error}};

        default:
            return state;
    }
}