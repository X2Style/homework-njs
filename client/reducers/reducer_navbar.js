import {TOGGLE_MENU} from '../actions/navbar';

const INITIAL_STATE = {
    navCollapsed: true
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case TOGGLE_MENU:
            return {...state, navCollapsed: !state.navCollapsed};

        default:
            return state;
    }
}