import {
    LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE,
    LOGOUT_REQUEST
} from '../actions/auth';

const INITIAL_STATE = {
    token: null, isLoading: null, isError: null
};

export default function (state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case LOGIN_REQUEST:
            return {...state, data: [], isLoading: true, isError: null};
        case LOGIN_SUCCESS:
            return {...state, data: action.payload.data, isLoading: false, isError: null};
        case LOGIN_FAILURE:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, data: [], isLoading: false, isError: error};
        case LOGOUT_REQUEST:
            return {...state, data: [], isLoading: true, isError: null};

        default:
            return state;
    }
}