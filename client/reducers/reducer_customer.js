//noinspection JSUnresolvedVariable
import {
    FETCH_CUSTOMERS, FETCH_CUSTOMERS_SUCCESS, FETCH_CUSTOMERS_ERROR,
    FETCH_CUSTOMER, FETCH_CUSTOMER_SUCCESS, FETCH_CUSTOMER_ERROR,
    FETCH_CUSTOMER_BOOKINGS, FETCH_CUSTOMER_BOOKINGS_SUCCESS, FETCH_CUSTOMER_BOOKINGS_ERROR,
    CREATE_CUSTOMER, CREATE_CUSTOMER_SUCCESS, CREATE_CUSTOMER_ERROR,
    UPDATE_CUSTOMER, UPDATE_CUSTOMER_SUCCESS, UPDATE_CUSTOMER_ERROR,
    DELETE_CUSTOMER, DELETE_CUSTOMER_SUCCESS, DELETE_CUSTOMER_ERROR,
    RESET_CUSTOMER, RESET_CUSTOMERS
} from '../actions/customer';

const INITIAL_STATE = {
    all: {data: [], isLoading: null, isError: null},
    current: {data: null, isLoading: null, isError: null},
    bookings: {data: [], isLoading: null, isError: null}

};

export default function (state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_CUSTOMERS:
            return {...state, all: {data: [], isLoading: true, isError: null}};
        case FETCH_CUSTOMERS_SUCCESS:
            // console.log(action);
            return {...state, all: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CUSTOMERS_ERROR:
            // console.log(action);
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: [], isLoading: false, isError: error}};

        case FETCH_CUSTOMER_BOOKINGS:
            return {...state, bookings: {data: [], isLoading: true, isError: null}};
        case FETCH_CUSTOMER_BOOKINGS_SUCCESS:
            return {...state, bookings: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CUSTOMER_BOOKINGS_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, bookings: {data: [], isLoading: false, isError: error}};

        case FETCH_CUSTOMER:
            return {...state, current: {data: [], isLoading: true, isError: null}};
        case FETCH_CUSTOMER_SUCCESS:
            // console.log(action);
            return {...state, current: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CUSTOMER_ERROR:
            // console.log(action);
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, current: {data: [], isLoading: false, isError: error}};

        case RESET_CUSTOMER:
            return {...state, current: {data: null, isLoading: null, isError: null}};

        case RESET_CUSTOMERS:
            return {...state, all: {data: [], isLoading: null, isError: null}};

        case CREATE_CUSTOMER:
            return {...state, current: {data: [], isLoading: true, isError: null}};
        case CREATE_CUSTOMER_SUCCESS:
            return {...state, current: {data: action.payload.data, isLoading: true, isError: null}};
        case CREATE_CUSTOMER_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, current: {data: [], isLoading: false, isError: error}};

        case UPDATE_CUSTOMER:
            return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        case UPDATE_CUSTOMER_SUCCESS:
            const data = state.all.data.map((item) => {
                if (item._id === action.payload.data._id) {
                    return action.payload.data;
                } else {
                    return item;
                }
            });
            return {...state, all: {data: data, isLoading: false, isError: null}};
        case UPDATE_CUSTOMER_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: state.all.data, isLoading: false, isError: error}};

        case DELETE_CUSTOMER:
            return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        case DELETE_CUSTOMER_SUCCESS:
            const allCustomers = state.all.data.filter((item) => {
                return item._id !== action.payload.data._id;
            });
            return {...state, current: {data: null}, all: {data: allCustomers}};
        case DELETE_CUSTOMER_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: [], isLoading: false, isError: error}};

        default:
            return state;
    }
}