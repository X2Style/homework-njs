import {
    CREATE_BOOKING_CLIENT, CREATE_BOOKING_CLIENT_SUCCESS, CREATE_BOOKING_CLIENT_ERROR,
    FETCH_CITIES_CLIENT, FETCH_CITIES_CLIENT_SUCCESS, FETCH_CITIES_CLIENT_ERROR,
    RESET_BOOKING_CLIENT, RESET_BOOKINGS_CLIENT, RESET_BOOKING_INPUT_CLIENT,
    SET_BOOKING_INPUT_CLIENT,
    RECALCULATE_PRICE
} from '../actions/booking-client';

const INITIAL_STATE = {
    cities: {data: [], isLoading: null, isError: null},
    booking: {data: null, isLoading: null, isError: null},
    input: null,
    price: null
};

export default function (state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        // case FETCH_BOOKINGS:
        //     return {...state, all: {data: [], isLoading: true, isError: null}};
        // case FETCH_BOOKINGS_SUCCESS:
        //     // console.log(action);
        //     return {...state, all: {data: action.payload.data, isLoading: false, isError: null}};
        // case FETCH_BOOKINGS_ERROR:
        //     // console.log(action);
        //     error = action.payload.response.data || {error: action.payload.message};
        //     return {...state, all: {data: [], isLoading: false, isError: error}};

        // case FETCH_BOOKING_BOOKINGS:
        //     return {...state, cleaners: {data: [], isLoading: true, isError: null}};
        // case FETCH_BOOKING_BOOKINGS_SUCCESS:
        //     return {...state, cleaners: {data: action.payload.data, isLoading: false, isError: null}};
        // case FETCH_BOOKING_BOOKINGS_ERROR:
        //     error = action.payload.response.data || {error: action.payload.message};
        //     return {...state, cleaners: {data: [], isLoading: false, isError: error}};
        //
        // case FETCH_BOOKING:
        //     return {...state, current: {data: [], isLoading: true, isError: null}};
        // case FETCH_BOOKING_SUCCESS:
        //     // console.log(action);
        //     return {...state, current: {data: action.payload.data, isLoading: false, isError: null}};
        // case FETCH_BOOKING_ERROR:
        //     // console.log(action);
        //     error = action.payload.response.data || {error: action.payload.message};
        //     return {...state, current: {data: [], isLoading: false, isError: error}};
        case FETCH_CITIES_CLIENT:
            return {...state, cities: {data: [], isLoading: true, isError: null}};
        case FETCH_CITIES_CLIENT_SUCCESS:
            // console.log(action);
            return {...state, cities: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CITIES_CLIENT_ERROR:
            // console.log(action);
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, cities: {data: [], isLoading: false, isError: error}};

        case RESET_BOOKING_CLIENT:
            return {...state, booking: {data: null, isLoading: null, isError: null}};

        case RESET_BOOKINGS_CLIENT:
            return {...state, all: {data: [], isLoading: null, isError: null}};

        case CREATE_BOOKING_CLIENT:
            return {...state, booking: {data: [], isLoading: true, isError: null}};
        case CREATE_BOOKING_CLIENT_SUCCESS:
            return {...state, booking: {data: action.payload.data, isLoading: true, isError: null}};
        case CREATE_BOOKING_CLIENT_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, booking: {data: [], isLoading: false, isError: error}};

        case SET_BOOKING_INPUT_CLIENT:
            return {...state, input: action.payload};

        case RESET_BOOKING_INPUT_CLIENT:
            return {...state, input: null};

        case RECALCULATE_PRICE:
            return {...state, price: action.payload.data};

        // case UPDATE_BOOKING:
        //     return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        // case UPDATE_BOOKING_SUCCESS:
        //     const data = state.all.data.map((item) => {
        //         if (item._id === action.payload.data._id) {
        //             return action.payload.data;
        //         } else {
        //             return item;
        //         }
        //     });
        //     return {...state, all: {data: data, isLoading: false, isError: null}};
        // case UPDATE_BOOKING_ERROR:
        //     error = action.payload.response.data || {error: action.payload.message};
        //     return {...state, all: {data: state.all.data, isLoading: false, isError: error}};
        //
        // case DELETE_BOOKING:
        //     return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        // case DELETE_BOOKING_SUCCESS:
        //     const allBookings = state.all.data.filter((item) => {
        //         return item._id !== action.payload.data._id;
        //     });
        //     return {...state, current: {data: null}, all: {data: allBookings}};
        // case DELETE_BOOKING_ERROR:
        //     error = action.payload.response.data || {error: action.payload.message};
        //     return {...state, all: {data: [], isLoading: false, isError: error}};

        default:
            return state;
    }
}