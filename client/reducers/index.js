import {combineReducers} from 'redux';
import { reducer as formReducer } from 'redux-form';
import navbarReducer from './reducer_navbar';
import authReducer from './reducer_auth';
import bookingReducer from './reducer_booking';
import cityReducer from './reducer_city';
import cleanerReducer from './reducer_cleaner';
import customerReducer from './reducer_customer';
import clientReducer from './reducer_booking_client';

const rootReducer = combineReducers({
    //state: (state = {}) => state
    form: formReducer,
    navbar: navbarReducer,
    auth: authReducer,
    booking: bookingReducer,
    city: cityReducer,
    cleaner: cleanerReducer,
    customer: customerReducer,
    client: clientReducer,
});

export default rootReducer;