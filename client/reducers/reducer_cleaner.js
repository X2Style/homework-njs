//noinspection JSUnresolvedVariable
import {
    FETCH_CLEANERS, FETCH_CLEANERS_SUCCESS, FETCH_CLEANERS_ERROR,
    FETCH_CLEANER, FETCH_CLEANER_SUCCESS, FETCH_CLEANER_ERROR,
    FETCH_CLEANER_BOOKINGS, FETCH_CLEANER_BOOKINGS_SUCCESS, FETCH_CLEANER_BOOKINGS_ERROR,
    CREATE_CLEANER, CREATE_CLEANER_SUCCESS, CREATE_CLEANER_ERROR,
    UPDATE_CLEANER, UPDATE_CLEANER_SUCCESS, UPDATE_CLEANER_ERROR,
    DELETE_CLEANER, DELETE_CLEANER_SUCCESS, DELETE_CLEANER_ERROR,
    RESET_CLEANER, RESET_CLEANERS
} from '../actions/cleaner';

const INITIAL_STATE = {
    all: {data: [], isLoading: null, isError: null},
    current: {data: null, isLoading: null, isError: null},
    bookings: {data: [], isLoading: null, isError: null}
};

export default function (state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_CLEANERS:
            return {...state, all: {data: [], isLoading: true, isError: null}};
        case FETCH_CLEANERS_SUCCESS:
            // console.log(action);
            return {...state, all: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CLEANERS_ERROR:
            // console.log(action);
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: [], isLoading: false, isError: error}};

        case FETCH_CLEANER_BOOKINGS:
            return {...state, bookings: {data: [], isLoading: true, isError: null}};
        case FETCH_CLEANER_BOOKINGS_SUCCESS:
            return {...state, bookings: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CLEANER_BOOKINGS_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, bookings: {data: [], isLoading: false, isError: error}};

        case FETCH_CLEANER:
            return {...state, current: {data: [], isLoading: true, isError: null}};
        case FETCH_CLEANER_SUCCESS:
            // console.log(action);
            return {...state, current: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_CLEANER_ERROR:
            // console.log(action);
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, current: {data: [], isLoading: false, isError: error}};

        case RESET_CLEANER:
            return {...state, current: {data: null, isLoading: null, isError: null}};

        case RESET_CLEANERS:
            return {...state, all: {data: [], isLoading: null, isError: null}};

        case CREATE_CLEANER:
            return {...state, current: {data: [], isLoading: true, isError: null}};
        case CREATE_CLEANER_SUCCESS:
            return {...state, current: {data: action.payload.data, isLoading: true, isError: null}};
        case CREATE_CLEANER_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, current: {data: [], isLoading: false, isError: error}};

        case UPDATE_CLEANER:
            return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        case UPDATE_CLEANER_SUCCESS:
            const data = state.all.data.map((item) => {
                if (item._id === action.payload.data._id) {
                    return action.payload.data;
                } else {
                    return item;
                }
            });
            return {...state, all: {data: data, isLoading: false, isError: null}};
        case UPDATE_CLEANER_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: state.all.data, isLoading: false, isError: error}};

        case DELETE_CLEANER:
            return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        case DELETE_CLEANER_SUCCESS:
            const allCleaners = state.all.data.filter((item) => {
                return item._id !== action.payload.data._id;
            });
            return {...state, current: {data: null}, all: {data: allCleaners}};
        case DELETE_CLEANER_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: [], isLoading: false, isError: error}};

        default:
            return state;
    }
}