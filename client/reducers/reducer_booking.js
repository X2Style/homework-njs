import {
    FETCH_BOOKINGS, FETCH_BOOKINGS_SUCCESS, FETCH_BOOKINGS_ERROR,
    FETCH_BOOKING, FETCH_BOOKING_SUCCESS, FETCH_BOOKING_ERROR,
    FETCH_BOOKING_BOOKINGS, FETCH_BOOKING_BOOKINGS_SUCCESS, FETCH_BOOKING_BOOKINGS_ERROR,
    CREATE_BOOKING, CREATE_BOOKING_SUCCESS, CREATE_BOOKING_ERROR,
    UPDATE_BOOKING, UPDATE_BOOKING_SUCCESS, UPDATE_BOOKING_ERROR,
    DELETE_BOOKING, DELETE_BOOKING_SUCCESS, DELETE_BOOKING_ERROR,
    RESET_BOOKING, RESET_BOOKINGS
} from '../actions/booking';

const INITIAL_STATE = {
    all: {data: [], isLoading: null, isError: null},
    current: {data: null, isLoading: null, isError: null}
};

export default function (state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_BOOKINGS:
            return {...state, all: {data: [], isLoading: true, isError: null}};
        case FETCH_BOOKINGS_SUCCESS:
            // console.log(action);
            return {...state, all: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_BOOKINGS_ERROR:
            // console.log(action);
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: [], isLoading: false, isError: error}};

        // case FETCH_BOOKING_BOOKINGS:
        //     return {...state, cleaners: {data: [], isLoading: true, isError: null}};
        // case FETCH_BOOKING_BOOKINGS_SUCCESS:
        //     return {...state, cleaners: {data: action.payload.data, isLoading: false, isError: null}};
        // case FETCH_BOOKING_BOOKINGS_ERROR:
        //     error = action.payload.response.data || {error: action.payload.message};
        //     return {...state, cleaners: {data: [], isLoading: false, isError: error}};

        case FETCH_BOOKING:
            return {...state, current: {data: [], isLoading: true, isError: null}};
        case FETCH_BOOKING_SUCCESS:
            // console.log(action);
            return {...state, current: {data: action.payload.data, isLoading: false, isError: null}};
        case FETCH_BOOKING_ERROR:
            // console.log(action);
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, current: {data: [], isLoading: false, isError: error}};

        case RESET_BOOKING:
            return {...state, current: {data: null, isLoading: null, isError: null}};

        case RESET_BOOKINGS:
            return {...state, all: {data: [], isLoading: null, isError: null}};

        case CREATE_BOOKING:
            return {...state, current: {data: [], isLoading: true, isError: null}};
        case CREATE_BOOKING_SUCCESS:
            return {...state, current: {data: action.payload.data, isLoading: true, isError: null}};
        case CREATE_BOOKING_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, current: {data: [], isLoading: false, isError: error}};

        case UPDATE_BOOKING:
            return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        case UPDATE_BOOKING_SUCCESS:
            const data = state.all.data.map((item) => {
                if (item._id === action.payload.data._id) {
                    return action.payload.data;
                } else {
                    return item;
                }
            });
            return {...state, all: {data: data, isLoading: false, isError: null}};
        case UPDATE_BOOKING_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: state.all.data, isLoading: false, isError: error}};

        case DELETE_BOOKING:
            return {...state, all: {data: state.all.data, isLoading: true, isError: null}};
        case DELETE_BOOKING_SUCCESS:
            const allBookings = state.all.data.filter((item) => {
                return item._id !== action.payload.data._id;
            });
            return {...state, current: {data: null}, all: {data: allBookings}};
        case DELETE_BOOKING_ERROR:
            error = action.payload.response.data || {error: action.payload.message};
            return {...state, all: {data: [], isLoading: false, isError: error}};

        default:
            return state;
    }
}