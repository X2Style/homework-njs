const configDev = {
    dbServer: 'ds127321.mlab.com',
    dbPort: 27321,
    dbUser: 'x2style',
    dbPassword: 'RP9GXTFguK',
    dbName: 'homework',
    port: 3000,
    pricePerHour: 122,
    jwtSecret: "hw-secret-key"
};

const configTest = {
    // dbServer: 'ds157521.mlab.com',
    // dbPort: 57521,
    // dbUser: 'test',
    // dbPassword: 'test',
    // dbName: 'homework',
    // port: 3003,
    dbServer: 'localhost',
    dbPort: 27017,
    dbUser: '',
    dbPassword: '',
    dbName: 'homework-test',
    port: 3003,
    pricePerHour: 100,
    jwtSecret: "hw-secret-key"
};

let config = configDev;

if (process.env.NODE_ENV === 'test') {
    config = configTest;
}

export default config;