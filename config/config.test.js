const config = {
    dbServer: 'ds157521.mlab.com',
    dbPort: 57521,
    dbUser: 'test',
    dbPassword: 'test',
    dbName: 'homework',
    port: 3000,
    pricePerHour: 100,
    jwtSecret: "hw-secret-key"
};

export default config;